<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

if (!function_exists('json_data_file')) {
	function json_data_file($mode) {
		$json_file = array();
		switch($mode) {
			case "home" :
				$json_file["slides"] = "http://www.honda-motorcycles.gr/mobile/data/home.php";
				//$json_file["slides"] = "http://localhost/www/hondamobile/data/json/home.json";
				break;
			case "menu" :
				$json_file["models_categories"] = "http://www.honda-motorcycles.gr/mobile/data/model-categories.php";
				break;
			case "offers" :
				$json_file["offers"] = "http://www.honda-motorcycles.gr/mobile/data/offers.php";
				//$json_file["offers"] = "http://localhost/www/hondamobile/data/json/offers.json";
				break;
			case "network" :
				$json_file["areas"] = "http://www.honda-motorcycles.gr/mobile/data/dealer_areas.php";
				$json_file["dealers"] = "http://www.honda-motorcycles.gr/mobile/data/dealers.php";
				break;
		}
		return $json_file;
	}

}
?>