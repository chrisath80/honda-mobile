<?php

?>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/model.css" />
<div id="Model" class='Content'>
	<div id="ModelContainer">
		<div class="ContentSlide" id="Parallax">
			<div id="ParallaxContainer">
				<div id="Loader" style="z-index:2000;position: absolute; background-color: #000; width:100%; height: 3px;">
					<div id="LoaderBar" style="z-index:2000;position: absolute; background-color: red; width:1px; height: 3px;"></div>
				</div>
				<div id="Background" style="z-index:1000;position: absolute; background-color: #000; width:100%; height: 100%;">
					<table width="100%" height="100%">
						<tr>
							<td align="center" valign="middle"><img src='<?php echo $model -> animation -> intro_image; ?>' width='80%' /></td>
						</tr>
					</table>
				</div>
				<div id="Arrows">
					<div id="Arrows1">
						<img src="<?php echo base_url(); ?>assets/images/model/arrow.png" />
					</div>
					<div id="Arrows2">
						<img src="<?php echo base_url(); ?>assets/images/model/arrow.png" />
					</div>
					<div id="Arrows3">
						<img src="<?php echo base_url(); ?>assets/images/model/arrow.png" />
					</div>
				</div>
				<div id="Road" style="z-index:900; top:-1000px; position: absolute; background-image:url(<?php echo $model -> animation -> moto_background; ?>); background-size:100% auto;background-position:center; width:100%; height: 100%;"></div>
				<div id="Moto" style="top:5000px; left:5000px; z-index:950; position: absolute; background-image:url(<?php echo $model -> animation -> moto; ?>); background-size:100% auto;background-position:center;background-repeat:no-repeat; width:90%; height: 65%;"></div>
				<div id="Main" style="z-index:890;top:-1000px;position: absolute; background-image:url(<?php echo $model -> animation -> main_photo; ?>); background-size:100% auto;background-position:center;background-repeat:no-repeat; width:100%; height: 100%;"></div>
				<div id="Zoomer" style="z-index:900;top:0px;left:-1000px;position: absolute; width:50%; height:100%;overflow:hidden;">
					<div id="ZoomerIn" style="z-index:890;top:0px;right:0px;position: absolute; background-image:url(<?php echo $model -> animation -> main_photo; ?>); background-size:120% auto;background-position:center;background-repeat:no-repeat; width:200%; height: 100%;"></div>
				</div>
			</div>

		</div>
		<div class="ContentSlide" id="Characteristics">
			<div style="width:100%; height:100%; overflow: auto;">
				<table bgcolor="#000000" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td><img id="ModelMainImage" src="<?php echo $model -> models -> varieties -> variety_photo; ?>" width="100%" /></td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;padding-top:20px;" class="MobileH1"> ΜΟΝΤΕΛΑ & ΤΙΜΕΣ </td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;padding-bottom:15px;padding-top:10px;" class="MobileSubtitles" id='ColorName'> TRICOLORE </td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;padding-bottom:15px;" class="MobileH1">
						<div style="position: relative;">
							<img width="33" src="<?php echo base_url();?>data/images/fireblade-variety-1-thumbnail.png" />
							<!--
							<div id="Sel1" style='display: none; position: absolute; float:left; border-radius: 50%; overflow: hidden; border: 1px solid #fff; width:37px; height:37px;'></div>
							
							<div id="Round1" onclick='javascript:ChangeMain("");' class='' style='float:left; border-radius: 50%; overflow: hidden; width:33px; height:33px;'>
								<div style='background-color:#1D2344; width:11px; height:33px;float:left;'></div>
								<div style='background-color:#fff; width:11px; height:33px;float:left;'></div>
								<div style='background-color:#BF2B3B; width:11px; height:33px;float:left;'></div>
							</div>
							-->
						</div>
						

						<script>
							//ChangeMain("");
						</script>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;padding-bottom:15px;" class="MobileSubtitles"> ΜΟΝΤΕΛΟ &amp; ΤΙΜΗ </td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;color:#fff !important;font-weight: bold;" class="H3"><?php echo $model -> models -> varieties -> variety_name; ?></td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;;font-size:18px; color:#B3B3B3 !important;"> <?php echo $model -> models ->  varieties -> variety_price; ?>€ </td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;padding-top:15px;" class="MobileSubtitles"> ΠΛΗΡΟΦΟΡΙΕΣ </td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-right:10px;padding-top:15px;padding-bottom:20px;line-height: 20px" class="MobileBodyText SpecsText">
							
							<?php echo str_replace("\r\n","<br/>",$model -> models -> info); ?>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="ContentSlide" id="Specs">
			<div style="width:100%; height:100%; overflow: auto; background-color: #000;">
				<table align="center" bgcolor="#000000" cellpadding="10" cellspacing="0" width="100%">
					<tr>
						<td style="padding-top:10px;padding-bottom:10px;">
						<div class="MobileH1">
							ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ
						</div></td>
					</tr>
					<tr>
						<td>
							<?php 
								
								foreach($model -> characteristics as $key => $value){
									echo '<div class="MobileSubtitles SpecsHeader">';
									echo "";
									echo '</div>';
									echo '<div class="MobileBodyText SpecsText">';
				
									echo '</div>';
								}
							?>
						<!--
						<div class="MobileSubtitles SpecsHeader">
							Κινητήρας
						</div>
						<div class="MobileBodyText SpecsText">
							Υγρόψυκτος – 4χρονος – 16βάλβιδος – 4κύλινδρος - DOHC
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Κυβισμός
						</div>
						<div class="MobileBodyText SpecsText">
							999 cc
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Μέγιστη Ισχύς
						</div>
						<div class="MobileBodyText SpecsText">
							178 HP / 12.000 σ.α.λ
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Μέγιστη Ροπή
						</div>
						<div class="MobileBodyText SpecsText">
							178 HP / 12.000 σ.α.λ
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Ταχύτητες
						</div>
						<div class="MobileBodyText SpecsText">
							6
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Τελική Mετάδοση
						</div>
						<div class="MobileBodyText SpecsText">
							Αλυσίδα
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Τροφοδοσία
						</div>
						<div class="MobileBodyText SpecsText">
							Ηλεκτρονικός Ψεκασμός PGM-DSFI
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Βάρος
						</div>
						<div class="MobileBodyText SpecsText">
							200 kg
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Φρένα
						</div>
						<div class="MobileBodyText SpecsText">
							E: 2 Δίσκοi 320mm με 4πίστονες δαγκάνες
							<br/>
							Π: Δίσκος 220mm με μονοπίστονη δαγκάνα C-ABS
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Διαστάσεις (ΜxΠxΥ)
						</div>
						<div class="MobileBodyText SpecsText">
							2.077 x 826 x 1.135mm
						</div><div class="SpecsSep"></div>
						<div class="MobileSubtitles SpecsHeader">
							Ύψος σέλας
						</div>
						<div class="MobileBodyText SpecsText">
							820 mm
						</div><div class="SpecsSep"></div>
						-->
						</td>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="ContentSlide" id="PhotosVideos">
		<script>
			function ShowPhotos() {
				$("#Photos")[0].style.display = "block";
				$("#Videos")[0].style.display = "none";
				return;
			}

			function ShowVideos() {
				$("#Photos")[0].style.display = "none";
				$("#Videos")[0].style.display = "block";
				return;
			}
		</script>
			<script>
				var ShowPopup = function(Mode,Img) {
					if (Mode) {
						$("#PhotoPopUp")[0].style.display = 'block';
						$("#PhotoPopUpImage")[0].style.backgroundImage = "url("+Img.src+")";
					} else {
						$("#PhotoPopUp")[0].style.display = 'none';
					}
				}
			</script>
			<div style="width:100%; height:100%; overflow: auto; background-color: #000;">
				<div id="Photos" style="width:100%; height:100%;">
					<style>
						#PhotosTable img {
							display: block;
						}
					</style>
					<table id="PhotosTable" align="center" bgcolor="#000000" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td height="80" width="50%" class="MobileH4" style="background-color: #EC1B2E" align="center" valign="middle"> ΦΩΤΟΓΡΑΦΙΕΣ </td>
							<td onclick="ShowVideos();" class="MobileH4" width="50%" style="background-color: #000" align="center" valign="middle"> VIDEOS </td>
						</tr>
						<?php
						$counter = 0;
						while($counter < count($model->images) / 2){
						?>
						<tr>
							<td width="50%"><img onclick='ShowPopup(true,this);' border="0" width="100%" src="<?php echo $model->images[$counter];?>"/></td>
							<td width="50&">
								<?php if($counter*2 < count($model->images)){?>
								<img onclick='ShowPopup(true,this);' border="0" width="100%" src="<?php echo $model->images[$counter+1];?>"/>
								<?php }else{
									if(count($model->images) % 2 == 0){
								?>
								<img onclick='ShowPopup(true,this);' border="0" width="100%" src="<?php echo $model->images[$counter+1];?>"/>
								<?php
									}
								} ?>
							</td>
						</tr>
						<?php
						$counter++;
						}
						?>
					</table>
				</div>
				<div id="Videos" style="display:none;">
					<style>
						#VideosTable img {
							display: block;
						}
					</style>
					<table id="VideosTable" align="center" bgcolor="#000000" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td onclick="ShowPhotos();" height="80" width="50%" class="MobileH4" style="background-color: #000" align="center" valign="middle"> ΦΩΤΟΓΡΑΦΙΕΣ </td>
							<td class="MobileH4" width="50%" style="background-color: #EC1B2E" align="center" valign="middle"> VIDEOS </td>
						</tr>
						<?php
						$counter = 0;
						while($counter < count($model->videos)){
							$video = explode("/",$model->videos[$counter]->src);
							$video_id = $video[count($video)-1];
						?>
						<tr>
							<td colspan="2">
								<iframe src="//player.vimeo.com/video/<?php echo $video_id; ?>?portrait=0" width="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</td>
						</tr>
						<?php
						$counter++;
						}
						?>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/model.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/model/main.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/model/animation.js"></script>
<script></script>