<?php
$json_data_areas = file_get_contents($json_data_file["areas"]);
$areas = json_decode($json_data_areas);
?>
<div class="w-clearfix h-google-map">

	<img style="z-index: 40000;" class="h-close-video-icon h-map" src="<?=base_url() ?>assets/images/close-black.svg" alt="54d20bfaa59e05340ae1ed00_close-black.svg" data-ix="close-map">
	<div id="map-canvas" style="width:100%; height:100%;"></div>
</div>
<div class="h-content-section">
	<div class="h-network-content">
		<h2 class="h-page-title">ΔΙΚΤΥΟ</h2>
		<div class="w-form h-contact-form">
			<form id="email-form" name="email-form" data-name="Email Form">
				<input class="w-input h-form-textfield" id="name" type="text" placeholder="ΟΝΟΜΑΣΙΑ ΣΥΝΕΡΓΑΤΗ" name="name" data-name="name">
				<h4 class="h-page-title h-form-label">ΕΝΑΛΛΑΚΤΙΚΗ ΑΝΑΖΗΤΗΣΗ</h4>
				<select class="w-select h-form-textfield" id="area" name="area" data-name="area" onchange="UpdateCities();">
					<option value="">ΕΠΙΛΕΞΤΕ ΠΕΡΙΟΧΗ</option>
					
					<?php foreach($areas as $area){?>
						<option value="<?=$area -> area ?>"><?=$area -> area ?></option>
					<?php } ?>
				</select>
				<select class="w-select h-form-textfield" id="city" name="city">

				</select>

				<select class="w-select h-form-textfield" id="type" name="type" data-name="type">
					<option value="">ΕΠΙΛΕΞΤΕ ΤΥΠΟ</option>
					<option value="synergeio">ΣΥΝΕΡΓΕΙΟ</option>
					<option value="pwliseis">ΠΩΛΗΣΕΙΣ</option>
					<option value="antallaktika">ΑΝΤΑΛΛΑΚΤΙΚΑ</option>
				</select><a onclick="SearchNetwork();" class="h-form-button h-form-net-button" href="#">ΑΝΑΖΗΤΗΣΗ</a>
			</form>
			<div class="w-form-done">
				<p>
					Thank you! Your submission has been received!
				</p>
			</div>
			<div class="w-form-fail">
				<p>
					Oops! Something went wrong while submitting the form :(
				</p>
			</div>
		</div>
	</div>
</div>
<script>
	var Cities = [];
	<?php foreach($areas as $area){?>
					Cities.push({"Area":"<?=$area -> area ?>","Cities":['<?=implode("','", $area -> cities) ?>
				']});
	<?php } ?>
		function UpdateCities() {
			$('#city').empty();
			$('#city').append($('<option></option>').val('').html('ΕΠΙΛΕΞΤΕ ΠΟΛΗ'));
			var Area = $("#area").val();
			if (Area) {
				for (City in Cities) {
					if (Cities[City].Area == Area) {
						for (InCity in Cities[City].Cities) {
							$('#city').append($('<option></option>').val(Cities[City].Cities[InCity]).html(Cities[City].Cities[InCity]));
						}
					}
				}
			}
		}
</script>
<script>
	// Asynchronously Load the map API
	var script = document.createElement('script');
	script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
	document.body.appendChild(script);

	var run = 0;

	function initialize() {
		//foo
	}

	function SearchNetwork() {
		$.ajax({
			type : "GET",
			url : "<?=base_url()?>assets/lib/search.php",
			data : {
				name : $("#name").val(),
				area : $("#area").val(),
				city: $("#city").val()
			}
		}).done(function(msg) {
			
		});
		$("#map-canvas")[0].innerHTML = "";
		$(".h-google-map").css({
			display : "block"
		});
		var map;
		var bounds = new google.maps.LatLngBounds();
		var mapOptions = {
			mapTypeId : 'roadmap'
		};

		// Display a map on the page
		map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
		map.setTilt(45);

		// Multiple Markers
		var markers = [['London Eye, London', 51.503454, -0.119562], ['Palace of Westminster, London', 51.499633, -0.124755]];

		// Info Window Content
		var infoWindowContent = [['<div class="info_content">' + '<h3>London Eye</h3>' + '<p>The London Eye is a giant Ferris wheel situated on the banks of the River Thames. The entire structure is 135 metres (443 ft) tall and the wheel has a diameter of 120 metres (394 ft).</p>' + '</div>'], ['<div class="info_content">' + '<h3>Palace of Westminster</h3>' + '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' + '</div>']];

		// Display multiple markers on a map
		var infoWindow = new google.maps.InfoWindow(), marker, i;

		// Loop through our array of markers & place each one on the map
		for ( i = 0; i < markers.length; i++) {
			var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
			bounds.extend(position);
			marker = new google.maps.Marker({
				position : position,
				map : map,
				title : markers[i][0]
			});

			// Allow each marker to have an info window
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infoWindow.setContent(infoWindowContent[i][0]);
					infoWindow.open(map, marker);
				}
			})(marker, i));

			// Automatically center the map fitting all markers on the screen
			//map.fitBounds(bounds);
			setTimeout(function() {
				map.fitBounds(bounds);
			}, 100);
		}

		// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)

		var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
			this.setZoom(14);
			google.maps.event.removeListener(boundsListener);
		});

	}
</script>