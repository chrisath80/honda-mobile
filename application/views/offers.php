<?php
$json_data_offers = file_get_contents($json_data_file["offers"]);
$offers = json_decode($json_data_offers);
?>
<div class="h-content-section">
	<div class="h-offers-section">
		<?php foreach($offers as $offer){?>
		<div class="h-offer-element" style="background-image: url(<?=$offer->thumbnail?>)">
			<a class="w-inline-block h-menu-link" href="<?=base_url()?>/index.php/model/view/<?=$offer->category_id?>/<?=$offer->model_id?>"></a>
			<div class="h-models-category-skin"></div>
			<div class="h-model-shadow"></div>
			<h4 class="h-models-category-text offers"><?=$offer->models_name?>
			<br>
			<span class="h-offer-description"><?=$offer->old_price?></span></h4>
		</div>
		<?php } ?>
	</div>
</div>