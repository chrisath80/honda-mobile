<?php
$json_data_slides = file_get_contents($json_data_file["slides"]);
$slides = json_decode($json_data_slides);
?>
    <div class="h-content-section">
      <div class="w-slider h-slder" data-animation="slide" data-duration="500" data-infinite="1" data-delay="5000" data-autoplay="1">
        <div class="w-slider-mask">
          <?php foreach($slides as $slide){ ?>
          <?php 
          	if(!$slide->video_src){
          ?>
          <div class="w-slide h-slide-1" style="background-image: url(<?=$slide->thumbnail;?>)">
            <a class="w-inline-block h-slider-link" href="<?php echo base_url()?>index.php/model/view/<?=$slide->category_id?>/<?=$slide->model_id?>"></a>
            <h3 class="h-slide-text"><?=$slide->title?></h3>
          </div>
          <?php 
			}else{
			$video_id = explode("/",$slide->video_src);
			$video_id = $video_id[count($video_id)-1];
          ?>
          <div class="w-slide h-slide-1" onclick="ViewVideo('<?=$video_id ?>');" data-ix="open-video-container" style="background-image:url(<?=base_url()?>assets/images/video-slide.jpg)">
            <h3 class="h-slide-text"><?=$slide->title?></h3>
          </div>
          <?php } ?>
          <?php } ?>
        </div>
        <div class="w-slider-arrow-left">
          <div class="w-icon-slider-left"></div>
        </div>
        <div class="w-slider-arrow-right">
          <div class="w-icon-slider-right"></div>
        </div>
        <div class="w-slider-nav w-round h-slide-bottom-nav"></div>
      </div>
    </div>
    <div class="h-video-section">
      <div class="w-clearfix h-close-video-container"><img class="h-close-video-icon" src="<?php echo base_url()?>assets/images/close.svg" alt="54cfab7223a1aa6d4eea9b2d_close.svg" data-ix="hide-video-container">
      </div>
      <div class="h-video-container">
        <div class="w-embed w-video h-vimeo-video" style="padding-top: 56.27659574468085%;">
			<iframe id="VimeoVideo" src="https://player.vimeo.com/video/60152269" width="100%" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>        </div>
      </div>
    </div>
    <script>
    	function ViewVideo(Id){
    		$("#VimeoVideo")[0].src = "https://player.vimeo.com/video/"+Id;
    		return;
    	}
    </script>