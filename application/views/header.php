<div class="h-header-section">
	<div class="w-row h-header-row-menu">
		<div class="w-col w-col-2 w-col-small-2 w-col-tiny-2 w-clearfix h-header-left-column"><img onclick="OpenMenu();" class="h-left-icon" src="<?php echo base_url()?>assets/images/menu-icon.svg" alt="54c22dad7bbf1d8c4cfd6158_menu-icon.svg" data-ix="open-menu">
		</div>
		<div class="w-col w-col-8 w-col-small-8 w-col-tiny-8 h-header-main-column"><img class="h-main-icon" src="<?php echo base_url()?>assets/images/honda-logo.svg" alt="54c230686631ca2737e867b1_honda-logo.svg">
		</div>
		<div class="w-col w-col-2 w-col-small-2 w-col-tiny-2 w-clearfix h-header-right-column">
			
				<?php $special_location = false; if($location=="network"){ $special_location = true; ?>
					<a id="right-icon-href" href="<?=base_url()?>index.php/network">
						<img onclick="" class="h-right-icon-menu" src="<?php echo base_url()?>assets/images/network-red.svg" alt="54c230f57bbf1d8c4cfd61b0_menu-network.svg">
					</a>
				<?php } ?>
				<?php if($location=="model"){ $special_location = true;?>
					<img class="h-right-icon-menu" src="<?php echo base_url()?>assets/images/network-red.svg" alt="54c230f57bbf1d8c4cfd61b0_menu-network.svg">
				<?php } ?>
				<?php if(!$special_location){ ?>
					<a id="right-icon-href" href="<?=base_url()?>index.php/network">
						<img onclick="" class="h-right-icon-menu" src="<?php echo base_url()?>assets/images/network.svg" alt="54c230f57bbf1d8c4cfd61b0_menu-network.svg">
					</a>
				<?php } ?>
			
		</div>
	</div>
</div>
<script>
	function OpenMenu(){
		$(".h-left-icon")[0].src = "<?php echo base_url()?>assets/images/back.svg";
		$(".h-right-icon-menu")[0].src = "<?php echo base_url()?>assets/images/close.svg";
		$(".h-right-icon-menu")[0].setAttribute("onclick","CloseMenu()");
		if($('#right-icon-href')){
			$('#right-icon-href')[0].href = '#';
		}
		return;
	}
	
	function CloseMenu(){
		$(".h-left-icon")[0].src = "<?php echo base_url()?>assets/images/menu-icon.svg";
		$(".h-right-icon-menu")[0].src = "<?php echo base_url()?>assets/images/network.svg";
		if(Location == "network"){
			$(".h-right-icon-menu")[0].src = "<?php echo base_url()?>assets/images/network-red.svg";
		}
		$(".h-right-icon-menu")[0].setAttribute("onclick","");
		$(".h-menu-section").css({
			display:"none"
		});
		if($('#right-icon-href')){
			setTimeout(function(){$('#right-icon-href')[0].href = '<?=base_url()?>index.php/network';
		},1000);
		}
		return false;
	}
</script>