<div class="h-content-section">
	<div class="h-contact-section">
		<h2 class="h-page-title">ΕΠΙΚΟΙΝΩΝΙΑ</h2>
		<div class="w-form">
			<form class="h-contact-form" id="email-form" name="email-form" data-name="Email Form">
				<input class="w-input h-form-textfield" id="name" type="text" placeholder="ΟΝΟΜΑ (*)" name="name" data-name="Name" required="required">
				<input class="w-input h-form-textfield" id="Lastname" type="text" placeholder="ΕΠΩΝΥΜΟ (*)" name="Lastname" data-name="Lastname" required="required">
				<input class="w-input h-form-textfield" id="Tel" type="text" placeholder="ΤΗΛΕΦΩΝΟ" name="Tel" data-name="Tel">
				<input class="w-input h-form-textfield" id="Email" type="email" placeholder="Email (*)" name="Email" data-name="Email" required="required">
				<textarea class="w-input h-form-textfield" id="Text" placeholder="ΠΑΡΑΤΗΡΗΣΕΙΣ (*)" name="Text" data-name="Text" required="required"></textarea>
				<select class="w-select h-form-textfield" id="To" name="To" required="required" data-name="To">
					<option value="">ΕΠΙΛΕΞΤΕ ΑΠΟΔΟΧΕΑ</option>
					<option value="ΤΕΧΝΙΚΟ ΤΜΗΜΑ">ΤΕΧΝΙΚΟ ΤΜΗΜΑ</option>
					<option value="ΤΜΗΜΑ ΑΝΤΑΛΛΑΚΤΙΚΩΝ">ΤΜΗΜΑ ΑΝΤΑΛΛΑΚΤΙΚΩΝ</option>
					<option value="ΤΜΗΜΑ ΠΩΛΗΣΕΩΝ">ΤΜΗΜΑ ΠΩΛΗΣΕΩΝ</option>
				</select>
				<select class="w-select h-form-textfield" id="Newsletter" name="Newsletter" required="required" data-name="Newsletter">
					<option value="">ΕΓΓΡΑΦΗ NEWSLETTER</option>
					<option value="ΝΑΙ">ΝΑΙ</option>
					<option value="ΟΧΙ">ΟΧΙ</option>
				</select>
				<input class="w-button h-form-button" type="submit" value="ΑΠΟΣΤΟΛΗ" data-wait="ΠΑΡΑΚΑΛΩ ΠΕΡΙΜΕΝΕΤΕ" wait="ΠΑΡΑΚΑΛΩ ΠΕΡΙΜΕΝΕΤΕ">
			</form>
			<div class="w-form-done">
				<p class="h-thank-you">
					ΕΥΧΑΡΙΣΤΟΥΜΕ ΓΙΑ ΤΗΝ ΕΠΙΚΟΙΝΩΝΙΑ
				</p>
			</div>
			<div class="w-form-fail">
				<p class="h-thank-you">
					ΠΑΡΑΚΑΛΩ ΕΛΕΓΞΤΕ ΤΗΝ ΟΡΘΟΤΗΤΑ ΤΩΝ ΣΤΟΙΧΕΙΩΝ
				</p>
			</div>
		</div>
	</div>
</div>