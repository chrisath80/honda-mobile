<!DOCTYPE html>
<html data-wf-site="54c22c5e077f5525376f5cc2" data-wf-page="54cfa12023a1aa6d4eea99e0">
<head>
  <meta charset="utf-8">
  <title>Honda Mobile</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="generator" content="Webflow">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/webflow.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/hondamobile.webflow.css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ["Roboto:100,300,regular,500,700,900:latin,greek"]
      }
    });
  </script>
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/modernizr.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico">
  <link rel="apple-touch-icon" href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png">
</head>
<body>
  <div class="h-main-section">
	<?=$menu; ?>
	<?=$header; ?>
	<?=$content; ?>
  </div>
  <script>
  	var Location = '<?=$location?>';
  </script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>assets/js/webflow.js"></script>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
</body>
</html>