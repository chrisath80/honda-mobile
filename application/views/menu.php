<?php
$json_data_models_categories = file_get_contents($json_data_file["models_categories"]);
$models_categories = json_decode($json_data_models_categories);
?>
    <div class="h-menu-section">
      <div class="h-menu-first-level">
        <div class="w-clearfix h-menu-first-level-element">
          <a class="w-inline-block h-menu-link" href="<?=base_url()?>"></a>
          <div class="h-menu-element-icon-container"><img class="h-menu-home-icon" src="<?php echo base_url()?>assets/images/home.svg" alt="54c65efb9da8dba102bcb428_home.svg">
          </div>
          <div class="h-menu-element-text-container">
            <h4 class="h-menu-element-text <?php if($location=="home"){echo 'selected';}?>">ΑΡΧΙΚΗ</h4>
          </div>
        </div>
        <div class="w-clearfix h-menu-first-level-element even" data-ix="open-models-categories">
          <div class="h-menu-element-icon-container"><img class="h-menu-home-icon models" src="<?php echo base_url()?>assets/images/models.svg" alt="54c65ffbb706751c7dbe2fa8_models.svg">
          </div>
          <div class="h-menu-element-text-container">
            <h4 class="h-menu-element-text <?php if($location=="models"){echo 'selected';}?>">ΜΟΝΤΕΛΑ</h4>
          </div>
        </div>
        <div class="w-clearfix h-menu-first-level-element">
          <a class="w-inline-block h-menu-link" href="<?=base_url()?>index.php/offers"></a>
          <div class="h-menu-element-icon-container"><img class="h-menu-home-icon offers" src="<?php echo base_url()?>assets/images/offers.svg" alt="54c6606481904c1b7ddff0d7_offers.svg">
          </div>
          <div class="h-menu-element-text-container">
            <h4 class="h-menu-element-text <?php if($location=="offers"){echo 'selected';}?>">ΠΡΟΣΦΟΡΕΣ</h4>
          </div>
        </div>
        <div class="w-clearfix h-menu-first-level-element even">
          <a class="w-inline-block h-menu-link" href="<?=base_url()?>index.php/social"></a>
          <div class="h-menu-element-icon-container"><img class="h-menu-home-icon models" src="<?php echo base_url()?>assets/images/social.svg" alt="54c6607c3d5e25a202c7cbbf_social.svg">
          </div>
          <div class="h-menu-element-text-container">
            <h4 class="h-menu-element-text <?php if($location=="social"){echo 'selected';}?>">SOCIAL</h4>
          </div>
        </div>
        <div class="w-clearfix h-menu-first-level-element">
          <a class="w-inline-block h-menu-link" href="<?=base_url()?>index.php/contact"></a>
          <div class="h-menu-element-icon-container"><img class="h-menu-home-icon contact" src="<?php echo base_url()?>assets/images/contact.svg" alt="54c66095b706751c7dbe2fb6_contact.svg">
          </div>
          <div class="h-menu-element-text-container">
            <h4 class="h-menu-element-text <?php if($location=="contact"){echo 'selected';}?>">ΕΠΙΚΟΙΝΩΝΙΑ</h4>
          </div>
        </div>
        <div class="w-clearfix h-menu-first-level-element even">
          <a class="w-inline-block h-menu-link" href="<?=base_url()?>index.php/network"></a>
          <div class="h-menu-element-icon-container"><img class="h-menu-home-icon network" src="<?php echo base_url()?>assets/images/network.svg" alt="54c230f57bbf1d8c4cfd61b0_menu-network.svg">
          </div>
          <div class="h-menu-element-text-container">
            <h4 class="h-menu-element-text <?php if($location=="network"){echo 'selected';}?>">ΔΙΚΤΥΟ</h4>
          </div>
        </div>
        <div class="w-clearfix h-menu-first-level-element">
          <a class="w-inline-block h-menu-link" href="http://www.saracakis.gr/" target="_blank"></a>
          <div class="h-menu-element-icon-container"><img class="h-menu-home-icon saracakis" src="<?php echo base_url()?>assets/images/saracakis.svg" alt="54c660c03d5e25a202c7cbc5_saracakis.svg">
          </div>
          <div class="h-menu-element-text-container">
            <h4 class="h-menu-element-text">ΣΑΡΑΚΑΚΗΣ</h4>
          </div>
        </div>
      </div>
      <div class="w-clearfix h-menu-second-level">
		<?php foreach($models_categories as $model_category){?>
        <div class="h-models-category _2" data-ix="open-model" style="background-image: url(<?=$model_category -> thumbnail ?>);" onclick="DisplayModels('<?=$model_category -> id ?>');">
          <div class="h-models-category-skin"></div>
          <h5 class="h-models-category-text"><?=$model_category -> category_name ?></h5>
        </div>
        <?php } ?>
      </div>
      <div class="h-menu-third-level">
        <?php foreach($models_categories as $model_category){?>
        	<div class="h-menu-model-js" id="<?=$model_category -> id ?>" style="display:none;">
        	<?php 
        		$models = json_decode(file_get_contents($model_category->models_data));
				foreach($models as $model){
        	?>
        <div class="h-model" style="background-image:url(<?=$model->thumbnail ?>)">
          <a class="w-inline-block h-menu-link" href="<?=base_url() ?>index.php/model/view/<?=$model_category->id?>/<?=$model->id?>"></a>
          <div class="h-models-category-skin"></div>
          <h5 class="h-models-category-text"><?=$model -> model_name; ?></h5>
          <div class="h-model-shadow"></div>
        </div>
        <?php } ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <script src="<?php echo base_url()?>assets/js/menu.js"></script>