<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Offers extends CI_Controller {
		
	public function index() {
		$this -> load -> helper('url');
		$this -> load -> helper('data');
		$data["location"] = 'offers';
		$data["json_data_file"] = json_data_file("offers");
		$data["content"] = $this -> load -> view("offers", $data, true);
		$data["json_data_file"] = json_data_file("menu");
		$data["menu"] = $this -> load -> view("menu", $data, true);
		$data["header"] = $this -> load -> view("header", $data, true);
		$this -> load -> view('template', $data);
		return;
	}

}
