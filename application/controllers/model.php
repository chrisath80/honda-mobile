<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model extends CI_Controller {

	public function index() {
		return;
	}

	public function view($category_id, $model_id) {
		$this -> load -> helper('url');
		$this -> load -> helper('data');
		$data["location"] = 'model';
		$data["model"] = $this -> get_model_data($category_id, $model_id);
		$data["content"] = $this -> load -> view("model", $data, true);
		$data["json_data_file"] = json_data_file("menu");
		$data["menu"] = $this -> load -> view("menu", $data, true);
		$this -> load -> view('template', $data);
	}

	public function get_category_data($category_id) {
		$json_data_file = json_data_file("menu");
		$json_data_models_categories = file_get_contents($json_data_file["models_categories"]);
		$models_categories = json_decode($json_data_models_categories);
		foreach ($models_categories as $key => $value) {
			if ($value -> id == $category_id) {
				return $value;
			}
		}
		return null;
	}

	public function get_model_data($category_id, $model_id) {
		$category_data_file = $this -> get_category_data($category_id) -> models_data;
		$json_data_models = file_get_contents($category_data_file);
		$models = json_decode($json_data_models);
		foreach ($models as $key => $value) {
			if ($value -> id == $model_id) {
				return json_decode(file_get_contents($value -> model_data));
			}
		}
		return null;
	}

}
