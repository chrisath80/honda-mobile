<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Social extends CI_Controller {
		
	public function index() {
		$this -> load -> helper('url');
		$this -> load -> helper('data');
		$data["location"] = 'social';
		$data["content"] = $this -> load -> view("social", $data, true);
		$data["json_data_file"] = json_data_file("menu");
		$data["menu"] = $this -> load -> view("menu", $data, true);
		$data["header"] = $this -> load -> view("header", $data, true);
		$this -> load -> view('template', $data);
		return;
	}

}
