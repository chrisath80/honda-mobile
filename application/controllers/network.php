<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Network extends CI_Controller {

	public function index() {
		$this -> load -> helper('url');
		$this -> load -> helper('data');
		$data["location"] = 'network';
		$data["json_data_file"] = json_data_file("network");
		$data["content"] = $this -> load -> view("network", $data, true);
		$data["json_data_file"] = json_data_file("menu");
		$data["menu"] = $this -> load -> view("menu", $data, true);
		$data["header"] = $this -> load -> view("header", $data, true);
		$this -> load -> view('template', $data);
		return;
	}

}
