<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function index() {
		$this -> load -> helper('url');
		$this -> load -> helper('data');
		$data["location"] = 'contact';
		$data["content"] = $this -> load -> view("contact", $data, true);
		$data["json_data_file"] = json_data_file("menu");
		$data["menu"] = $this -> load -> view("menu", $data, true);
		$data["header"] = $this -> load -> view("header", $data, true);
		$this -> load -> view('template', $data);
		return;
	}

}
