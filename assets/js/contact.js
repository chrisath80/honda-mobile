var SendMessage = function(URL) {
	var Name = $("#ContactName").val();
	var LastName = $("#ContactLastName").val();
	var Tel = $("#ContactTel").val();
	var Email = $("#ContactEmail").val();
	var Obs = $("#ContactObs").val();
	var To = $("#ContactTo").val();
	var Newsletter = $("#ContactNewsletter").val();
	if (Name != "" && LastName != "" && Email != "" && Obs != "" && To != "") {
		if (IsEmail(Email)) {
			$.ajax({
				type : "POST",
				url : URL,
				data : {
					Name : Name,
					LastName : LastName,
					Tel : Tel,
					Email : Email,
					Obs : Obs,
					To : To,
					Newsletter : Newsletter
				}
			}).done(function(msg) {
				if (msg == "true") {
					alert("Το μήνυμά σας στάλθηκε επιτυχώς.\r\nΕυχαριστούμε για την επικοινωνία.");
					$(':input', '#Contact').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
				} else {
					alert("Παρουσιάστηκε κάποιο πρόβλημα.\r\nΠαρακαλώ δοκιμάστε ξανά.");
				}
			});
		} else {
			alert("Παρακαλώ συμπληρώστε έναν έγκυρο λογαριασμό e-mail");
		}
	} else {
		alert("Παρακαλώ συμπληρώστε τα απαραίτητα πεδία");
	}
	return;
};

/* http://stackoverflow.com/questions/46155/validate-email-address-in-javascript */
function IsEmail(Email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(Email);
} 