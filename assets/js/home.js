var HomePage = 1;
var InitHomepage = function() {
	$("#HomepageSlider").on("swiperight", function(e) {
		var a = MoveHomepageSlider("right");
		return;
	});
	$("#HomepageSlider").on("swipeleft", function(e) {
		var a = MoveHomepageSlider("left");
		return;
	});
	return;
};

var AutoMove = function() {
	HomePage++;
	if (HomePage > TotalSliders) {
		HomePage = 1;
	}
	$("#HomepageSliderMover").animate({
		left : "-" + ((HomePage - 1) * 100) + "%",
	}, 500, "easeInOutQuart");
	return;
};

var MoveHomepageSlider = function(Direction) {
	var PageChanged = false;
	if (Direction == "left" && HomePage < TotalSliders) {
		PageChanged = true;
		HomePage++;
	}
	if (Direction == "right" && HomePage > 1) {
		PageChanged = true;
		HomePage--;
	}
	if (PageChanged) {
		$("#HomepageSliderMover").animate({
			left : "-" + ((HomePage - 1) * 100) + "%",
		}, 500, "easeInOutQuart");
	}
	return;
};
