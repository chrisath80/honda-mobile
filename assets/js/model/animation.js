var FinishAnimationRun = false;
var Scroll;
var Animation;
var ScrollAnimate;
var Screen = $("#Screen")[0];
var GetHeight = function() {
	return Screen.offsetHeight;
};

var GetWidth = function() {
	return Screen.offsetWidth;
};

function ResetAnimation() {
	setTimeout("ScrollAnimate.scrollTo(0)", 200);
	return;
}

function AnimationInit() {

	CreateAnimation();
	ScrollAnimate = ScrollAnimator();
	ScrollAnimate.init({
		animation : Animation,
		maxScroll : Scroll * 24,
		useRAF : false,
		tickSpeed : 50,
		scrollSpeed : 25,
		debug : false,
		tweenSpeed : .2,
		startAt : 0,
		endAt : Scroll * 24,
		scrollTop : 0,
		container : $('#ParallaxContainer'),
		onStart : function() {
		},
		onResize : function() {
		},
		onUpdate : function() {
		},
		onComplete : function() {

		}
	});

}

function CreateAnimation() {
	Screen = $('#Screen')[0];

	Scroll = GetHeight() / 3;
	Animation = [];

	Animation[Animation.length] = {
		selector : '#LoaderBar',
		startAt : 0,
		endAt : (Scroll * 24) - 100,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"width" : 1
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"width" : GetWidth()
			}
		}]
	};

	Animation[Animation.length] = {
		selector : '#Arrows',
		startAt : 0,
		endAt : Scroll * 2,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"opacity" : 1
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"opacity" : 0
			}
		}]
	};

	Animation[Animation.length] = {
		selector : '#Background',
		startAt : 0,
		endAt : Scroll * 3,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"top" : 0
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"top" : -(Scroll * 3)
			}
		}]
	};

	Animation[Animation.length] = {
		selector : '#Road',
		startAt : 0,
		endAt : (Scroll * 3) - 250,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"top" : (Scroll * 3) - 250
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"top" : 0
			}
		}]
	};

	Animation[Animation.length] = {
		selector : '#Moto',
		startAt : Scroll * 3 - 200,
		endAt : Scroll * 6,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				top : 0,
				"left" : GetWidth()
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				top : GetHeight() / 2,
				"left" : -(GetWidth())
			}
		}]
	};

	Animation[Animation.length] = {
		selector : '#Road',
		startAt : Scroll * 6,
		endAt : Scroll * 9,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"top" : 0
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"top" : -(Scroll * 3)
			}
		}]
	};

	Animation[Animation.length] = {
		selector : '#Main',
		startAt : Scroll * 6,
		endAt : (Scroll * 9) - 100,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"top" : (Scroll * 3) - 250
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"top" : 0
			}
		}]
	};

	Animation[Animation.length] = {
		selector : '#Zoomer',
		startAt : Scroll * 9,
		endAt : Scroll * 12,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"left" : (GetWidth())
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"left" : -(GetWidth())
			}
		}]
	};
	Animation[Animation.length] = {
		selector : '#ZoomerIn',
		startAt : Scroll * 9,
		endAt : Scroll * 13,
		onEndAnimate : function(anim) {
		},
		keyframes : [{
			position : 0,
			properties : {
				"right" : 0
			}
		}, {
			position : 1,
			ease : TWEEN.Easing.Linear.EaseNone,
			properties : {
				"right" : -(GetWidth())
			}
		}]
	};
	/*
	 Animation[Animation.length] = {
	 selector : '#Block1',
	 startAt : Scroll * 12,
	 endAt : Scroll * 18,
	 onEndAnimate : function(anim) {
	 },
	 keyframes : [{
	 position : 0,
	 properties : {
	 "left" : (GetWidth())
	 }
	 }, {
	 position : 1,
	 ease : TWEEN.Easing.Linear.EaseNone,
	 properties : {
	 "left" : -(GetWidth() * 2)
	 }
	 }]
	 };
	 Animation[Animation.length] = {
	 selector : '#Zoomer2In',
	 startAt : Scroll * 12,
	 endAt : Scroll * 19,
	 onEndAnimate : function(anim) {
	 },
	 keyframes : [{
	 position : 0,
	 properties : {
	 "right" : 0
	 }
	 }, {
	 position : 1,
	 ease : TWEEN.Easing.Linear.EaseNone,
	 properties : {
	 "right" : -(GetWidth())
	 }
	 }]
	 };
	 Animation[Animation.length] = {
	 selector : '#Zoomer3In',
	 startAt : Scroll * 12,
	 endAt : Scroll * 19,
	 onEndAnimate : function(anim) {
	 },
	 keyframes : [{
	 position : 0,
	 properties : {
	 "right" : 0
	 }
	 }, {
	 position : 1,
	 ease : TWEEN.Easing.Linear.EaseNone,
	 properties : {
	 "right" : -(GetWidth() - 200)
	 }
	 }]
	 };
	 Animation[Animation.length] = {
	 selector : '#Block2',
	 startAt : Scroll * 16,
	 endAt : Scroll * 22,
	 onEndAnimate : function(anim) {
	 },
	 keyframes : [{
	 position : 0,
	 properties : {
	 "left" : (GetWidth())
	 }
	 }, {
	 position : 1,
	 ease : TWEEN.Easing.Linear.EaseNone,
	 properties : {
	 "left" : -(GetWidth() * 3 + (GetWidth() / 5))
	 }
	 }]
	 };
	 Animation[Animation.length] = {
	 selector : '#Zoomer4In',
	 startAt : Scroll * 16,
	 endAt : Scroll * 22,
	 onEndAnimate : function(anim) {
	 },
	 keyframes : [{
	 position : 0,
	 properties : {
	 "right" : 0
	 }
	 }, {
	 position : 1,
	 ease : TWEEN.Easing.Linear.EaseNone,
	 properties : {
	 "right" : -(GetWidth())
	 }
	 }]
	 };
	 Animation[Animation.length] = {
	 selector : '#Zoomer5In',
	 startAt : Scroll * 16,
	 endAt : Scroll * 24,
	 onEndAnimate : function(anim) {
	 },
	 keyframes : [{
	 position : 0,
	 properties : {
	 "right" : 0
	 }
	 }, {
	 position : 1,
	 ease : TWEEN.Easing.Linear.EaseNone,
	 properties : {
	 "right" : -(GetWidth())
	 }
	 }]
	 };
	 */
};

var FinishAnimation = function() {
	FinishAnimationRun = true;
	MoveFireblade("left");
};

window.onload = function() {
	AnimationInit();
};
