jQuery.easing['jswing'] = jQuery.easing['swing'];
jQuery.extend(jQuery.easing, {
    def: 'easeOutQuad',
    swing: function (x, t, b, c, d) {
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeInQuad: function (x, t, b, c, d) {
        return c * (t /= d) * t + b;
    },
    easeOutQuad: function (x, t, b, c, d) {
        return -c * (t /= d) * (t - 2) + b;
    },
    easeInOutQuad: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t + b;
        return -c / 2 * ((--t) * (t - 2) - 1) + b;
    },
    easeInCubic: function (x, t, b, c, d) {
        return c * (t /= d) * t * t + b;
    },
    easeOutCubic: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    },
    easeInOutCubic: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    },
    easeInQuart: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t + b;
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    },
    easeInQuint: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t * t + b;
    },
    easeOutQuint: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    },
    easeInOutQuint: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
    },
    easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    },
    easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t / d * (Math.PI / 2)) + b;
    },
    easeInOutSine: function (x, t, b, c, d) {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    },
    easeInExpo: function (x, t, b, c, d) {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    },
    easeOutExpo: function (x, t, b, c, d) {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    },
    easeInOutExpo: function (x, t, b, c, d) {
        if (t == 0) return b;
        if (t == d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    },
    easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    },
    easeInOutCirc: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    },
    easeInElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    },
    easeOutElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    },
    easeInOutElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d / 2) == 2) return b + c;
        if (!p) p = d * (.3 * 1.5);
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a); if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
    },
    easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    },
    easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    },
    easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    },
    easeInBounce: function (x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
    },
    easeOutBounce: function (x, t, b, c, d) {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        } else if (t < (2 / 2.75)) {
            return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
        } else if (t < (2.5 / 2.75)) {
            return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
        } else {
            return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
        }
    },
    easeInOutBounce: function (x, t, b, c, d) {
        if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
    }
});; // Modernizr v1.7  www.modernizr.com
window.Modernizr = function (a, b, c) {
    function G() {
        e.input = function (a) {
            for (var b = 0, c = a.length; b < c; b++) t[a[b]] = !!(a[b] in l);
            return t
        }("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), e.inputtypes = function (a) {
            for (var d = 0, e, f, h, i = a.length; d < i; d++) l.setAttribute("type", f = a[d]), e = l.type !== "text", e && (l.value = m, l.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(f) && l.style.WebkitAppearance !== c ? (g.appendChild(l), h = b.defaultView, e = h.getComputedStyle && h.getComputedStyle(l, null).WebkitAppearance !== "textfield" && l.offsetHeight !== 0, g.removeChild(l)) : /^(search|tel)$/.test(f) || (/^(url|email)$/.test(f) ? e = l.checkValidity && l.checkValidity() === !1 : /^color$/.test(f) ? (g.appendChild(l), g.offsetWidth, e = l.value != m, g.removeChild(l)) : e = l.value != m)), s[a[d]] = !!e;
            return s
        }("search tel url email datetime date month week time datetime-local number range color".split(" "))
    }

    function F(a, b) {
        var c = a.charAt(0).toUpperCase() + a.substr(1),
            d = (a + " " + p.join(c + " ") + c).split(" ");
        return !!E(d, b)
    }

    function E(a, b) {
        for (var d in a)
            if (k[a[d]] !== c && (!b || b(a[d], j))) return !0
    }

    function D(a, b) {
        return ("" + a).indexOf(b) !== -1
    }

    function C(a, b) {
        return typeof a === b
    }

    function B(a, b) {
        return A(o.join(a + ";") + (b || ""))
    }

    function A(a) {
        k.cssText = a
    }
    var d = "1.7",
        e = {},
        f = !0,
        g = b.documentElement,
        h = b.head || b.getElementsByTagName("head")[0],
        i = "modernizr",
        j = b.createElement(i),
        k = j.style,
        l = b.createElement("input"),
        m = ":)",
        n = Object.prototype.toString,
        o = " -webkit- -moz- -o- -ms- -khtml- ".split(" "),
        p = "Webkit Moz O ms Khtml".split(" "),
        q = {
            svg: "http://www.w3.org/2000/svg"
        },
        r = {},
        s = {},
        t = {},
        u = [],
        v, w = function (a) {
            var c = b.createElement("style"),
                d = b.createElement("div"),
                e;
            c.textContent = a + "{#modernizr{height:3px}}", h.appendChild(c), d.id = "modernizr", g.appendChild(d), e = d.offsetHeight === 3, c.parentNode.removeChild(c), d.parentNode.removeChild(d);
            return !!e
        },
        x = function () {
            function d(d, e) {
                e = e || b.createElement(a[d] || "div");
                var f = (d = "on" + d) in e;
                f || (e.setAttribute || (e = b.createElement("div")), e.setAttribute && e.removeAttribute && (e.setAttribute(d, ""), f = C(e[d], "function"), C(e[d], c) || (e[d] = c), e.removeAttribute(d))), e = null;
                return f
            }
            var a = {
                select: "input",
                change: "input",
                submit: "form",
                reset: "form",
                error: "img",
                load: "img",
                abort: "img"
            };
            return d
        }(),
        y = ({}).hasOwnProperty,
        z;
    C(y, c) || C(y.call, c) ? z = function (a, b) {
        return b in a && C(a.constructor.prototype[b], c)
    } : z = function (a, b) {
        return y.call(a, b)
    }, r.flexbox = function () {
        function c(a, b, c, d) {
            a.style.cssText = o.join(b + ":" + c + ";") + (d || "")
        }

        function a(a, b, c, d) {
            b += ":", a.style.cssText = (b + o.join(c + ";" + b)).slice(0, -b.length) + (d || "")
        }
        var d = b.createElement("div"),
            e = b.createElement("div");
        a(d, "display", "box", "width:42px;padding:0;"), c(e, "box-flex", "1", "width:10px;"), d.appendChild(e), g.appendChild(d);
        var f = e.offsetWidth === 42;
        d.removeChild(e), g.removeChild(d);
        return f
    }, r.canvas = function () {
        var a = b.createElement("canvas");
        return a.getContext && a.getContext("2d")
    }, r.canvastext = function () {
        return e.canvas && C(b.createElement("canvas").getContext("2d").fillText, "function")
    }, r.webgl = function () {
        return !!a.WebGLRenderingContext
    }, r.touch = function () {
        return "ontouchstart" in a || w("@media (" + o.join("touch-enabled),(") + "modernizr)")
    }, r.geolocation = function () {
        return !!navigator.geolocation
    }, r.postmessage = function () {
        return !!a.postMessage
    }, r.websqldatabase = function () {
        var b = !!a.openDatabase;
        return b
    }, r.indexedDB = function () {
        for (var b = -1, c = p.length; ++b < c;) {
            var d = p[b].toLowerCase();
            if (a[d + "_indexedDB"] || a[d + "IndexedDB"]) return !0
        }
        return !1
    }, r.hashchange = function () {
        return x("hashchange", a) && (b.documentMode === c || b.documentMode > 7)
    }, r.history = function () {
        return !!(a.history && history.pushState)
    }, r.draganddrop = function () {
        return x("dragstart") && x("drop")
    }, r.websockets = function () {
        return "WebSocket" in a
    }, r.rgba = function () {
        A("background-color:rgba(150,255,150,.5)");
        return D(k.backgroundColor, "rgba")
    }, r.hsla = function () {
        A("background-color:hsla(120,40%,100%,.5)");
        return D(k.backgroundColor, "rgba") || D(k.backgroundColor, "hsla")
    }, r.multiplebgs = function () {
        A("background:url(//:),url(//:),red url(//:)");
        return (new RegExp("(url\\s*\\(.*?){3}")).test(k.background)
    }, r.backgroundsize = function () {
        return F("backgroundSize")
    }, r.borderimage = function () {
        return F("borderImage")
    }, r.borderradius = function () {
        return F("borderRadius", "", function (a) {
            return D(a, "orderRadius")
        })
    }, r.boxshadow = function () {
        return F("boxShadow")
    }, r.textshadow = function () {
        return b.createElement("div").style.textShadow === ""
    }, r.opacity = function () {
        B("opacity:.55");
        return /^0.55$/.test(k.opacity)
    }, r.cssanimations = function () {
        return F("animationName")
    }, r.csscolumns = function () {
        return F("columnCount")
    }, r.cssgradients = function () {
        var a = "background-image:",
            b = "gradient(linear,left top,right bottom,from(#9f9),to(white));",
            c = "linear-gradient(left top,#9f9, white);";
        A((a + o.join(b + a) + o.join(c + a)).slice(0, -a.length));
        return D(k.backgroundImage, "gradient")
    }, r.cssreflections = function () {
        return F("boxReflect")
    }, r.csstransforms = function () {
        return !!E(["transformProperty", "WebkitTransform", "MozTransform", "OTransform", "msTransform"])
    }, r.csstransforms3d = function () {
        var a = !!E(["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"]);
        a && "webkitPerspective" in g.style && (a = w("@media (" + o.join("transform-3d),(") + "modernizr)"));
        return a
    }, r.csstransitions = function () {
        return F("transitionProperty")
    }, r.fontface = function () {
        var a, c, d = h || g,
            e = b.createElement("style"),
            f = b.implementation || {
                hasFeature: function () {
                    return !1
                }
            };
        e.type = "text/css", d.insertBefore(e, d.firstChild), a = e.sheet || e.styleSheet;
        var i = f.hasFeature("CSS2", "") ? function (b) {
            if (!a || !b) return !1;
            var c = !1;
            try {
                a.insertRule(b, 0), c = /src/i.test(a.cssRules[0].cssText), a.deleteRule(a.cssRules.length - 1)
            } catch (d) {}
            return c
        } : function (b) {
            if (!a || !b) return !1;
            a.cssText = b;
            return a.cssText.length !== 0 && /src/i.test(a.cssText) && a.cssText.replace(/\r+|\n+/g, "").indexOf(b.split(" ")[0]) === 0
        };
        c = i('@font-face { font-family: "font"; src: url(data:,); }'), d.removeChild(e);
        return c
    }, r.video = function () {
        var a = b.createElement("video"),
            c = !!a.canPlayType;
        if (c) {
            c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"');
            var d = 'video/mp4; codecs="avc1.42E01E';
            c.h264 = a.canPlayType(d + '"') || a.canPlayType(d + ', mp4a.40.2"'), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"')
        }
        return c
    }, r.audio = function () {
        var a = b.createElement("audio"),
            c = !!a.canPlayType;
        c && (c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"'), c.mp3 = a.canPlayType("audio/mpeg;"), c.wav = a.canPlayType('audio/wav; codecs="1"'), c.m4a = a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;"));
        return c
    }, r.localstorage = function () {
        try {
            return !!localStorage.getItem
        } catch (a) {
            return !1
        }
    }, r.sessionstorage = function () {
        try {
            return !!sessionStorage.getItem
        } catch (a) {
            return !1
        }
    }, r.webWorkers = function () {
        return !!a.Worker
    }, r.applicationcache = function () {
        return !!a.applicationCache
    }, r.svg = function () {
        return !!b.createElementNS && !!b.createElementNS(q.svg, "svg").createSVGRect
    }, r.inlinesvg = function () {
        var a = b.createElement("div");
        a.innerHTML = "<svg/>";
        return (a.firstChild && a.firstChild.namespaceURI) == q.svg
    }, r.smil = function () {
        return !!b.createElementNS && /SVG/.test(n.call(b.createElementNS(q.svg, "animate")))
    }, r.svgclippaths = function () {
        return !!b.createElementNS && /SVG/.test(n.call(b.createElementNS(q.svg, "clipPath")))
    };
    for (var H in r) z(r, H) && (v = H.toLowerCase(), e[v] = r[H](), u.push((e[v] ? "" : "no-") + v));
    e.input || G(), e.crosswindowmessaging = e.postmessage, e.historymanagement = e.history, e.addTest = function (a, b) {
        a = a.toLowerCase();
        if (!e[a]) {
            b = !!b(), g.className += " " + (b ? "" : "no-") + a, e[a] = b;
            return e
        }
    }, A(""), j = l = null, f && a.attachEvent && function () {
        var a = b.createElement("div");
        a.innerHTML = "<elem></elem>";
        return a.childNodes.length !== 1
    }() && function (a, b) {
        function p(a, b) {
            var c = -1,
                d = a.length,
                e, f = [];
            while (++c < d) e = a[c], (b = e.media || b) != "screen" && f.push(p(e.imports, b), e.cssText);
            return f.join("")
        }

        function o(a) {
            var b = -1;
            while (++b < e) a.createElement(d[b])
        }
        var c = "abbr|article|aside|audio|canvas|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
            d = c.split("|"),
            e = d.length,
            f = new RegExp("(^|\\s)(" + c + ")", "gi"),
            g = new RegExp("<(/*)(" + c + ")", "gi"),
            h = new RegExp("(^|[^\\n]*?\\s)(" + c + ")([^\\n]*)({[\\n\\w\\W]*?})", "gi"),
            i = b.createDocumentFragment(),
            j = b.documentElement,
            k = j.firstChild,
            l = b.createElement("body"),
            m = b.createElement("style"),
            n;
        o(b), o(i), k.insertBefore(m, k.firstChild), m.media = "print", a.attachEvent("onbeforeprint", function () {
            var a = -1,
                c = p(b.styleSheets, "all"),
                k = [],
                o;
            n = n || b.body;
            while ((o = h.exec(c)) != null) k.push((o[1] + o[2] + o[3]).replace(f, "$1.iepp_$2") + o[4]);
            m.styleSheet.cssText = k.join("\n");
            while (++a < e) {
                var q = b.getElementsByTagName(d[a]),
                    r = q.length,
                    s = -1;
                while (++s < r) q[s].className.indexOf("iepp_") < 0 && (q[s].className += " iepp_" + d[a])
            }
            i.appendChild(n), j.appendChild(l), l.className = n.className, l.innerHTML = n.innerHTML.replace(g, "<$1font")
        }), a.attachEvent("onafterprint", function () {
            l.innerHTML = "", j.removeChild(l), j.appendChild(n), m.styleSheet.cssText = ""
        })
    }(a, b), e._enableHTML5 = f, e._version = d, g.className = g.className.replace(/\bno-js\b/, "") + " js " + u.join(" ");
    return e
}(this, this.document);
(function ($) {
    var container = null;
    var allLIs = '',
        containerStr = '';
    var element = this;
    var _bgStretcherPause = false;
    var _bgStretcherAction = false;
    var _bgStretcherTm = null;
    var random_line = new Array();
    var random_temp = new Array();
    var r_image = 0;
    var swf_mode = false;
    var img_options = new Array();
    $.fn.bgStretcher = function (settings) {
        if ($('.bgstretcher-page').length || $('.bgstretcher-area').length) {
            if (typeof (console) !== 'undefined' && console != null) console.log('More than one bgStretcher');
            return false;
        }
        settings = $.extend({}, $.fn.bgStretcher.defaults, settings);
        $.fn.bgStretcher.settings = settings;

        function _build(body_content) {
            if (!settings.images.length) {
                return;
            }
            _genHtml(body_content);
            containerStr = '#' + settings.imageContainer;
            container = $(containerStr);
            allLIs = '#' + settings.imageContainer + ' LI';
            $(allLIs).hide().css({
                'z-index': 1
            });
            if (!container.length) {
                return;
            }
            $(window).resize(function () {
                _resize(body_content)
            });
            _resize(body_content);
            var stratElement = 0;
            if (settings.transitionEffect == 'simpleSlide') {
                if (settings.sequenceMode == 'random') {
                    if (typeof (console) !== 'undefined' && console != null) {
                        console.log('Effect \'simpleSlide\' don\'t be to use with mode random.');
                        console.log('Mode was automaticly set in normal.');
                    }
                }
                $(allLIs).css({
                    'float': 'left',
                    position: 'static'
                });
                $(allLIs).show();
                if ($.fn.bgStretcher.settings.slideDirection == 'NW' || $.fn.bgStretcher.settings.slideDirection == 'NE') {
                    $.fn.bgStretcher.settings.slideDirection = 'N';
                }
                if ($.fn.bgStretcher.settings.slideDirection == 'SW' || $.fn.bgStretcher.settings.slideDirection == 'SE') {
                    $.fn.bgStretcher.settings.slideDirection = 'S';
                }
                if ($.fn.bgStretcher.settings.slideDirection == 'S' || $.fn.bgStretcher.settings.slideDirection == 'E') {
                    settings.sequenceMode = 'back';
                    $(allLIs).removeClass('bgs-current');
                    $(allLIs).eq($(allLIs).length - $.fn.bgStretcher.settings.startElementIndex - 1).addClass('bgs-current');
                    if ($.fn.bgStretcher.settings.slideDirection == 'E') {
                        l = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * $(containerStr).width() * (-1);
                        t = 0;
                    } else {
                        t = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * $(containerStr).height() * (-1);
                        l = 0;
                    }
                    $(containerStr + ' UL').css({
                        left: l + 'px',
                        top: t + 'px'
                    });
                } else {
                    settings.sequenceMode = 'normal';
                    if ($.fn.bgStretcher.settings.startElementIndex != 0) {
                        if ($.fn.bgStretcher.settings.slideDirection == 'N') {
                            t = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * $(containerStr).height() * (-1);
                            l = 0;
                        } else {
                            l = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * $(containerStr).width() * (-1);
                            t = 0;
                            console.log(l);
                        }
                        $(containerStr + ' UL').css({
                            left: l + 'px',
                            top: t + 'px'
                        });
                    }
                }
            }
            if ($(settings.buttonNext).length || $(settings.buttonPrev).length || $(settings.pagination).length) {
                if (settings.sequenceMode == 'random') {
                    if (typeof (console) !== 'undefined' && console != null) {
                        console.log('Don\'t use random mode width prev-button, next-button and pagination.');
                    }
                } else {
                    if ($(settings.buttonPrev).length) {
                        $(settings.buttonPrev).addClass('bgStretcherNav bgStretcherNavPrev');
                        $(settings.buttonPrev).click(function () {
                            $.fn.bgStretcher.buttonSlide('prev');
                        });
                    }
                    if ($(settings.buttonNext).length) {
                        $(settings.buttonNext).addClass('bgStretcherNav bgStretcherNavNext');
                        $(settings.buttonNext).click(function () {
                            $.fn.bgStretcher.buttonSlide('next');
                        });
                    }
                    if ($(settings.pagination).length) {
                        $.fn.bgStretcher.pagination();
                    }
                }
            }
            if (settings.sequenceMode == 'random') {
                var i = Math.floor(Math.random() * $(allLIs).length);
                $.fn.bgStretcher.buildRandom(i);
                if (settings.transitionEffect != 'simpleSlide') {
                    $.fn.bgStretcher.settings.startElementIndex = i;
                }
                stratElement = i;
            } else {
                if ($.fn.bgStretcher.settings.startElementIndex > ($(allLIs).length - 1)) $.fn.bgStretcher.settings.startElementIndex = 0;
                stratElement = $.fn.bgStretcher.settings.startElementIndex;
                if (settings.transitionEffect == 'simpleSlide') {
                    if ($.fn.bgStretcher.settings.slideDirection == 'S' || $.fn.bgStretcher.settings.slideDirection == 'E') {
                        stratElement = $(allLIs).length - 1 - $.fn.bgStretcher.settings.startElementIndex;
                    }
                }
            }
            $(allLIs).eq(stratElement).show().addClass('bgs-current');
            $.fn.bgStretcher.loadImg($(allLIs).eq(stratElement));
            if (settings.slideShow && $(allLIs).length > 1) {
                _bgStretcherTm = setTimeout('jQuery.fn.bgStretcher.slideShow(\'' + jQuery.fn.bgStretcher.settings.sequenceMode + '\', -1)', settings.nextSlideDelay);
            }
        };

        function _resize(body_content) {
            var winW = 0;
            var winH = 0;
            var contH = 0;
            var contW = 0;
            if ($('BODY').hasClass('bgStretcher-container')) {
                winW = $(window).width();
                winH = $(window).height();
                if (($.browser.msie) && (parseInt(jQuery.browser.version) == 6)) {
                    $(window).scroll(function () {
                        $('#' + settings.imageContainer).css('top', $(window).scrollTop());
                    });
                }
            } else {
                $('.bgstretcher').css('position', 'absolute').css('top', '0px');
                winW = $(window).width();
                winH = $(window).height();
            }
            var imgW = 0,
                imgH = 0;
            var leftSpace = 0;
            if (settings.maxWidth != 'auto') {
                if (winW > settings.maxWidth) {
                    leftSpace = (winW - settings.maxWidth) / 2;
                    contW = settings.maxWidth;
                } else contW = winW;
            } else contW = winW; if (settings.maxHeight != 'auto') {
                if (winH > settings.maxHeight) {
                    contH = settings.maxHeight;
                } else contH = winH;
            } else contH = winH;
            container.width(contW);
            container.height(contH);
            if (!settings.resizeProportionally) {
                imgW = contH;
                imgH = contH;
            } else {
                var initW = settings.imageWidth,
                    initH = settings.imageHeight;
                var ratio = initH / initW;
                imgW = contW;
                imgH = Math.round(contW * ratio);
                if (imgH < contH) {
                    imgH = contH;
                    imgW = Math.round(imgH / ratio);
                }
            }
            var mar_left = 0;
            var mar_top = 0;
            var anchor_arr;
            if ($.fn.bgStretcher.settings.anchoring != 'left top') {
                anchor_arr = ($.fn.bgStretcher.settings.anchoring).split(' ');
                if (anchor_arr[0] == 'right') {
                    mar_left = (winW - contW);
                } else {
                    if (anchor_arr[0] == 'center') mar_left = Math.round((winW - contW) / 2);
                }
                if (anchor_arr[1] == 'bottom') {
                    mar_top = (winH - contH);
                } else {
                    if (anchor_arr[1] == 'center') {
                        mar_top = Math.round((winH - contH) / 2);
                    }
                }
                container.css('marginLeft', mar_left + 'px').css('marginTop', mar_top + 'px');
            }
            mar_left = 0;
            mar_top = 0;
            if ($.fn.bgStretcher.settings.anchoringImg != 'left top') {
                anchor_arr = ($.fn.bgStretcher.settings.anchoringImg).split(' ');
                if (anchor_arr[0] == 'right') {
                    mar_left = (contW - imgW);
                } else {
                    if (anchor_arr[0] == 'center') mar_left = Math.round((contW - imgW) / 2);
                }
                if (anchor_arr[1] == 'bottom') {
                    mar_top = (contH - imgH);
                } else {
                    if (anchor_arr[1] == 'center') {
                        mar_top = Math.round((contH - imgH) / 2);
                    }
                }
            }
            img_options['mar_left'] = mar_left;
            img_options['mar_top'] = mar_top;
            if (container.find('LI:first').hasClass('swf-mode')) {
                var path_swf = container.find('LI:first').html();
                container.find('LI:first').html('<div id="bgstretcher-flash">&nbsp;</div>');
                var header = new SWFObject('flash/stars.swf', 'flash-obj', contW, contH, '9');
                header.addParam('wmode', 'transparent');
                header.write('bgstretcher-flash');
            };
            img_options['imgW'] = imgW;
            img_options['imgH'] = imgH;
            if (!settings.resizeAnimate) {
                container.children('UL').children('LI.img-loaded').find('IMG').css({
                    'marginLeft': img_options["mar_left"] + 'px',
                    'marginTop': img_options["mar_top"] + 'px'
                });
                container.children('UL').children('LI.img-loaded').find('IMG').css({
                    'width': img_options["imgW"] + 'px',
                    'height': img_options["imgH"] + 'px'
                });
            } else {
                container.children('UL').children('LI.img-loaded').find('IMG').animate({
                    'marginLeft': img_options["mar_left"] + 'px',
                    'marginTop': img_options["mar_top"] + 'px'
                }, 'normal');
                container.children('UL').children('LI.img-loaded').find('IMG').animate({
                    'width': img_options["imgW"] + 'px',
                    'height': img_options["imgH"] + 'px'
                }, 'normal');
            }
            $(allLIs).width(container.width()).height(container.height());
            if ($.fn.bgStretcher.settings.transitionEffect == 'simpleSlide') {
                if ($.fn.bgStretcher.settings.slideDirection == 'W' || $.fn.bgStretcher.settings.slideDirection == 'E') {
                    container.children('UL').width(container.width() * $(allLIs).length).height(container.height());
                    if ($(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) != -1) {
                        l = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * container.width() * (-1);
                        container.children('UL').css({
                            left: l + 'px'
                        });
                    }
                } else {
                    container.children('UL').height(container.height() * $(allLIs).length).width(container.width());
                    if ($(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) != -1) {
                        t = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * $(containerStr).height() * (-1);
                        container.children('UL').css({
                            top: t + 'px'
                        });
                    }
                }
            }
        };

        function _genHtml(body_content) {
            var code = '';
            var cur_bgstretcher;
            body_content.each(function () {
                $(this).wrapInner('<div class="bgstretcher-page" />').wrapInner('<div class="bgstretcher-area" />');
                code = '<div id="' + settings.imageContainer + '" class="bgstretcher"><ul>';
                if (settings.images.length) {
                    var ext = settings.images[0].split('.');
                    ext = ext[ext.length - 1];
                    if (ext != 'swf') {
                        var ind = 0;
                        for (i = 0; i < settings.images.length; i++) {
                            if (settings.transitionEffect == 'simpleSlide' && settings.sequenceMode == 'back')
                                ind = settings.images.length - 1 - i;
                            else ind = i; if ($.fn.bgStretcher.settings.preloadImg) {
                                code += '<li><span class="image-path">' + settings.images[ind] + '</span></li>';
                            } else {
                                code += '<li class="img-loaded"><img src="' + settings.images[ind] + '" alt="" /></li>';
                            }
                        }
                    } else {
                        code += '<li class="swf-mode">' + settings.images[0] + '</li>';
                    }
                }
                code += '</ul></div>';
                cur_bgstretcher = $(this).children('.bgstretcher-area');
                $(code).prependTo(cur_bgstretcher);
                cur_bgstretcher.css({
                    position: 'relative'
                });
                cur_bgstretcher.children('.bgstretcher-page').css({
                    'position': 'relative',
                    'z-index': 3
                });
            });
        };
        this.addClass('bgStretcher-container');
        _build(this);
    };
    $.fn.bgStretcher.loadImg = function (obj) {
        if (obj.hasClass('img-loaded')) return true;
        obj.find('SPAN.image-path').each(function () {
            var imgsrc = $(this).html();
            var imgalt = '';
            var parent = $(this).parent();
            var img = new Image();
            $(img).load(function () {
                $(this).hide();
                parent.prepend(this);
                $(this).fadeIn('100');
            }).error(function () {}).attr('src', imgsrc).attr('alt', imgalt);
            $(img).css({
                'marginLeft': img_options["mar_left"] + 'px',
                'marginTop': img_options["mar_top"] + 'px'
            });
            $(img).css({
                'width': img_options["imgW"] + 'px',
                'height': img_options["imgH"] + 'px'
            });
        });
        obj.addClass('img-loaded');
        return true;
    }
    $.fn.bgStretcher.play = function () {
        _bgStretcherPause = false;
        $.fn.bgStretcher._clearTimeout();
        $.fn.bgStretcher.slideShow($.fn.bgStretcher.settings.sequenceMode, -1);
    };
    $.fn.bgStretcher._clearTimeout = function () {
        if (_bgStretcherTm != null) {
            clearTimeout(_bgStretcherTm);
            _bgStretcherTm = null;
        }
    }
    $.fn.bgStretcher.pause = function () {
        _bgStretcherPause = true;
        $.fn.bgStretcher._clearTimeout();
    };
    $.fn.bgStretcher.sliderDestroy = function () {
        var cont = $('.bgstretcher-page').html();
        $('.bgStretcher-container').html('').html(cont).removeClass('bgStretcher-container');
        $.fn.bgStretcher._clearTimeout();
        _bgStretcherPause = false;
    }
    $.fn.bgStretcher.slideShow = function (sequence_mode, index_next) {
        _bgStretcherAction = true;
        if ($(allLIs).length < 2) return true;
        var current = $(containerStr + ' LI.bgs-current');
        var next;
        $(current).stop(true, true);
        if (index_next == -1) {
            switch (sequence_mode) {
            case 'back':
                next = current.prev();
                if (!next.length) {
                    next = $(containerStr + ' LI:last');
                }
                break;
            case 'random':
                if (r_image == $(containerStr + ' LI').length) {
                    $.fn.bgStretcher.buildRandom(random_line[$(containerStr + ' LI').length - 1]);
                    r_image = 0;
                }
                next = $(containerStr + ' LI').eq(random_line[r_image]);
                r_image++;
                break;
            default:
                next = current.next();
                if (!next.length) {
                    next = $(containerStr + ' LI:first');
                }
            }
        } else {
            next = $(containerStr + ' LI').eq(index_next);
        }
        $(containerStr + ' LI').removeClass('bgs-current');
        $.fn.bgStretcher.loadImg(next);
        next.addClass('bgs-current');
        switch ($.fn.bgStretcher.settings.transitionEffect) {
        case 'fade':
            $.fn.bgStretcher.effectFade(current, next);
            break;
        case 'simpleSlide':
            $.fn.bgStretcher.simpleSlide();
            break;
        case 'superSlide':
            $.fn.bgStretcher.superSlide(current, next, sequence_mode);
            break;
        default:
            $.fn.bgStretcher.effectNone(current, next);
        }
        if ($($.fn.bgStretcher.settings.pagination).find('LI').length) {
            $($.fn.bgStretcher.settings.pagination).find('LI.showPage').removeClass('showPage');
            $($.fn.bgStretcher.settings.pagination).find('LI').eq($(containerStr + ' LI').index($(containerStr + ' LI.bgs-current'))).addClass('showPage');
        }
        if ($.fn.bgStretcher.settings.callbackfunction) {
            if (typeof $.fn.bgStretcher.settings.callbackfunction == 'function')
                $.fn.bgStretcher.settings.callbackfunction.call();
        }
        if (!_bgStretcherPause) {
            _bgStretcherTm = setTimeout('jQuery.fn.bgStretcher.slideShow(\'' + jQuery.fn.bgStretcher.settings.sequenceMode + '\', -1)', jQuery.fn.bgStretcher.settings.nextSlideDelay);
        }
    };
    $.fn.bgStretcher.effectNone = function (current, next) {
        next.show();
        current.hide();
        _bgStretcherAction = false;
    };
    $.fn.bgStretcher.effectFade = function (current, next) {
        next.fadeIn($.fn.bgStretcher.settings.slideShowSpeed);
        current.fadeOut($.fn.bgStretcher.settings.slideShowSpeed, function () {
            _bgStretcherAction = false;
        });
    };
    $.fn.bgStretcher.simpleSlide = function () {
        var t, l;
        switch ($.fn.bgStretcher.settings.slideDirection) {
        case 'N':
        case 'S':
            t = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * $(containerStr).height() * (-1);
            l = 0;
            break;
        default:
            l = $(containerStr + ' LI').index($(containerStr + ' LI.bgs-current')) * $(containerStr).width() * (-1);
            t = 0;
        }
        $(containerStr + ' UL').animate({
            left: l + 'px',
            top: t + 'px'
        }, $.fn.bgStretcher.settings.slideShowSpeed, function () {
            _bgStretcherAction = false;
        });
    };
    $.fn.bgStretcher.superSlide = function (current, next, sequence_mode) {
        var t, l;
        switch ($.fn.bgStretcher.settings.slideDirection) {
        case 'S':
            t = $(containerStr).height();
            l = 0;
            break;
        case 'E':
            t = 0;
            l = $(containerStr).width();
            break;
        case 'W':
            t = 0;
            l = $(containerStr).width() * (-1);
            break;
        case 'NW':
            t = $(containerStr).height() * (-1);
            l = $(containerStr).width() * (-1);
            break;
        case 'NE':
            t = $(containerStr).height() * (-1);
            l = $(containerStr).width();
            break;
        case 'SW':
            t = $(containerStr).height();
            l = $(containerStr).width() * (-1);
            break;
        case 'SE':
            t = $(containerStr).height();
            l = $(containerStr).width();
            break;
        default:
            t = $(containerStr).height() * (-1);
            l = 0;
        }
        if (sequence_mode == 'back') {
            next.css({
                'z-index': 2,
                top: t + 'px',
                left: l + 'px'
            });
            next.show();
            next.animate({
                left: '0px',
                top: '0px'
            }, $.fn.bgStretcher.settings.slideShowSpeed, function () {
                current.hide();
                $(this).css({
                    'z-index': 1
                });
                _bgStretcherAction = false;
            });
        } else {
            current.css('z-index', 2);
            next.show();
            current.animate({
                left: l + 'px',
                top: t + 'px'
            }, $.fn.bgStretcher.settings.slideShowSpeed, function () {
                $(this).hide().css({
                    'z-index': 1,
                    top: '0px',
                    left: '0px'
                });
                _bgStretcherAction = false;
            });
        }
    };
    $.fn.bgStretcher.buildRandom = function (el_not) {
        var l = $(allLIs).length;
        var i, j, rt;
        for (i = 0; i < l; i++) {
            random_line[i] = i;
            random_temp[i] = Math.random() * l;
        }
        for (i = 0; i < l; i++) {
            for (j = 0; j < (l - i - 1); j++) {
                if (random_temp[j] > random_temp[j + 1]) {
                    rt = random_temp[j];
                    random_temp[j] = random_temp[j + 1];
                    random_temp[j + 1] = rt;
                    rt = random_line[j];
                    random_line[j] = random_line[j + 1];
                    random_line[j + 1] = rt;
                }
            }
        }
        if (random_line[0] == el_not) {
            rt = random_line[0];
            random_line[0] = random_line[l - 1];
            random_line[l - 1] = rt;
        }
    };
    $.fn.bgStretcher.buttonSlide = function (button_point) {
        if (_bgStretcherAction || ($(allLIs).length < 2)) return false;
        var mode = '';
        if (button_point == 'prev') {
            mode = 'back';
            if ($.fn.bgStretcher.settings.sequenceMode == 'back') mode = 'normal';
        } else {
            mode = $.fn.bgStretcher.settings.sequenceMode;
        }
        $(allLIs).stop(true, true);
        $.fn.bgStretcher._clearTimeout();
        $.fn.bgStretcher.slideShow(mode, -1);
        return false;
    };
    $.fn.bgStretcher.pagination = function () {
        var l = $(allLIs).length;
        var output = '';
        var i = 0;
        if (l > 0) {
            output += '<ul>';
            for (i = 0; i < l; i++) {
                output += '<li><a href="javascript:;">' + (i + 1) + '</a></li>';
            }
            output += '</ul>';
            $($.fn.bgStretcher.settings.pagination).html(output);
            $($.fn.bgStretcher.settings.pagination).find('LI:first').addClass('showPage');
            $($.fn.bgStretcher.settings.pagination).find('A').click(function () {
                if ($(this).parent().hasClass('showPage')) return false;
                $(allLIs).stop(true, true);
                $.fn.bgStretcher._clearTimeout();
                $.fn.bgStretcher.slideShow($.fn.bgStretcher.settings.sequenceMode, $($.fn.bgStretcher.settings.pagination).find('A').index($(this)));
                return false;
            });
        }
        return false;
    }
    $.fn.bgStretcher.defaults = {
        imageContainer: 'bgstretcher',
        resizeProportionally: true,
        resizeAnimate: false,
        images: [],
        imageWidth: 1024,
        imageHeight: 768,
        maxWidth: 'auto',
        maxHeight: 'auto',
        nextSlideDelay: 3000,
        slideShowSpeed: 'normal',
        slideShow: true,
        transitionEffect: 'fade',
        slideDirection: 'N',
        sequenceMode: 'normal',
        buttonPrev: '',
        buttonNext: '',
        pagination: '',
        anchoring: 'left top',
        anchoringImg: 'left top',
        preloadImg: false,
        startElementIndex: 0,
        callbackfunction: null
    };
    $.fn.bgStretcher.settings = {};
})(jQuery);;
if (jQuery)(function ($) {
    $.extend($.fn, {
        selectBox: function (method, data) {
            var typeTimer, typeSearch = '',
                isMac = navigator.platform.match(/mac/i);
            var init = function (select, data) {
                var options;
                if (navigator.userAgent.match(/iPad|iPhone|Android|IEMobile|BlackBerry/i)) return false;
                if (select.tagName.toLowerCase() !== 'select') return false;
                select = $(select);
                if (select.data('selectBox-control')) return false;
                var control = $('<a class="selectBox" />'),
                    inline = select.attr('multiple') || parseInt(select.attr('size')) > 1;
                var settings = data || {};
                control.width(select.outerWidth()).addClass(select.attr('class')).attr('title', select.attr('title') || '').attr('tabindex', parseInt(select.attr('tabindex'))).css('display', 'inline-block').bind('focus.selectBox', function () {
                    if (this !== document.activeElement && document.body !== document.activeElement) $(document.activeElement).blur();
                    if (control.hasClass('selectBox-active')) return;
                    control.addClass('selectBox-active');
                    select.trigger('focus');
                }).bind('blur.selectBox', function () {
                    if (!control.hasClass('selectBox-active')) return;
                    control.removeClass('selectBox-active');
                    select.trigger('blur');
                });
                if (!$(window).data('selectBox-bindings')) {
                    $(window).data('selectBox-bindings', true).bind('scroll.selectBox', hideMenus).bind('resize.selectBox', hideMenus);
                }
                if (select.attr('disabled')) control.addClass('selectBox-disabled');
                select.bind('click.selectBox', function (event) {
                    control.focus();
                    event.preventDefault();
                });
                if (inline) {
                    options = getOptions(select, 'inline');
                    control.append(options).data('selectBox-options', options).addClass('selectBox-inline selectBox-menuShowing').bind('keydown.selectBox', function (event) {
                        handleKeyDown(select, event);
                    }).bind('keypress.selectBox', function (event) {
                        handleKeyPress(select, event);
                    }).bind('mousedown.selectBox', function (event) {
                        if ($(event.target).is('A.selectBox-inline')) event.preventDefault();
                        if (!control.hasClass('selectBox-focus')) control.focus();
                    }).insertAfter(select);
                    if (!select[0].style.height) {
                        var size = select.attr('size') ? parseInt(select.attr('size')) : 5;
                        var tmp = control.clone().removeAttr('id').css({
                            position: 'absolute',
                            top: '-9999em'
                        }).show().appendTo('body');
                        tmp.find('.selectBox-options').html('<li><a>\u00A0</a></li>');
                        var optionHeight = parseInt(tmp.find('.selectBox-options A:first').html('&nbsp;').outerHeight());
                        tmp.remove();
                        control.height(optionHeight * size);
                    }
                    disableSelection(control);
                } else {
                    var label = $('<span class="selectBox-label" />'),
                        arrow = $('<span class="selectBox-arrow" />');
                    label.attr('class', getLabelClass(select)).text(getLabelText(select));
                    options = getOptions(select, 'dropdown');
                    options.appendTo('BODY');
                    control.data('selectBox-options', options).addClass('selectBox-dropdown').append(label).append(arrow).bind('mousedown.selectBox', function (event) {
                        if (control.hasClass('selectBox-menuShowing')) {
                            hideMenus();
                        } else {
                            event.stopPropagation();
                            options.data('selectBox-down-at-x', event.screenX).data('selectBox-down-at-y', event.screenY);
                            showMenu(select);
                        }
                    }).bind('keydown.selectBox', function (event) {
                        handleKeyDown(select, event);
                    }).bind('keypress.selectBox', function (event) {
                        handleKeyPress(select, event);
                    }).bind('open.selectBox', function (event, triggerData) {
                        if (triggerData && triggerData._selectBox === true) return;
                        showMenu(select);
                    }).bind('close.selectBox', function (event, triggerData) {
                        if (triggerData && triggerData._selectBox === true) return;
                        hideMenus();
                    }).insertAfter(select);
                    var labelWidth = control.width() - arrow.outerWidth() - parseInt(label.css('paddingLeft')) - parseInt(label.css('paddingLeft'));
                    label.width(labelWidth);
                    disableSelection(control);
                }
                select.addClass('selectBox').data('selectBox-control', control).data('selectBox-settings', settings).hide();
            };
            var getOptions = function (select, type) {
                var options;
                var _getOptions = function (select, options) {
                    select.children('OPTION, OPTGROUP').each(function () {
                        if ($(this).is('OPTION')) {
                            if ($(this).length > 0) {
                                generateOptions($(this), options);
                            } else {
                                options.append('<li>\u00A0</li>');
                            }
                        } else {
                            var optgroup = $('<li class="selectBox-optgroup" />');
                            optgroup.text($(this).attr('label'));
                            options.append(optgroup);
                            options = _getOptions($(this), options);
                        }
                    });
                    return options;
                };
                switch (type) {
                case 'inline':
                    options = $('<ul class="selectBox-options" />');
                    options = _getOptions(select, options);
                    options.find('A').bind('mouseover.selectBox', function (event) {
                        addHover(select, $(this).parent());
                    }).bind('mouseout.selectBox', function (event) {
                        removeHover(select, $(this).parent());
                    }).bind('mousedown.selectBox', function (event) {
                        event.preventDefault();
                        if (!select.selectBox('control').hasClass('selectBox-active')) select.selectBox('control').focus();
                    }).bind('mouseup.selectBox', function (event) {
                        hideMenus();
                        selectOption(select, $(this).parent(), event);
                    });
                    disableSelection(options);
                    return options;
                case 'dropdown':
                    options = $('<ul class="selectBox-dropdown-menu selectBox-options" />');
                    options = _getOptions(select, options);
                    options.data('selectBox-select', select).css('display', 'none').appendTo('BODY').find('A').bind('mousedown.selectBox', function (event) {
                        event.preventDefault();
                        if (event.screenX === options.data('selectBox-down-at-x') && event.screenY === options.data('selectBox-down-at-y')) {
                            options.removeData('selectBox-down-at-x').removeData('selectBox-down-at-y');
                            hideMenus();
                        }
                    }).bind('mouseup.selectBox', function (event) {
                        if (event.screenX === options.data('selectBox-down-at-x') && event.screenY === options.data('selectBox-down-at-y')) {
                            return;
                        } else {
                            options.removeData('selectBox-down-at-x').removeData('selectBox-down-at-y');
                        }
                        selectOption(select, $(this).parent());
                        hideMenus();
                    }).bind('mouseover.selectBox', function (event) {
                        addHover(select, $(this).parent());
                    }).bind('mouseout.selectBox', function (event) {
                        removeHover(select, $(this).parent());
                    });
                    var classes = select.attr('class') || '';
                    if (classes !== '') {
                        classes = classes.split(' ');
                        for (var i in classes) options.addClass(classes[i] + '-selectBox-dropdown-menu');
                    }
                    disableSelection(options);
                    return options;
                }
            };
            var getLabelClass = function (select) {
                var selected = $(select).find('OPTION:selected');
                return ('selectBox-label ' + (selected.attr('class') || '')).replace(/\s+$/, '');
            };
            var getLabelText = function (select) {
                var selected = $(select).find('OPTION:selected');
                return selected.text() || '\u00A0';
            };
            var setLabel = function (select) {
                select = $(select);
                var control = select.data('selectBox-control');
                if (!control) return;
                control.find('.selectBox-label').attr('class', getLabelClass(select)).text(getLabelText(select));
            };
            var destroy = function (select) {
                select = $(select);
                var control = select.data('selectBox-control');
                if (!control) return;
                var options = control.data('selectBox-options');
                options.remove();
                control.remove();
                select.removeClass('selectBox').removeData('selectBox-control').data('selectBox-control', null).removeData('selectBox-settings').data('selectBox-settings', null).show();
            };
            var refresh = function (select) {
                select = $(select);
                select.selectBox('options', select.html());
            };
            var showMenu = function (select) {
                select = $(select);
                var control = select.data('selectBox-control'),
                    settings = select.data('selectBox-settings'),
                    options = control.data('selectBox-options');
                if (control.hasClass('selectBox-disabled')) return false;
                hideMenus();
                var borderBottomWidth = isNaN(control.css('borderBottomWidth')) ? 0 : parseInt(control.css('borderBottomWidth'));
                options.width(control.innerWidth()).css({
                    top: control.offset().top + control.outerHeight() - borderBottomWidth,
                    left: control.offset().left
                });
                if (select.triggerHandler('beforeopen')) return false;
                var dispatchOpenEvent = function () {
                    select.triggerHandler('open', {
                        _selectBox: true
                    });
                };
                switch (settings.menuTransition) {
                case 'fade':
                    options.fadeIn(settings.menuSpeed, dispatchOpenEvent);
                    break;
                case 'slide':
                    options.slideDown(settings.menuSpeed, dispatchOpenEvent);
                    break;
                default:
                    options.show(settings.menuSpeed, dispatchOpenEvent);
                    break;
                }
                if (!settings.menuSpeed) dispatchOpenEvent();
                var li = options.find('.selectBox-selected:first');
                keepOptionInView(select, li, true);
                addHover(select, li);
                control.addClass('selectBox-menuShowing');
                $(document).bind('mousedown.selectBox', function (event) {
                    if ($(event.target).parents().andSelf().hasClass('selectBox-options')) return;
                    hideMenus();
                });
            };
            var hideMenus = function () {
                if ($(".selectBox-dropdown-menu:visible").length === 0) return;
                $(document).unbind('mousedown.selectBox');
                $(".selectBox-dropdown-menu").each(function () {
                    var options = $(this),
                        select = options.data('selectBox-select'),
                        control = select.data('selectBox-control'),
                        settings = select.data('selectBox-settings');
                    if (select.triggerHandler('beforeclose')) return false;
                    var dispatchCloseEvent = function () {
                        select.triggerHandler('close', {
                            _selectBox: true
                        });
                    };
                    switch (settings.menuTransition) {
                    case 'fade':
                        options.fadeOut(settings.menuSpeed, dispatchCloseEvent);
                        break;
                    case 'slide':
                        options.slideUp(settings.menuSpeed, dispatchCloseEvent);
                        break;
                    default:
                        options.hide(settings.menuSpeed, dispatchCloseEvent);
                        break;
                    }
                    if (!settings.menuSpeed) dispatchCloseEvent();
                    control.removeClass('selectBox-menuShowing');
                });
            };
            var selectOption = function (select, li, event) {
                select = $(select);
                li = $(li);
                var control = select.data('selectBox-control'),
                    settings = select.data('selectBox-settings');
                if (control.hasClass('selectBox-disabled')) return false;
                if (li.length === 0 || li.hasClass('selectBox-disabled')) return false;
                if (select.attr('multiple')) {
                    if (event.shiftKey && control.data('selectBox-last-selected')) {
                        li.toggleClass('selectBox-selected');
                        var affectedOptions;
                        if (li.index() > control.data('selectBox-last-selected').index()) {
                            affectedOptions = li.siblings().slice(control.data('selectBox-last-selected').index(), li.index());
                        } else {
                            affectedOptions = li.siblings().slice(li.index(), control.data('selectBox-last-selected').index());
                        }
                        affectedOptions = affectedOptions.not('.selectBox-optgroup, .selectBox-disabled');
                        if (li.hasClass('selectBox-selected')) {
                            affectedOptions.addClass('selectBox-selected');
                        } else {
                            affectedOptions.removeClass('selectBox-selected');
                        }
                    } else if ((isMac && event.metaKey) || (!isMac && event.ctrlKey)) {
                        li.toggleClass('selectBox-selected');
                    } else {
                        li.siblings().removeClass('selectBox-selected');
                        li.addClass('selectBox-selected');
                    }
                } else {
                    li.siblings().removeClass('selectBox-selected');
                    li.addClass('selectBox-selected');
                }
                if (control.hasClass('selectBox-dropdown')) {
                    control.find('.selectBox-label').text(li.text());
                }
                var i = 0,
                    selection = [];
                if (select.attr('multiple')) {
                    control.find('.selectBox-selected A').each(function () {
                        selection[i++] = $(this).attr('rel');
                    });
                } else {
                    selection = li.find('A').attr('rel');
                }
                control.data('selectBox-last-selected', li);
                if (select.val() !== selection) {
                    select.val(selection);
                    setLabel(select);
                    select.trigger('change');
                }
                return true;
            };
            var addHover = function (select, li) {
                select = $(select);
                li = $(li);
                var control = select.data('selectBox-control'),
                    options = control.data('selectBox-options');
                options.find('.selectBox-hover').removeClass('selectBox-hover');
                li.addClass('selectBox-hover');
            };
            var removeHover = function (select, li) {
                select = $(select);
                li = $(li);
                var control = select.data('selectBox-control'),
                    options = control.data('selectBox-options');
                options.find('.selectBox-hover').removeClass('selectBox-hover');
            };
            var keepOptionInView = function (select, li, center) {
                if (!li || li.length === 0) return;
                select = $(select);
                var control = select.data('selectBox-control'),
                    options = control.data('selectBox-options'),
                    scrollBox = control.hasClass('selectBox-dropdown') ? options : options.parent(),
                    top = parseInt(li.offset().top - scrollBox.position().top),
                    bottom = parseInt(top + li.outerHeight());
                if (center) {
                    scrollBox.scrollTop(li.offset().top - scrollBox.offset().top + scrollBox.scrollTop() - (scrollBox.height() / 2));
                } else {
                    if (top < 0) {
                        scrollBox.scrollTop(li.offset().top - scrollBox.offset().top + scrollBox.scrollTop());
                    }
                    if (bottom > scrollBox.height()) {
                        scrollBox.scrollTop((li.offset().top + li.outerHeight()) - scrollBox.offset().top + scrollBox.scrollTop() - scrollBox.height());
                    }
                }
            };
            var handleKeyDown = function (select, event) {
                select = $(select);
                var control = select.data('selectBox-control'),
                    options = control.data('selectBox-options'),
                    settings = select.data('selectBox-settings'),
                    totalOptions = 0,
                    i = 0;
                if (control.hasClass('selectBox-disabled')) return;
                switch (event.keyCode) {
                case 8:
                    event.preventDefault();
                    typeSearch = '';
                    break;
                case 9:
                case 27:
                    hideMenus();
                    removeHover(select);
                    break;
                case 13:
                    if (control.hasClass('selectBox-menuShowing')) {
                        selectOption(select, options.find('LI.selectBox-hover:first'), event);
                        if (control.hasClass('selectBox-dropdown')) hideMenus();
                    } else {
                        showMenu(select);
                    }
                    break;
                case 38:
                case 37:
                    event.preventDefault();
                    if (control.hasClass('selectBox-menuShowing')) {
                        var prev = options.find('.selectBox-hover').prev('LI');
                        totalOptions = options.find('LI:not(.selectBox-optgroup)').length;
                        i = 0;
                        while (prev.length === 0 || prev.hasClass('selectBox-disabled') || prev.hasClass('selectBox-optgroup')) {
                            prev = prev.prev('LI');
                            if (prev.length === 0) {
                                if (settings.loopOptions) {
                                    prev = options.find('LI:last');
                                } else {
                                    prev = options.find('LI:first');
                                }
                            }
                            if (++i >= totalOptions) break;
                        }
                        addHover(select, prev);
                        selectOption(select, prev, event);
                        keepOptionInView(select, prev);
                    } else {
                        showMenu(select);
                    }
                    break;
                case 40:
                case 39:
                    event.preventDefault();
                    if (control.hasClass('selectBox-menuShowing')) {
                        var next = options.find('.selectBox-hover').next('LI');
                        totalOptions = options.find('LI:not(.selectBox-optgroup)').length;
                        i = 0;
                        while (next.length === 0 || next.hasClass('selectBox-disabled') || next.hasClass('selectBox-optgroup')) {
                            next = next.next('LI');
                            if (next.length === 0) {
                                if (settings.loopOptions) {
                                    next = options.find('LI:first');
                                } else {
                                    next = options.find('LI:last');
                                }
                            }
                            if (++i >= totalOptions) break;
                        }
                        addHover(select, next);
                        selectOption(select, next, event);
                        keepOptionInView(select, next);
                    } else {
                        showMenu(select);
                    }
                    break;
                }
            };
            var handleKeyPress = function (select, event) {
                select = $(select);
                var control = select.data('selectBox-control'),
                    options = control.data('selectBox-options');
                if (control.hasClass('selectBox-disabled')) return;
                switch (event.keyCode) {
                case 9:
                case 27:
                case 13:
                case 38:
                case 37:
                case 40:
                case 39:
                    break;
                default:
                    if (!control.hasClass('selectBox-menuShowing')) showMenu(select);
                    event.preventDefault();
                    clearTimeout(typeTimer);
                    typeSearch += String.fromCharCode(event.charCode || event.keyCode);
                    options.find('A').each(function () {
                        if ($(this).text().substr(0, typeSearch.length).toLowerCase() === typeSearch.toLowerCase()) {
                            addHover(select, $(this).parent());
                            keepOptionInView(select, $(this).parent());
                            return false;
                        }
                    });
                    typeTimer = setTimeout(function () {
                        typeSearch = '';
                    }, 1000);
                    break;
                }
            };
            var enable = function (select) {
                select = $(select);
                select.attr('disabled', false);
                var control = select.data('selectBox-control');
                if (!control) return;
                control.removeClass('selectBox-disabled');
            };
            var disable = function (select) {
                select = $(select);
                select.attr('disabled', true);
                var control = select.data('selectBox-control');
                if (!control) return;
                control.addClass('selectBox-disabled');
            };
            var setValue = function (select, value) {
                select = $(select);
                select.val(value);
                value = select.val();
                var control = select.data('selectBox-control');
                if (!control) return;
                var settings = select.data('selectBox-settings'),
                    options = control.data('selectBox-options');
                setLabel(select);
                options.find('.selectBox-selected').removeClass('selectBox-selected');
                options.find('A').each(function () {
                    if (typeof (value) === 'object') {
                        for (var i = 0; i < value.length; i++) {
                            if ($(this).attr('rel') == value[i]) {
                                $(this).parent().addClass('selectBox-selected');
                            }
                        }
                    } else {
                        if ($(this).attr('rel') == value) {
                            $(this).parent().addClass('selectBox-selected');
                        }
                    }
                });
                if (settings.change) settings.change.call(select);
            };
            var setOptions = function (select, options) {
                select = $(select);
                var control = select.data('selectBox-control'),
                    settings = select.data('selectBox-settings');
                switch (typeof (data)) {
                case 'string':
                    select.html(data);
                    break;
                case 'object':
                    select.html('');
                    for (var i in data) {
                        if (data[i] === null) continue;
                        if (typeof (data[i]) === 'object') {
                            var optgroup = $('<optgroup label="' + i + '" />');
                            for (var j in data[i]) {
                                optgroup.append('<option value="' + j + '">' + data[i][j] + '</option>');
                            }
                            select.append(optgroup);
                        } else {
                            var option = $('<option value="' + i + '">' + data[i] + '</option>');
                            select.append(option);
                        }
                    }
                    break;
                }
                if (!control) return;
                control.data('selectBox-options').remove();
                var type = control.hasClass('selectBox-dropdown') ? 'dropdown' : 'inline';
                options = getOptions(select, type);
                control.data('selectBox-options', options);
                switch (type) {
                case 'inline':
                    control.append(options);
                    break;
                case 'dropdown':
                    setLabel(select);
                    $("BODY").append(options);
                    break;
                }
            };
            var disableSelection = function (selector) {
                $(selector).css('MozUserSelect', 'none').bind('selectstart', function (event) {
                    event.preventDefault();
                });
            };
            var generateOptions = function (self, options) {
                var li = $('<li />'),
                    a = $('<a />');
                li.addClass(self.attr('class'));
                li.data(self.data());
                a.attr('rel', self.val()).text(self.text());
                li.append(a);
                if (self.attr('disabled')) li.addClass('selectBox-disabled');
                if (self.attr('selected')) li.addClass('selectBox-selected');
                options.append(li);
            };
            switch (method) {
            case 'control':
                return $(this).data('selectBox-control');
            case 'settings':
                if (!data) return $(this).data('selectBox-settings');
                $(this).each(function () {
                    $(this).data('selectBox-settings', $.extend(true, $(this).data('selectBox-settings'), data));
                });
                break;
            case 'options':
                if (data === undefined) return $(this).data('selectBox-control').data('selectBox-options');
                $(this).each(function () {
                    setOptions(this, data);
                });
                break;
            case 'value':
                if (data === undefined) return $(this).val();
                $(this).each(function () {
                    setValue(this, data);
                });
                break;
            case 'refresh':
                $(this).each(function () {
                    refresh(this);
                });
                break;
            case 'enable':
                $(this).each(function () {
                    enable(this);
                });
                break;
            case 'disable':
                $(this).each(function () {
                    disable(this);
                });
                break;
            case 'destroy':
                $(this).each(function () {
                    destroy(this);
                });
                break;
            default:
                $(this).each(function () {
                    init(this, method);
                });
                break;
            }
            return $(this);
        }
    });
})(jQuery);
/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 *
 * Requires: 1.2.2+
 */
(function ($) {
    var types = ['DOMMouseScroll', 'mousewheel'];
    if ($.event.fixHooks) {
        for (var i = types.length; i;) {
            $.event.fixHooks[types[--i]] = $.event.mouseHooks;
        }
    }
    $.event.special.mousewheel = {
        setup: function () {
            if (this.addEventListener) {
                for (var i = types.length; i;) {
                    this.addEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = handler;
            }
        },
        teardown: function () {
            if (this.removeEventListener) {
                for (var i = types.length; i;) {
                    this.removeEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = null;
            }
        }
    };
    $.fn.extend({
        mousewheel: function (fn) {
            return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },
        unmousewheel: function (fn) {
            return this.unbind("mousewheel", fn);
        }
    });

    function handler(event) {
        var orgEvent = event || window.event,
            args = [].slice.call(arguments, 1),
            delta = 0,
            returnValue = true,
            deltaX = 0,
            deltaY = 0;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";
        if (orgEvent.wheelDelta) {
            delta = orgEvent.wheelDelta / 120;
        }
        if (orgEvent.detail) {
            delta = -orgEvent.detail / 3;
        }
        deltaY = delta;
        if (orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
            deltaY = 0;
            deltaX = -1 * delta;
        }
        if (orgEvent.wheelDeltaY !== undefined) {
            deltaY = orgEvent.wheelDeltaY / 120;
        }
        if (orgEvent.wheelDeltaX !== undefined) {
            deltaX = -1 * orgEvent.wheelDeltaX / 120;
        }
        args.unshift(event, delta, deltaX, deltaY);
        return ($.event.dispatch || $.event.handle).apply(this, args);
    }
})(jQuery);
(function ($) {
    $.fn.jCarouselLite = function (o) {
        o = $.extend({
            btnPrev: null,
            btnNext: null,
            btnGo: null,
            mouseWheel: false,
            auto: null,
            speed: 200,
            easing: null,
            vertical: false,
            circular: true,
            visible: 3,
            start: 0,
            scroll: 1,
            beforeStart: null,
            afterEnd: null
        }, o || {});
        return this.each(function () {
            var b = false,
                animCss = o.vertical ? "top" : "left",
                sizeCss = o.vertical ? "height" : "width";
            var c = $(this),
                ul = $("ul", c),
                tLi = $("li", ul),
                tl = tLi.size(),
                v = o.visible;
            if (o.circular) {
                ul.prepend(tLi.slice(tl - v - 1 + 1).clone()).append(tLi.slice(0, v).clone());
                o.start += v
            }
            var f = $("li", ul),
                itemLength = f.size(),
                curr = o.start;
            c.css("visibility", "visible");
            f.css({
                overflow: "hidden",
                float: o.vertical ? "none" : "left"
            });
            ul.css({
                margin: "0",
                padding: "0",
                position: "relative",
                "list-style-type": "none",
                "z-index": "1"
            });
            c.css({
                overflow: "hidden",
                position: "relative",
                "z-index": "2",
                left: "0px"
            });
            var g = o.vertical ? height(f) : width(f);
            var h = g * itemLength;
            var j = g * v;
            f.css({
                width: f.width(),
                height: f.height()
            });
            ul.css(sizeCss, h + "px").css(animCss, -(curr * g));
            c.css(sizeCss, j + "px");
            if (o.btnPrev) $(o.btnPrev).click(function () {
                return go(curr - o.scroll)
            });
            if (o.btnNext) $(o.btnNext).click(function () {
                return go(curr + o.scroll)
            });
            if (o.btnGo) $.each(o.btnGo, function (i, a) {
                $(a).click(function () {
                    return go(o.circular ? o.visible + i : i)
                })
            });
            if (o.mouseWheel && c.mousewheel) c.mousewheel(function (e, d) {
                return d > 0 ? go(curr - o.scroll) : go(curr + o.scroll)
            });
            if (o.auto) setInterval(function () {
                go(curr + o.scroll)
            }, o.auto + o.speed);

            function vis() {
                return f.slice(curr).slice(0, v)
            };

            function go(a) {
                if (!b) {
                    if (o.beforeStart) o.beforeStart.call(this, vis());
                    if (o.circular) {
                        if (a <= o.start - v - 1) {
                            ul.css(animCss, -((itemLength - (v * 2)) * g) + "px");
                            curr = a == o.start - v - 1 ? itemLength - (v * 2) - 1 : itemLength - (v * 2) - o.scroll
                        } else if (a >= itemLength - v + 1) {
                            ul.css(animCss, -((v) * g) + "px");
                            curr = a == itemLength - v + 1 ? v + 1 : v + o.scroll
                        } else curr = a
                    } else {
                        if (a < 0 || a > itemLength - v) return;
                        else curr = a
                    }
                    b = true;
                    ul.animate(animCss == "left" ? {
                        left: -(curr * g)
                    } : {
                        top: -(curr * g)
                    }, o.speed, o.easing, function () {
                        if (o.afterEnd) o.afterEnd.call(this, vis());
                        b = false
                    });
                    if (!o.circular) {
                        $(o.btnPrev + "," + o.btnNext).removeClass("disabled");
                        $((curr - o.scroll < 0 && o.btnPrev) || (curr + o.scroll > itemLength - v && o.btnNext) || []).addClass("disabled")
                    }
                }
                return false
            }
        })
    };

    function css(a, b) {
        return parseInt($.css(a[0], b)) || 0
    };

    function width(a) {
        return a[0].offsetWidth + css(a, 'marginLeft') + css(a, 'marginRight')
    };

    function height(a) {
        return a[0].offsetHeight + css(a, 'marginTop') + css(a, 'marginBottom')
    }
})(jQuery);
var TWEEN = TWEEN || (function () {
    var i, tl, interval, time, fps = 60,
        autostart = false,
        tweens = [],
        num_tweens;
    return {
        setFPS: function (f) {
            fps = f || 60;
        },
        start: function (f) {
            if (arguments.length != 0) {
                this.setFPS(f);
            }
            interval = setInterval(this.update, 1000 / fps);
        },
        stop: function () {
            clearInterval(interval);
        },
        setAutostart: function (value) {
            autostart = value;
            if (autostart && !interval) {
                this.start();
            }
        },
        add: function (tween) {
            tweens.push(tween);
            if (autostart && !interval) {
                this.start();
            }
        },
        getAll: function () {
            return tweens;
        },
        removeAll: function () {
            tweens = [];
        },
        remove: function (tween) {
            i = tweens.indexOf(tween);
            if (i !== -1) {
                tweens.splice(i, 1);
            }
        },
        update: function (_time) {
            i = 0;
            num_tweens = tweens.length;
            var time = _time || new Date().getTime();
            while (i < num_tweens) {
                if (tweens[i].update(time)) {
                    i++;
                } else {
                    tweens.splice(i, 1);
                    num_tweens--;
                }
            }
            if (num_tweens == 0 && autostart == true) {
                this.stop();
            }
        }
    };
})();
TWEEN.Tween = function (object) {
    var _object = object,
        _valuesStart = {},
        _valuesDelta = {},
        _valuesEnd = {},
        _duration = 1000,
        _delayTime = 0,
        _startTime = null,
        _easingFunction = TWEEN.Easing.Linear.EaseNone,
        _chainedTween = null,
        _onUpdateCallback = null,
        _onCompleteCallback = null;
    this.to = function (properties, duration) {
        if (duration !== null) {
            _duration = duration;
        }
        for (var property in properties) {
            if (_object[property] === null) {
                continue;
            }
            _valuesEnd[property] = properties[property];
        }
        return this;
    };
    this.start = function (_time) {
        TWEEN.add(this);
        _startTime = _time ? _time + _delayTime : new Date().getTime() + _delayTime;
        for (var property in _valuesEnd) {
            if (_object[property] === null) {
                continue;
            }
            _valuesStart[property] = _object[property];
            _valuesDelta[property] = _valuesEnd[property] - _object[property];
        }
        return this;
    };
    this.stop = function () {
        TWEEN.remove(this);
        return this;
    };
    this.delay = function (amount) {
        _delayTime = amount;
        return this;
    };
    this.easing = function (easing) {
        _easingFunction = easing;
        return this;
    };
    this.chain = function (chainedTween) {
        _chainedTween = chainedTween;
        return this;
    };
    this.onUpdate = function (onUpdateCallback) {
        _onUpdateCallback = onUpdateCallback;
        return this;
    };
    this.onComplete = function (onCompleteCallback) {
        _onCompleteCallback = onCompleteCallback;
        return this;
    };
    this.update = function (time) {
        var property, elapsed, value;
        if (time < _startTime) {
            return true;
        }
        elapsed = (time - _startTime) / _duration;
        elapsed = elapsed > 1 ? 1 : elapsed;
        value = _easingFunction(elapsed);
        for (property in _valuesDelta) {
            _object[property] = _valuesStart[property] + _valuesDelta[property] * value;
        }
        if (_onUpdateCallback !== null) {
            _onUpdateCallback.call(_object, value);
        }
        if (elapsed == 1) {
            if (_onCompleteCallback !== null) {
                _onCompleteCallback.call(_object);
            }
            if (_chainedTween !== null) {
                _chainedTween.start();
            }
            return false;
        }
        return true;
    };
}
TWEEN.Easing = {
    Linear: {},
    Quadratic: {},
    Cubic: {},
    Quartic: {},
    Quintic: {},
    Sinusoidal: {},
    Exponential: {},
    Circular: {},
    Elastic: {},
    Back: {},
    Bounce: {}
};
TWEEN.Easing.Linear.EaseNone = function (k) {
    return k;
};
TWEEN.Easing.Quadratic.EaseIn = function (k) {
    return k * k;
};
TWEEN.Easing.Quadratic.EaseOut = function (k) {
    return -k * (k - 2);
};
TWEEN.Easing.Quadratic.EaseInOut = function (k) {
    if ((k *= 2) < 1) return 0.5 * k * k;
    return -0.5 * (--k * (k - 2) - 1);
};
TWEEN.Easing.Cubic.EaseIn = function (k) {
    return k * k * k;
};
TWEEN.Easing.Cubic.EaseOut = function (k) {
    return --k * k * k + 1;
};
TWEEN.Easing.Cubic.EaseInOut = function (k) {
    if ((k *= 2) < 1) return 0.5 * k * k * k;
    return 0.5 * ((k -= 2) * k * k + 2);
};
TWEEN.Easing.Quartic.EaseIn = function (k) {
    return k * k * k * k;
};
TWEEN.Easing.Quartic.EaseOut = function (k) {
    return -(--k * k * k * k - 1);
}
TWEEN.Easing.Quartic.EaseInOut = function (k) {
    if ((k *= 2) < 1) return 0.5 * k * k * k * k;
    return -0.5 * ((k -= 2) * k * k * k - 2);
};
TWEEN.Easing.Quintic.EaseIn = function (k) {
    return k * k * k * k * k;
};
TWEEN.Easing.Quintic.EaseOut = function (k) {
    return (k = k - 1) * k * k * k * k + 1;
};
TWEEN.Easing.Quintic.EaseInOut = function (k) {
    if ((k *= 2) < 1) return 0.5 * k * k * k * k * k;
    return 0.5 * ((k -= 2) * k * k * k * k + 2);
};
TWEEN.Easing.Sinusoidal.EaseIn = function (k) {
    return -Math.cos(k * Math.PI / 2) + 1;
};
TWEEN.Easing.Sinusoidal.EaseOut = function (k) {
    return Math.sin(k * Math.PI / 2);
};
TWEEN.Easing.Sinusoidal.EaseInOut = function (k) {
    return -0.5 * (Math.cos(Math.PI * k) - 1);
};
TWEEN.Easing.Exponential.EaseIn = function (k) {
    return k == 0 ? 0 : Math.pow(2, 10 * (k - 1));
};
TWEEN.Easing.Exponential.EaseOut = function (k) {
    return k == 1 ? 1 : -Math.pow(2, -10 * k) + 1;
};
TWEEN.Easing.Exponential.EaseInOut = function (k) {
    if (k == 0) return 0;
    if (k == 1) return 1;
    if ((k *= 2) < 1) return 0.5 * Math.pow(2, 10 * (k - 1));
    return 0.5 * (-Math.pow(2, -10 * (k - 1)) + 2);
};
TWEEN.Easing.Circular.EaseIn = function (k) {
    return -(Math.sqrt(1 - k * k) - 1);
};
TWEEN.Easing.Circular.EaseOut = function (k) {
    return Math.sqrt(1 - --k * k);
};
TWEEN.Easing.Circular.EaseInOut = function (k) {
    if ((k /= 0.5) < 1) return -0.5 * (Math.sqrt(1 - k * k) - 1);
    return 0.5 * (Math.sqrt(1 - (k -= 2) * k) + 1);
};
TWEEN.Easing.Elastic.EaseIn = function (k) {
    var s, a = 0.1,
        p = 0.4;
    if (k == 0) return 0;
    if (k == 1) return 1;
    if (!p) p = 0.3;
    if (!a || a < 1) {
        a = 1;
        s = p / 4;
    } else s = p / (2 * Math.PI) * Math.asin(1 / a);
    return -(a * Math.pow(2, 10 * (k -= 1)) * Math.sin((k - s) * (2 * Math.PI) / p));
};
TWEEN.Easing.Elastic.EaseOut = function (k) {
    var s, a = 0.1,
        p = 0.4;
    if (k == 0) return 0;
    if (k == 1) return 1;
    if (!p) p = 0.3;
    if (!a || a < 1) {
        a = 1;
        s = p / 4;
    } else s = p / (2 * Math.PI) * Math.asin(1 / a);
    return (a * Math.pow(2, -10 * k) * Math.sin((k - s) * (2 * Math.PI) / p) + 1);
};
TWEEN.Easing.Elastic.EaseInOut = function (k) {
    var s, a = 0.1,
        p = 0.4;
    if (k == 0) return 0;
    if (k == 1) return 1;
    if (!p) p = 0.3;
    if (!a || a < 1) {
        a = 1;
        s = p / 4;
    } else s = p / (2 * Math.PI) * Math.asin(1 / a); if ((k *= 2) < 1) return -0.5 * (a * Math.pow(2, 10 * (k -= 1)) * Math.sin((k - s) * (2 * Math.PI) / p));
    return a * Math.pow(2, -10 * (k -= 1)) * Math.sin((k - s) * (2 * Math.PI) / p) * 0.5 + 1;
};
TWEEN.Easing.Back.EaseIn = function (k) {
    var s = 1.70158;
    return k * k * ((s + 1) * k - s);
};
TWEEN.Easing.Back.EaseOut = function (k) {
    var s = 1.70158;
    return (k = k - 1) * k * ((s + 1) * k + s) + 1;
};
TWEEN.Easing.Back.EaseInOut = function (k) {
    var s = 1.70158 * 1.525;
    if ((k *= 2) < 1) return 0.5 * (k * k * ((s + 1) * k - s));
    return 0.5 * ((k -= 2) * k * ((s + 1) * k + s) + 2);
};
TWEEN.Easing.Bounce.EaseIn = function (k) {
    return 1 - TWEEN.Easing.Bounce.EaseOut(1 - k);
};
TWEEN.Easing.Bounce.EaseOut = function (k) {
    if ((k /= 1) < (1 / 2.75)) {
        return 7.5625 * k * k;
    } else if (k < (2 / 2.75)) {
        return 7.5625 * (k -= (1.5 / 2.75)) * k + 0.75;
    } else if (k < (2.5 / 2.75)) {
        return 7.5625 * (k -= (2.25 / 2.75)) * k + 0.9375;
    } else {
        return 7.5625 * (k -= (2.625 / 2.75)) * k + 0.984375;
    }
};
TWEEN.Easing.Bounce.EaseInOut = function (k) {
    if (k < 0.5) return TWEEN.Easing.Bounce.EaseIn(k * 2) * 0.5;
    return TWEEN.Easing.Bounce.EaseOut(k * 2 - 1) * 0.5 + 0.5;
};;
var ScrollAnimator = function () {
    var settings = {},
        page, started = false,
        paused = false,
        animation = null;
    var w = $(window),
        d = $(document),
        touch = false,
        touchStart = {
            x: 0,
            y: 0
        },
        scrollStart = 0,
        scrollTopTweened = 0,
        scrollTop = 0,
        scrollDirection = 0,
        autoScrollInterval;

    function animationLoop() {
        requestAnimFrame(animationLoop);
        if (paused)
            return;
        if (Math.ceil(scrollTopTweened) !== Math.floor(scrollTop)) {
            scrollTopTweened += settings.tweenSpeed * (scrollTop - scrollTopTweened);
            scrollDirection = scrollTop > scrollTopTweened ? 1 : -1;
            for (var i in animation) {
                var anim = animation[i];
                if (scrollTopTweened >= anim.startAt && scrollTopTweened <= anim.endAt) {
                    startAnimatable(anim);
                    render(anim);
                } else {
                    stopAnimatable(anim);
                }
            }
            if (typeof settings.onUpdate === 'function') settings.onUpdate(scrollTopTweened);
        }
    }

    function render(anim) {
        var progress = (anim.startAt - scrollTopTweened) / (anim.startAt - anim.endAt);
        progress = Math.max(0, Math.min(1, progress));
        var properties = {};
        anim.lastProgress = progress;
        if (anim.keyframes) {
            for (var i = 1; i < anim.keyframes.length; i++) {
                var keyframe = anim.keyframes[i],
                    lastkeyframe = anim.keyframes[i - 1],
                    keyframeProgress = (lastkeyframe.position - progress) / (lastkeyframe.position - keyframe.position);
                if (keyframeProgress >= 0 && keyframeProgress <= 1) {
                    if (keyframe.onProgress && typeof keyframe.onProgress === 'function') {
                        keyframe.onProgress(keyframeProgress, scrollDirection);
                    }
                    for (var property in keyframe.properties) {
                        if (property === "background-position" && keyframe.properties[property].hasOwnProperty("x")) {
                            var startValues = keyframe.properties[property];
                            var endValues = lastkeyframe.properties[property];
                            var result = "";
                            if (typeof startValues.x === "number") {
                                result += getTweenedValue(endValues.x, startValues.x, keyframeProgress, 1, keyframe.ease) + "px";
                            } else {
                                result += startValues.x
                            }
                            result += " ";
                            if (typeof startValues.y === "number") {
                                result += getTweenedValue(endValues.y, startValues.y, keyframeProgress, 1, keyframe.ease) + "px";
                            } else {
                                result += startValues.y
                            }
                            properties[property] = result;
                        } else {
                            properties[property] = getTweenedValue(lastkeyframe.properties[property], keyframe.properties[property], keyframeProgress, 1, keyframe.ease);
                        }
                    }
                }
            }
        }
        anim._elem.css(properties);
        if (anim.onProgress && typeof anim.onProgress === 'function') {
            anim.onProgress.call(anim, progress);
        }
    }

    function startAnimatable(anim) {
        if (!anim._started) {
            if (anim.onStartAnimate && typeof anim.onStartAnimate === 'function') {
                anim.onStartAnimate.call(anim, scrollDirection);
            } else {
                anim._elem.css('display', 'block');
            }
            anim._started = true;
        }
    }

    function stopAnimatable(anim) {
        if ((anim._started && anim.endAt < scrollTopTweened || anim._started && anim.startAt > scrollTopTweened) || (scrollDirection < 0 && anim.lastProgress > 0 && anim.startAt > scrollTopTweened) || (scrollDirection > 0 && anim.lastProgress < 1 && anim.endAt < scrollTopTweened)) {
            render(anim);
            if (anim.onEndAnimate && typeof anim.onEndAnimate === 'function') {
                anim.onEndAnimate.call(anim, scrollDirection);
            } else {
                anim._elem.css('display', 'none');
            }
            anim._started = false;
        }
    }

    function setAnimatable() {
        for (var i in animation) {
            var anim = animation[i];
            anim.lastProgress = 0;
            if (anim._elem == undefined) {
                anim._elem = $(anim.selector);
            }
            if (typeof anim.onInit == 'function') anim.onInit.call(anim);
            for (var k in anim.keyframes) {
                var keyframe = anim.keyframes[k];
                if (keyframe.position == 0) {
                    var nKeyframe = anim.keyframes[Number(k) + 1];
                    for (var property in nKeyframe.properties) {
                        if (keyframe.properties[property] == undefined) {
                            if (/left|top/.test(property)) {
                                keyframe.properties[property] = anim._elem.position()[property];
                            }
                        }
                    }
                }
                var bIndex = Number(k);
                while (bIndex > 0) {
                    var bKeyframe = anim.keyframes[bIndex];
                    for (var property in bKeyframe.properties) {
                        if (keyframe.properties[property] == undefined) {
                            keyframe.properties[property] = bKeyframe.properties[property];
                        }
                    }
                    bIndex--;
                };
                if (typeof keyframe.onInit == 'function') keyframe.onInit(anim);
            }
        }
    }

    function resize() {
        var container = settings.container;
        page = {
            wWidth: settings.container.width(),
            wHeight: settings.container.height(),
            wCenter: {
                left: settings.container.width() / 2,
                top: settings.container.height() / 2
            }
        };
        if (settings.onResize && typeof settings.onResize === 'function')
            settings.onResize(page);
        resetAnimatable();
        setAnimatable();
        start();
    }

    function resetAnimatable() {
        for (var i in animation) {
            var anim = animation[i];
            if (anim._started) {
                delete anim._elem;
                delete anim._started;
            }
        }
    }

    function resizeHandler(e) {
        resize();
    }

    function touchStartHandler(e) {
        touchStart.x = e.touches[0].pageX;
        touchStart.y = e.touches[0].pageY;
        scrollStart = scrollTop;
    };

    function touchEndHandler(e) {}

    function touchMoveHandler(e) {
        e.preventDefault();
        if (paused)
            return;
        var offset = {};
        offset.x = touchStart.x - e.touches[0].pageX;
        offset.y = touchStart.y - e.touches[0].pageY;
		 scrollTop = Math.max(0, scrollStart + offset.y);
        checkScrollExtents();
    }

    function wheelHandler(e, delta, deltaX, deltaY) {
    	
        if (paused)
            return;
        scrollTop -= delta * settings.scrollSpeed;
        if (scrollTop < 0) scrollTop = 0;
        checkScrollExtents();
    };

    function checkScrollExtents() {
        if (scrollTop < 0)
            scrollTop = 0;
        else if (scrollTop > settings.maxScroll){
            scrollTop = settings.maxScroll;
            if(!FinishAnimationRun){
           		FinishAnimation();
           		ResetAnimation();
           	}
           }
    }

    function getTweenedValue(start, end, currentTime, totalTime, tweener) {
        var delta = end - start;
        var percentComplete = currentTime / totalTime;
        if (!tweener) tweener = TWEEN.Easing.Linear.EaseNone;
        return tweener(percentComplete) * delta + start
    }

    function isTouch() {
        return 'ontouchstart' in window;
    }

    function init(opts) {
        var defaults = {
            maxScroll: 600,
            tickSpeed: 30,
            scrollSpeed: 20,
            useRAF: true,
            tweenSpeed: .3
        };
        settings = $.extend(defaults, opts);
        animation = settings.animation;
        touch = isTouch();
        if (touch) {
            var container = settings.container[0];
            container.addEventListener('touchstart', touchStartHandler, true);
            container.addEventListener('touchmove', touchMoveHandler, true);
            container.addEventListener('touchend', touchEndHandler, true);
        }
        //settings.container[0].on('mousewheel', wheelHandler);
		$("#ParallaxContainer").on('mousewheel', wheelHandler);
		//var container = settings.container[0];
		//container.addEventListener('onmousewheel', wheelHandler, true);
        w.on('resize', resizeHandler);
        window.requestAnimFrame = (function () {
            if (settings.useRAF) {
                return
                window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
                    window.setTimeout(callback, settings.tickSpeed);
                };
            } else {
                return function (callback) {
                    window.setTimeout(callback, settings.tickSpeed);
                }
            };
        })();
        resize();
        return this;
    };

    function start() {
        if (!started && settings.startAt) scrollTopTweened = scrollTop = settings.startAt;
        scrollTop++;
        if (!started) {
            animationLoop();
            started = true;
        };
        if (settings.onStart && typeof settings.onStart === 'function') {
            settings.onStart();
        }
    };

    function getPageInfo() {
        return page;
    };

    function getScrollTop() {
        return scrollTopTweened;
    };

    function getMaxScroll() {
        return settings.maxScroll;
    };

    function scrollTo(scroll) {
        if (paused)
            return;
        scrollTop = scroll;
    };

    function autoScrollStart() {
        if (autoScrollInterval)
            return;
        autoScrollInterval = setInterval(aScroll, 100);
    }

    function autoScrollStop() {
        clearInterval(autoScrollInterval);
    }

    function aScroll() {
        scrollTop += 5;
        if (scrollTop > settings.maxScroll) scrollTop = scrollTopTweened = 0;
    }

    function stopScroll() {
        scrollTopTweened = scrollTop;
    }

    function pauseScroll() {
        paused = true;
    }

    function resumeScroll() {
        paused = false;
    }

    function toggleDebug() {
        if (settings.debugId == false || settings.debugId == undefined) {
            console.log('debug on');
            settings.debugId = true;
            $('#status2').show();
            $('#status').show();
        } else {
            console.log('debug off');
            settings.debugId = false;
            $('#status2').hide();
            $('#status').hide();
        }
        for (var i in animation) {
            var anim = animation[i];
            if (settings.debugId == true) {
                anim._elem.css('border', '1px dashed red');
            } else {
                anim._elem.css('border', '');
                $('body').find('.debugid').remove();
            }
        }
    };

    function isDebug() {
        return settings.debug;
    };
    return {
        init: init,
        start: start,
        pause: pauseScroll,
        resume: resumeScroll,
        getPageInfo: getPageInfo,
        getScrollTop: getScrollTop,
        getMaxScroll: getMaxScroll,
        autoScrollStart: autoScrollStart,
        autoScrollStop: autoScrollStop,
        stopScroll: stopScroll,
        scrollTo: scrollTo,
        isDebug: isDebug,
        toggleDebug: toggleDebug,
        resize: resize
    }
};;
/*!
 * jQuery Tools v1.2.7 - The missing UI library for the Web
 *
 * tooltip/tooltip.js
 * tooltip/tooltip.dynamic.js
 *
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 *
 * http://flowplayer.org/tools/
 *
 */
(function (a) {
    a.tools = a.tools || {
        version: "v1.2.7"
    }, a.tools.tooltip = {
        conf: {
            effect: "toggle",
            fadeOutSpeed: "fast",
            predelay: 0,
            delay: 30,
            opacity: 1,
            tip: 0,
            fadeIE: !1,
            position: ["top", "center"],
            offset: [0, 0],
            relative: !1,
            cancelDefault: !0,
            events: {
                def: "mouseenter,mouseleave",
                input: "focus,blur",
                widget: "focus mouseenter,blur mouseleave",
                tooltip: "mouseenter,mouseleave"
            },
            layout: "<div/>",
            tipClass: "tooltip"
        },
        addEffect: function (a, c, d) {
            b[a] = [c, d]
        }
    };
    var b = {
        toggle: [
            function (a) {
                var b = this.getConf(),
                    c = this.getTip(),
                    d = b.opacity;
                d < 1 && c.css({
                    opacity: d
                }), c.show(), a.call()
            },
            function (a) {
                this.getTip().hide(), a.call()
            }
        ],
        fade: [
            function (b) {
                var c = this.getConf();
                !a.browser.msie || c.fadeIE ? this.getTip().fadeTo(c.fadeInSpeed, c.opacity, b) : (this.getTip().show(), b())
            },
            function (b) {
                var c = this.getConf();
                !a.browser.msie || c.fadeIE ? this.getTip().fadeOut(c.fadeOutSpeed, b) : (this.getTip().hide(), b())
            }
        ]
    };

    function c(b, c, d) {
        var e = d.relative ? b.position().top : b.offset().top,
            f = d.relative ? b.position().left : b.offset().left,
            g = d.position[0];
        e -= c.outerHeight() - d.offset[0], f += b.outerWidth() + d.offset[1], /iPad/i.test(navigator.userAgent) && (e -= a(window).scrollTop());
        var h = c.outerHeight() + b.outerHeight();
        g == "center" && (e += h / 2), g == "bottom" && (e += h), g = d.position[1];
        var i = c.outerWidth() + b.outerWidth();
        g == "center" && (f -= i / 2), g == "left" && (f -= i);
        return {
            top: e,
            left: f
        }
    }

    function d(d, e) {
        var f = this,
            g = d.add(f),
            h, i = 0,
            j = 0,
            k = d.attr("title"),
            l = d.attr("data-tooltip"),
            m = b[e.effect],
            n, o = d.is(":input"),
            p = o && d.is(":checkbox, :radio, select, :button, :submit"),
            q = d.attr("type"),
            r = e.events[q] || e.events[o ? p ? "widget" : "input" : "def"];
        if (!m) throw "Nonexistent effect \"" + e.effect + "\"";
        r = r.split(/,\s*/);
        if (r.length != 2) throw "Tooltip: bad events configuration for " + q;
        d.on(r[0], function (a) {
            clearTimeout(i), e.predelay ? j = setTimeout(function () {
                f.show(a)
            }, e.predelay) : f.show(a)
        }).on(r[1], function (a) {
            clearTimeout(j), e.delay ? i = setTimeout(function () {
                f.hide(a)
            }, e.delay) : f.hide(a)
        }), k && e.cancelDefault && (d.removeAttr("title"), d.data("title", k)), a.extend(f, {
            show: function (b) {
                if (!h) {
                    l ? h = a(l) : e.tip ? h = a(e.tip).eq(0) : k ? h = a(e.layout).addClass(e.tipClass).appendTo(document.body).hide().append(k) : (h = d.next(), h.length || (h = d.parent().next()));
                    if (!h.length) throw "Cannot find tooltip for " + d
                }
                if (f.isShown()) return f;
                h.stop(!0, !0);
                var o = c(d, h, e);
                e.tip && h.html(d.data("title")), b = a.Event(), b.type = "onBeforeShow", g.trigger(b, [o]);
                if (b.isDefaultPrevented()) return f;
                o = c(d, h, e), h.css({
                    position: "absolute",
                    top: o.top,
                    left: o.left
                }), n = !0, m[0].call(f, function () {
                    b.type = "onShow", n = "full", g.trigger(b)
                });
                var p = e.events.tooltip.split(/,\s*/);
                h.data("__set") || (h.off(p[0]).on(p[0], function () {
                    clearTimeout(i), clearTimeout(j)
                }), p[1] && !d.is("input:not(:checkbox, :radio), textarea") && h.off(p[1]).on(p[1], function (a) {
                    a.relatedTarget != d[0] && d.trigger(r[1].split(" ")[0])
                }), e.tip || h.data("__set", !0));
                return f
            },
            hide: function (c) {
                if (!h || !f.isShown()) return f;
                c = a.Event(), c.type = "onBeforeHide", g.trigger(c);
                if (!c.isDefaultPrevented()) {
                    n = !1, b[e.effect][1].call(f, function () {
                        c.type = "onHide", g.trigger(c)
                    });
                    return f
                }
            },
            isShown: function (a) {
                return a ? n == "full" : n
            },
            getConf: function () {
                return e
            },
            getTip: function () {
                return h
            },
            getTrigger: function () {
                return d
            }
        }), a.each("onHide,onBeforeShow,onShow,onBeforeHide".split(","), function (b, c) {
            a.isFunction(e[c]) && a(f).on(c, e[c]), f[c] = function (b) {
                b && a(f).on(c, b);
                return f
            }
        })
    }
    a.fn.tooltip = function (b) {
        var c = this.data("tooltip");
        if (c) return c;
        b = a.extend(!0, {}, a.tools.tooltip.conf, b), typeof b.position == "string" && (b.position = b.position.split(/,?\s/)), this.each(function () {
            c = new d(a(this), b), a(this).data("tooltip", c)
        });
        return b.api ? c : this
    }
})(jQuery);
(function (a) {
    var b = a.tools.tooltip;
    b.dynamic = {
        conf: {
            classNames: "top right bottom left"
        }
    };

    function c(b) {
        var c = a(window),
            d = c.width() + c.scrollLeft(),
            e = c.height() + c.scrollTop();
        return [b.offset().top <= c.scrollTop(), d <= b.offset().left + b.width(), e <= b.offset().top + b.height(), c.scrollLeft() >= b.offset().left]
    }

    function d(a) {
        var b = a.length;
        while (b--)
            if (a[b]) return !1;
        return !0
    }
    a.fn.dynamic = function (e) {
        typeof e == "number" && (e = {
            speed: e
        }), e = a.extend({}, b.dynamic.conf, e);
        var f = a.extend(!0, {}, e),
            g = e.classNames.split(/\s/),
            h;
        this.each(function () {
            var b = a(this).tooltip().onBeforeShow(function (b, e) {
                var i = this.getTip(),
                    j = this.getConf();
                h || (h = [j.position[0], j.position[1], j.offset[0], j.offset[1], a.extend({}, j)]), a.extend(j, h[4]), j.position = [h[0], h[1]], j.offset = [h[2], h[3]], i.css({
                    visibility: "hidden",
                    position: "absolute",
                    top: e.top,
                    left: e.left
                }).show();
                var k = a.extend(!0, {}, f),
                    l = c(i);
                if (!d(l)) {
                    l[2] && (a.extend(j, k.top), j.position[0] = "top", i.addClass(g[0])), l[3] && (a.extend(j, k.right), j.position[1] = "right", i.addClass(g[1])), l[0] && (a.extend(j, k.bottom), j.position[0] = "bottom", i.addClass(g[2])), l[1] && (a.extend(j, k.left), j.position[1] = "left", i.addClass(g[3]));
                    if (l[0] || l[2]) j.offset[0] *= -1;
                    if (l[1] || l[3]) j.offset[1] *= -1
                }
                i.css({
                    visibility: "visible"
                }).hide()
            });
            b.onBeforeShow(function () {
                var a = this.getConf(),
                    b = this.getTip();
                setTimeout(function () {
                    a.position = [h[0], h[1]], a.offset = [h[2], h[3]]
                }, 0)
            }), b.onHide(function () {
                var a = this.getTip();
                a.removeClass(e.classNames)
            }), ret = b
        });
        return e.api ? ret : this
    }
})(jQuery);

;

function Loader(divid, options) {
    if (!divid) divid = "body";
    this.div_id = divid;
    this.overlayColor = "#000000";
    this.userCallback = 0;
    this.showProgress = false;
    this.showProgressText = false;
    this.progressColor = "#ffffff";
    this.textColor = "#ffffff";
    this.textSize = "15";
    this.started = false;
    this.loaderPosition = {
        top: "50",
        left: "50"
    };
    if (options) {
        this.overlayColor = (!options.overlayColor) ? this.overlayColor : options.overlayColor;
        this.userCallback = (!options.userCallback) ? this.userCallback : options.userCallback;
        this.showProgress = (!options.showProgress) ? this.showProgress : options.showProgress;
        this.showProgressText = (!options.showProgressText) ? this.showProgressText : options.showProgressText;
        this.progressColor = (!options.progressColor) ? this.progressColor : options.progressColor;
        this.textColor = (!options.textColor) ? this.textColor : options.textColor;
        this.textSize = (!options.textSize) ? this.textSize : options.textSize;
        if (options.loaderPosition) {
            this.loaderPosition.top = (!options.loaderPosition.top) ? this.loaderPosition.top : options.loaderPosition.top;
            this.loaderPosition.left = (!options.loaderPosition.left) ? this.loaderPosition.left : options.loaderPosition.left;
        }
        this.debug = (!options.debug) ? false : options.debug;
    }
    this.Stop = function () {
        if (!this.started) return;
        clearTimeout(this.ieTimeout);
        if (!this.toLoadImages) return;
        for (var i = 0; i < this.toLoadImages.length; ++i)
            this.toLoadImages[i].unbind("load");
        this.toLoadImages = 0;
        this.started = false;
    }
    this.Start = function () {
        if (this.started) return;
        if (navigator.userAgent.match(/MSIE (\d+(?:\.\d+)+(?:b\d*)?)/) == "MSIE 6.0,6.0") {
            if (this.userCallback) this.userCallback();
            return;
        }
        this.images = this.getImages();
        if (this.images.length == 0) {
            if (this.userCallback) this.userCallback();
            return;
        }
        this.started = true;
        this.width = $(this.div_id).outerWidth();
        this.height = $(this.div_id).outerHeight();
        this.position = $(this.div_id).offset();
        this.overlay = $("<div id=\"honda_preloader\"></div>").appendTo($(this.div_id));
        var overlyDesc = {
            'background-color': this.overlayColor,
            'z-index': '9999',
            'position': 'fixed',
            'top': this.position.top,
            'left': this.position.left,
            'width': this.width + "px",
            'height': "100%"
        };
        $(this.overlay).css(overlyDesc);
        this.preloader = $("<div></div>").appendTo(this.selectorPreload);
        $(this.preloader).css({
            height: "0px",
            width: "0px",
            overflow: "hidden"
        });
        this.imagesLoaded = 0;
        var parent = this;
        var callback = this.imgLoaded;
        if (this.showProgressText) {
            this.loadAmt = $("<div id=\"honda_loader_bg\"><div id=\"honda_loader_logo\"></div><div id=\"loader_percent\">0%</div><div id=\"honda_loader_bar\"><div id=\"honda_loading_txt\">loading</div><div id=\"honda_loader_bar2\"></div></div></div>").appendTo($(this.overlay));
            this.loadAmt = $('#loader_percent');
            $(this.loadAmt).css({});
        }
        if (this.showProgress) {
            this.loadBar = $('#honda_loader_bar2');
            $(this.loadBar).css({});
        }
        var scope = this;
        $(window).resize(function () {
            if (!scope.overlay) return;
            scope.width = $(scope.div_id).outerWidth();
            scope.height = $(scope.div_id).outerHeight();
            scope.position = $(scope.div_id).offset();
            var overlyDesc = {
                'top': scope.position.top,
                'left': scope.position.left,
                'width': scope.width + "px",
                'height': scope.height + "px"
            };
            $(scope.overlay).css(overlyDesc);
        });
        this.toLoadImages = new Array();
        for (var i = 0; i < this.images.length; i++) {
            var imgLoad = $("<img></img>");
            $(imgLoad).attr("src", this.images[i]);
            $(imgLoad).unbind("load");
            $(imgLoad).bind("load", function () {
                callback(parent);
            });
            $(imgLoad).appendTo($(this.preloader));
            this.toLoadImages.push($(imgLoad));
        }
        var scope = this;
    };
    this.ieLoadFix = function (scope) {
        while ((100 / scope.images.length) * scope.imagesLoaded < 100) {
            scope.imgLoaded(scope);
        }
    };
    this.animateLoader = function (scope) {
        var perc = (101 / scope.images.length) * scope.imagesLoaded;
        if (perc > 99) {
            if (scope.loadAmt)
                $(scope.loadAmt).html("100%");
            if (scope.loadBar) {
                $(scope.loadBar).stop().animate({
                    width: perc + "%"
                }, 500, "linear", function () {
                    scope.doneLoading(scope);
                });
            } else {
                scope.doneLoading();
            }
        } else {
            if (scope.loadBar)
                $(scope.loadBar).stop().animate({
                    width: perc + "%"
                }, 500, "linear", function () {});
            if (scope.loadAmt)
                $(scope.loadAmt).html(Math.floor(perc) + "%");
        }
    }
    this.imgLoaded = function (scope) {
        scope.imagesLoaded++;
        scope.animateLoader(scope);
    };
    this.doneLoading = function (scope) {
        $(scope.preloader).remove();
        var cbk = scope.userCallback;
        if (this.started) {
            scope.overlay.fadeOut("slow", function () {
                $(this).remove();
                if (cbk) cbk();
            });
        } else
            $(scope.overlay).remove();
        scope.overlay.fadeOut("slow", function () {
            $(this).remove();
            if (cbk) cbk();
        });
        scope.overlay = 0;
        this.started = false;
        this.toLoadImages = 0;
    }
    this.getImages = function () {
        var imgs = new Array();
        var url = "";
        if ($(this.div_id).css("background-image") != "none") {
            url = $(this.div_id).css("background-image");
        } else if (typeof ($(this.div_id).attr("src")) != "undefined" && $(this.div_id).attr("tagName") == "img") {
            url = $(this.div_id).attr("src");
        }
        url = url.replace("url(\"", "");
        url = url.replace("url(", "");
        url = url.replace("\")", "");
        url = url.replace(")", "");
        if (url.length > 0) {
            imgs.push(url);
        }
        var p = this;
        var everything = $(this.div_id).find("*:not(script)").each(function () {
            var url = "";
            if ($(this).css("background-image") != "none") {
                url = $(this).css("background-image");
            } else if (typeof ($(this).attr("src")) != "undefined" && $(this).attr("tagName") == "img") {
                url = $(this).attr("src");
            }
            url = url.replace("url(\"", "");
            url = url.replace("url(", "");
            url = url.replace("\")", "");
            url = url.replace(")", "");
            if (url.length > 0) {
                imgs.push(url);
            }
        });
        return imgs;
    };
};
/*!
 * fancyBox - jQuery Plugin
 * version: 2.0.5 (21/02/2012)
 * @requires jQuery v1.6 or later
 *
 * Examples at http://fancyapps.com/fancybox/
 * License: www.fancyapps.com/fancybox/#license
 *
 * Copyright 2012 Janis Skarnelis - janis@fancyapps.com
 *
 */
(function (window, document, $) {
    var W = $(window),
        D = $(document),
        F = $.fancybox = function () {
            F.open.apply(this, arguments);
        },
        didResize = false,
        resizeTimer = null,
        isMobile = typeof document.createTouch !== "undefined";
    $.extend(F, {
        version: '2.0.5',
        defaults: {
            padding: 15,
            margin: 20,
            width: 800,
            height: 600,
            minWidth: 100,
            minHeight: 100,
            maxWidth: 9999,
            maxHeight: 9999,
            autoSize: true,
            autoResize: !isMobile,
            autoCenter: !isMobile,
            fitToView: true,
            aspectRatio: false,
            topRatio: 0.5,
            fixed: !($.browser.msie && $.browser.version <= 6) && !isMobile,
            scrolling: 'auto',
            wrapCSS: 'fancybox-default',
            arrows: true,
            closeBtn: true,
            closeClick: false,
            nextClick: false,
            mouseWheel: true,
            autoPlay: false,
            playSpeed: 3000,
            preload: 3,
            modal: false,
            loop: true,
            ajax: {
                dataType: 'html',
                headers: {
                    'X-fancyBox': true
                }
            },
            keys: {
                next: [13, 32, 34, 39, 40],
                prev: [8, 33, 37, 38],
                close: [27]
            },
            index: 0,
            type: null,
            href: null,
            content: null,
            title: null,
            tpl: {
                wrap: '<div class="fancybox-wrap"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div>',
                image: '<img class="fancybox-image" src="{href}" alt="" />',
                iframe: '<iframe class="fancybox-iframe" name="fancybox-frame{rnd}" frameborder="0" hspace="0"' + ($.browser.msie ? ' allowtransparency="true"' : '') + '></iframe>',
                swf: '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="wmode" value="transparent" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{href}" /><embed src="{href}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="100%" height="100%" wmode="transparent"></embed></object>',
                error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
                closeBtn: '<div title="Close" class="fancybox-item fancybox-close"></div>',
                next: '<a title="Next" class="fancybox-nav fancybox-next"><span></span></a>',
                prev: '<a title="Previous" class="fancybox-nav fancybox-prev"><span></span></a>'
            },
            openEffect: 'fade',
            openSpeed: 250,
            openEasing: 'swing',
            openOpacity: true,
            openMethod: 'zoomIn',
            closeEffect: 'fade',
            closeSpeed: 250,
            closeEasing: 'swing',
            closeOpacity: true,
            closeMethod: 'zoomOut',
            nextEffect: 'elastic',
            nextSpeed: 300,
            nextEasing: 'swing',
            nextMethod: 'changeIn',
            prevEffect: 'elastic',
            prevSpeed: 300,
            prevEasing: 'swing',
            prevMethod: 'changeOut',
            helpers: {
                overlay: {
                    speedIn: 0,
                    speedOut: 300,
                    opacity: 0.8,
                    css: {
                        cursor: 'pointer'
                    },
                    closeClick: true
                },
                title: {
                    type: 'float'
                }
            },
            onCancel: $.noop,
            beforeLoad: $.noop,
            afterLoad: $.noop,
            beforeShow: $.noop,
            afterShow: $.noop,
            beforeClose: $.noop,
            afterClose: $.noop
        },
        group: {},
        opts: {},
        coming: null,
        current: null,
        isOpen: false,
        isOpened: false,
        wrap: null,
        outer: null,
        inner: null,
        player: {
            timer: null,
            isActive: false
        },
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        open: function (group, opts) {
            F.close(true);
            if (group && !$.isArray(group)) {
                group = group instanceof $ ? $(group).get() : [group];
            }
            F.isActive = true;
            F.opts = $.extend(true, {}, F.defaults, opts);
            if ($.isPlainObject(opts) && typeof opts.keys !== 'undefined') {
                F.opts.keys = opts.keys ? $.extend({}, F.defaults.keys, opts.keys) : false;
            }
            F.group = group;
            F._start(F.opts.index || 0);
        },
        cancel: function () {
            if (F.coming && false === F.trigger('onCancel')) {
                return;
            }
            F.coming = null;
            F.hideLoading();
            if (F.ajaxLoad) {
                F.ajaxLoad.abort();
            }
            F.ajaxLoad = null;
            if (F.imgPreload) {
                F.imgPreload.onload = F.imgPreload.onabort = F.imgPreload.onerror = null;
            }
        },
        close: function (a) {
            F.cancel();
            if (!F.current || false === F.trigger('beforeClose')) {
                return;
            }
            F.unbindEvents();
            if (!F.isOpen || (a && a[0] === true)) {
                $(".fancybox-wrap").stop().trigger('onReset').remove();
                F._afterZoomOut();
            } else {
                F.isOpen = F.isOpened = false;
                $(".fancybox-item, .fancybox-nav").remove();
                F.wrap.stop(true).removeClass('fancybox-opened');
                F.inner.css('overflow', 'hidden');
                F.transitions[F.current.closeMethod]();
            }
        },
        play: function (a) {
            var clear = function () {
                    clearTimeout(F.player.timer);
                },
                set = function () {
                    clear();
                    if (F.current && F.player.isActive) {
                        F.player.timer = setTimeout(F.next, F.current.playSpeed);
                    }
                },
                stop = function () {
                    clear();
                    $('body').unbind('.player');
                    F.player.isActive = false;
                    F.trigger('onPlayEnd');
                },
                start = function () {
                    if (F.current && (F.current.loop || F.current.index < F.group.length - 1)) {
                        F.player.isActive = true;
                        $('body').bind({
                            'afterShow.player onUpdate.player': set,
                            'onCancel.player beforeClose.player': stop,
                            'beforeLoad.player': clear
                        });
                        set();
                        F.trigger('onPlayStart');
                    }
                };
            if (F.player.isActive || (a && a[0] === false)) {
                stop();
            } else {
                start();
            }
        },
        next: function () {
            if (F.current) {
                F.jumpto(F.current.index + 1);
            }
        },
        prev: function () {
            if (F.current) {
                F.jumpto(F.current.index - 1);
            }
        },
        jumpto: function (index) {
            if (!F.current) {
                return;
            }
            index = parseInt(index, 10);
            if (F.group.length > 1 && F.current.loop) {
                if (index >= F.group.length) {
                    index = 0;
                } else if (index < 0) {
                    index = F.group.length - 1;
                }
            }
            if (typeof F.group[index] !== 'undefined') {
                F.cancel();
                F._start(index);
            }
        },
        reposition: function (a) {
            if (F.isOpen) {
                F.wrap.css(F._getPosition(a));
            }
        },
        update: function (e) {
            if (F.isOpen) {
                if (!didResize) {
                    resizeTimer = setTimeout(function () {
                        var current = F.current;
                        if (didResize) {
                            didResize = false;
                            if (current) {
                                if (current.autoResize || (e && e.type === 'orientationchange')) {
                                    if (current.autoSize) {
                                        F.inner.height('auto');
                                        current.height = F.inner.height();
                                    }
                                    F._setDimension();
                                    if (current.canGrow) {
                                        F.inner.height('auto');
                                    }
                                }
                                if (current.autoCenter) {
                                    F.reposition();
                                }
                                F.trigger('onUpdate');
                            }
                        }
                    }, 100);
                }
                didResize = true;
            }
        },
        toggle: function () {
            if (F.isOpen) {
                F.current.fitToView = !F.current.fitToView;
                F.update();
            }
        },
        hideLoading: function () {
            $("#fancybox-loading").remove();
        },
        showLoading: function () {
            F.hideLoading();
            $('<div id="fancybox-loading"><div></div></div>').click(F.cancel).appendTo('body');
        },
        getViewport: function () {
            return {
                x: W.scrollLeft(),
                y: W.scrollTop(),
                w: W.width(),
                h: W.height()
            };
        },
        unbindEvents: function () {
            if (F.wrap) {
                F.wrap.unbind('.fb');
            }
            D.unbind('.fb');
            W.unbind('.fb');
        },
        bindEvents: function () {
            var current = F.current,
                keys = current.keys;
            if (!current) {
                return;
            }
            W.bind('resize.fb, orientationchange.fb', F.update);
            if (keys) {
                D.bind('keydown.fb', function (e) {
                    var code;
                    if (!e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey && $.inArray(e.target.tagName.toLowerCase(), ['input', 'textarea', 'select', 'button']) < 0) {
                        code = e.keyCode;
                        if ($.inArray(code, keys.close) > -1) {
                            F.close();
                            e.preventDefault();
                        } else if ($.inArray(code, keys.next) > -1) {
                            F.next();
                            e.preventDefault();
                        } else if ($.inArray(code, keys.prev) > -1) {
                            F.prev();
                            e.preventDefault();
                        }
                    }
                });
            }
            if ($.fn.mousewheel && current.mouseWheel && F.group.length > 1) {
                F.wrap.bind('mousewheel.fb', function (e, delta) {
                    var target = $(e.target).get(0);
                    if (target.clientHeight === 0 || (target.scrollHeight === target.clientHeight && target.scrollWidth === target.clientWidth)) {
                        e.preventDefault();
                        F[delta > 0 ? 'prev' : 'next']();
                    }
                });
            }
        },
        trigger: function (event) {
            var ret, obj = F[$.inArray(event, ['onCancel', 'beforeLoad', 'afterLoad']) > -1 ? 'coming' : 'current'];
            if (!obj) {
                return;
            }
            if ($.isFunction(obj[event])) {
                ret = obj[event].apply(obj, Array.prototype.slice.call(arguments, 1));
            }
            if (ret === false) {
                return false;
            }
            if (obj.helpers) {
                $.each(obj.helpers, function (helper, opts) {
                    if (opts && typeof F.helpers[helper] !== 'undefined' && $.isFunction(F.helpers[helper][event])) {
                        F.helpers[helper][event](opts, obj);
                    }
                });
            }
            $.event.trigger(event + '.fb');
        },
        isImage: function (str) {
            return str && str.match(/\.(jpg|gif|png|bmp|jpeg)(.*)?$/i);
        },
        isSWF: function (str) {
            return str && str.match(/\.(swf)(.*)?$/i);
        },
        _start: function (index) {
            var coming = {},
                element = F.group[index] || null,
                isDom, href, type, rez;
            if (element && (element.nodeType || element instanceof $)) {
                isDom = true;
                if ($.metadata) {
                    coming = $(element).metadata();
                }
            }
            coming = $.extend(true, {}, F.opts, {
                index: index,
                element: element
            }, ($.isPlainObject(element) ? element : coming));
            $.each(['href', 'title', 'content', 'type'], function (i, v) {
                coming[v] = F.opts[v] || (isDom && $(element).attr(v)) || coming[v] || null;
            });
            if (typeof coming.margin === 'number') {
                coming.margin = [coming.margin, coming.margin, coming.margin, coming.margin];
            }
            if (coming.modal) {
                $.extend(true, coming, {
                    closeBtn: false,
                    closeClick: false,
                    nextClick: false,
                    arrows: false,
                    mouseWheel: false,
                    keys: null,
                    helpers: {
                        overlay: {
                            css: {
                                cursor: 'auto'
                            },
                            closeClick: false
                        }
                    }
                });
            }
            F.coming = coming;
            if (false === F.trigger('beforeLoad')) {
                F.coming = null;
                return;
            }
            type = coming.type;
            href = coming.href || element;
            if (!type) {
                if (isDom) {
                    rez = $(element).data('fancybox-type');
                    if (!rez && element.className) {
                        rez = element.className.match(/fancybox\.(\w+)/);
                        type = rez ? rez[1] : null;
                    }
                }
                if (!type && $.type(href) === "string") {
                    if (F.isImage(href)) {
                        type = 'image';
                    } else if (F.isSWF(href)) {
                        type = 'swf';
                    } else if (href.match(/^#/)) {
                        type = 'inline';
                    }
                }
                if (!type) {
                    type = isDom ? 'inline' : 'html';
                }
                coming.type = type;
            }
            if (type === 'inline' || type === 'html') {
                if (!coming.content) {
                    if (type === 'inline') {
                        coming.content = $($.type(href) === "string" ? href.replace(/.*(?=#[^\s]+$)/, '') : href);
                    } else {
                        coming.content = element;
                    }
                }
                if (!coming.content || !coming.content.length) {
                    type = null;
                }
            } else if (!href) {
                type = null;
            }
            coming.group = F.group;
            coming.isDom = isDom;
            coming.href = href;
            if (type === 'image') {
                F._loadImage();
            } else if (type === 'ajax') {
                F._loadAjax();
            } else if (type) {
                F._afterLoad();
            } else {
                F._error('type');
            }
        },
        _error: function (type) {
            F.hideLoading();
            $.extend(F.coming, {
                type: 'html',
                autoSize: true,
                minHeight: 0,
                hasError: type,
                content: F.coming.tpl.error
            });
            F._afterLoad();
        },
        _loadImage: function () {
            F.imgPreload = new Image();
            F.imgPreload.onload = function () {
                this.onload = this.onerror = null;
                F.coming.width = this.width;
                F.coming.height = this.height;
                F._afterLoad();
            };
            F.imgPreload.onerror = function () {
                this.onload = this.onerror = null;
                F._error('image');
            };
            F.imgPreload.src = F.coming.href;
            if (!F.imgPreload.width) {
                F.showLoading();
            }
        },
        _loadAjax: function () {
            F.showLoading();
            F.ajaxLoad = $.ajax($.extend({}, F.coming.ajax, {
                url: F.coming.href,
                error: function (jqXHR, textStatus) {
                    if (textStatus !== 'abort') {
                        F._error('ajax', jqXHR);
                    } else {
                        F.hideLoading();
                    }
                },
                success: function (data, textStatus) {
                    if (textStatus === 'success') {
                        F.coming.content = data;
                        F._afterLoad();
                    }
                }
            }));
        },
        _preloadImages: function () {
            var group = F.group,
                current = F.current,
                len = group.length,
                item, href;
            if (!current.preload || group.length < 2) {
                return;
            }
            for (var i = 1; i <= Math.min(current.preload, len - 1); i++) {
                item = group[(current.index + i) % len];
                href = $(item).attr('href') || item;
                if (href) {
                    new Image().src = href;
                }
            }
        },
        _afterLoad: function () {
            F.hideLoading();
            if (!F.coming || false === F.trigger('afterLoad', F.current)) {
                F.coming = false;
                return;
            }
            if (F.isOpened) {
                $(".fancybox-item").remove();
                F.wrap.stop(true).removeClass('fancybox-opened');
                F.inner.css('overflow', 'hidden');
                F.transitions[F.current.prevMethod]();
            } else {
                $(".fancybox-wrap").stop().trigger('onReset').remove();
                F.trigger('afterClose');
            }
            F.unbindEvents();
            F.isOpen = false;
            F.current = F.coming;
            F.wrap = $(F.current.tpl.wrap).addClass('fancybox-' + (isMobile ? 'mobile' : 'desktop') + ' fancybox-tmp ' + F.current.wrapCSS).appendTo('body');
            F.outer = $('.fancybox-outer', F.wrap).css('padding', F.current.padding + 'px');
            F.inner = $('.fancybox-inner', F.wrap);
            F._setContent();
        },
        _setContent: function () {
            var content, loadingBay, iframe, current = F.current,
                type = current.type;
            switch (type) {
            case 'inline':
            case 'ajax':
            case 'html':
                content = current.content;
                if (content instanceof $) {
                    content = content.show().detach();
                    if (content.parent().hasClass('fancybox-inner')) {
                        content.parents('.fancybox-wrap').trigger('onReset').remove();
                    }
                    $(F.wrap).bind('onReset', function () {
                        content.appendTo('body').hide();
                    });
                }
                if (current.autoSize) {
                    loadingBay = $('<div class="fancybox-tmp ' + F.current.wrapCSS + '"></div>').appendTo('body').append(content);
                    current.width = loadingBay.width();
                    current.height = loadingBay.height();
                    loadingBay.width(F.current.width);
                    if (loadingBay.height() > current.height) {
                        loadingBay.width(current.width + 1);
                        current.width = loadingBay.width();
                        current.height = loadingBay.height();
                    }
                    content = loadingBay.contents().detach();
                    loadingBay.remove();
                }
                break;
            case 'image':
                content = current.tpl.image.replace('{href}', current.href);
                current.aspectRatio = true;
                break;
            case 'swf':
                content = current.tpl.swf.replace(/\{width\}/g, current.width).replace(/\{height\}/g, current.height).replace(/\{href\}/g, current.href);
                break;
            }
            if (type === 'iframe') {
                content = $(current.tpl.iframe.replace('{rnd}', new Date().getTime())).attr('scrolling', current.scrolling);
                current.scrolling = 'auto';
                if (current.autoSize) {
                    content.width(current.width);
                    F.showLoading();
                    content.data('ready', false).appendTo(F.inner).bind({
                        onCancel: function () {
                            $(this).unbind();
                            F._afterZoomOut();
                        },
                        load: function () {
                            var iframe = $(this),
                                height;
                            try {
                                if (this.contentWindow.document.location) {
                                    height = iframe.contents().find('body').height() + 12;
                                    iframe.height(height);
                                }
                            } catch (e) {
                                current.autoSize = false;
                            }
                            if (iframe.data('ready') === false) {
                                F.hideLoading();
                                if (height) {
                                    F.current.height = height;
                                }
                                F._beforeShow();
                                iframe.data('ready', true);
                            } else if (height) {
                                F.update();
                            }
                        }
                    }).attr('src', current.href);
                    return;
                } else {
                    content.attr('src', current.href);
                }
            } else if (type === 'image' || type === 'swf') {
                current.autoSize = false;
                current.scrolling = 'visible';
            }
            F.inner.append(content);
            F._beforeShow();
        },
        _beforeShow: function () {
            F.coming = null;
            F.trigger('beforeShow');
            F._setDimension();
            F.wrap.hide().removeClass('fancybox-tmp');
            F.bindEvents();
            F._preloadImages();
            F.transitions[F.isOpened ? F.current.nextMethod : F.current.openMethod]();
        },
        _setDimension: function () {
            var wrap = F.wrap,
                outer = F.outer,
                inner = F.inner,
                current = F.current,
                viewport = F.getViewport(),
                margin = current.margin,
                padding2 = current.padding * 2,
                width = current.width,
                height = current.height,
                maxWidth = current.maxWidth,
                maxHeight = current.maxHeight,
                minWidth = current.minWidth,
                minHeight = current.minHeight,
                ratio, height_, space;
            viewport.w -= (margin[1] + margin[3]);
            viewport.h -= (margin[0] + margin[2]);
            if (width.toString().indexOf('%') > -1) {
                width = (((viewport.w - padding2) * parseFloat(width)) / 100);
            }
            if (height.toString().indexOf('%') > -1) {
                height = (((viewport.h - padding2) * parseFloat(height)) / 100);
            }
            ratio = width / height;
            width += padding2;
            height += padding2;
            if (current.fitToView) {
                maxWidth = Math.min(viewport.w, maxWidth);
                maxHeight = Math.min(viewport.h, maxHeight);
            }
            if (current.aspectRatio) {
                if (width > maxWidth) {
                    width = maxWidth;
                    height = ((width - padding2) / ratio) + padding2;
                }
                if (height > maxHeight) {
                    height = maxHeight;
                    width = ((height - padding2) * ratio) + padding2;
                }
                if (width < minWidth) {
                    width = minWidth;
                    height = ((width - padding2) / ratio) + padding2;
                }
                if (height < minHeight) {
                    height = minHeight;
                    width = ((height - padding2) * ratio) + padding2;
                }
            } else {
                width = Math.max(minWidth, Math.min(width, maxWidth));
                height = Math.max(minHeight, Math.min(height, maxHeight));
            }
            width = Math.round(width);
            height = Math.round(height);
            $(wrap.add(outer).add(inner)).width('auto').height('auto');
            inner.width(width - padding2).height(height - padding2);
            wrap.width(width);
            height_ = wrap.height();
            if (width > maxWidth || height_ > maxHeight) {
                while ((width > maxWidth || height_ > maxHeight) && width > minWidth && height_ > minHeight) {
                    height = height - 10;
                    if (current.aspectRatio) {
                        width = Math.round(((height - padding2) * ratio) + padding2);
                        if (width < minWidth) {
                            width = minWidth;
                            height = ((width - padding2) / ratio) + padding2;
                        }
                    } else {
                        width = width - 10;
                    }
                    inner.width(width - padding2).height(height - padding2);
                    wrap.width(width);
                    height_ = wrap.height();
                }
            }
            current.dim = {
                width: width,
                height: height_
            };
            current.canGrow = current.autoSize && height > minHeight && height < maxHeight;
            current.canShrink = false;
            current.canExpand = false;
            if ((width - padding2) < current.width || (height - padding2) < current.height) {
                current.canExpand = true;
            } else if ((width > viewport.w || height_ > viewport.h) && width > minWidth && height > minHeight) {
                current.canShrink = true;
            }
            space = height_ - padding2;
            F.innerSpace = space - inner.height();
            F.outerSpace = space - outer.height();
        },
        _getPosition: function (a) {
            var current = F.current,
                viewport = F.getViewport(),
                margin = current.margin,
                width = F.wrap.width() + margin[1] + margin[3],
                height = F.wrap.height() + margin[0] + margin[2],
                rez = {
                    position: 'absolute',
                    top: margin[0] + viewport.y,
                    left: margin[3] + viewport.x
                };
            if (current.fixed && (!a || a[0] === false) && height <= viewport.h && width <= viewport.w) {
                rez = {
                    position: 'fixed',
                    top: margin[0],
                    left: margin[3]
                };
            }
            rez.top = Math.ceil(Math.max(rez.top, rez.top + ((viewport.h - height) * current.topRatio))) + 'px';
            rez.left = Math.ceil(Math.max(rez.left, rez.left + ((viewport.w - width) * 0.5))) + 'px';
            return rez;
        },
        _afterZoomIn: function () {
            var current = F.current,
                scrolling = current.scrolling;
            F.isOpen = F.isOpened = true;
            F.wrap.addClass('fancybox-opened').css('overflow', 'visible');
            F.update();
            F.inner.css('overflow', scrolling === 'yes' ? 'scroll' : (scrolling === 'no' ? 'hidden' : scrolling));
            if (current.closeClick || current.nextClick) {
                F.inner.css('cursor', 'pointer').bind('click.fb', current.nextClick ? F.next : F.close);
            }
            if (current.closeBtn) {
                $(current.tpl.closeBtn).appendTo(F.outer).bind('click.fb', F.close);
            }
            if (current.arrows && F.group.length > 1) {
                if (current.loop || current.index > 0) {
                    $(current.tpl.prev).appendTo(F.inner).bind('click.fb', F.prev);
                }
                if (current.loop || current.index < F.group.length - 1) {
                    $(current.tpl.next).appendTo(F.inner).bind('click.fb', F.next);
                }
            }
            F.trigger('afterShow');
            if (F.opts.autoPlay && !F.player.isActive) {
                F.opts.autoPlay = false;
                F.play();
            }
        },
        _afterZoomOut: function () {
            F.trigger('afterClose');
            F.wrap.trigger('onReset').remove();
            $.extend(F, {
                group: {},
                opts: {},
                current: null,
                isActive: false,
                isOpened: false,
                isOpen: false,
                wrap: null,
                outer: null,
                inner: null
            });
        }
    });
    F.transitions = {
        getOrigPosition: function () {
            var current = F.current,
                element = current.element,
                padding = current.padding,
                orig = $(current.orig),
                pos = {},
                width = 50,
                height = 50,
                viewport;
            if (!orig.length && current.isDom && $(element).is(':visible')) {
                orig = $(element).find('img:first');
                if (!orig.length) {
                    orig = $(element);
                }
            }
            if (orig.length) {
                pos = orig.offset();
                if (orig.is('img')) {
                    width = orig.outerWidth();
                    height = orig.outerHeight();
                }
            } else {
                viewport = F.getViewport();
                pos.top = viewport.y + (viewport.h - height) * 0.5;
                pos.left = viewport.x + (viewport.w - width) * 0.5;
            }
            pos = {
                top: Math.ceil(pos.top - padding) + 'px',
                left: Math.ceil(pos.left - padding) + 'px',
                width: Math.ceil(width + padding * 2) + 'px',
                height: Math.ceil(height + padding * 2) + 'px'
            };
            return pos;
        },
        step: function (now, fx) {
            var ratio, innerValue, outerValue;
            if (fx.prop === 'width' || fx.prop === 'height') {
                innerValue = outerValue = Math.ceil(now - (F.current.padding * 2));
                if (fx.prop === 'height') {
                    ratio = (now - fx.start) / (fx.end - fx.start);
                    if (fx.start > fx.end) {
                        ratio = 1 - ratio;
                    }
                    innerValue -= F.innerSpace * ratio;
                    outerValue -= F.outerSpace * ratio;
                }
                F.inner[fx.prop](innerValue);
                F.outer[fx.prop](outerValue);
            }
        },
        zoomIn: function () {
            var wrap = F.wrap,
                current = F.current,
                startPos, endPos, dim = current.dim;
            if (current.openEffect === 'elastic') {
                endPos = $.extend({}, dim, F._getPosition(true));
                delete endPos.position;
                startPos = this.getOrigPosition();
                if (current.openOpacity) {
                    startPos.opacity = 0;
                    endPos.opacity = 1;
                }
                F.outer.add(F.inner).width('auto').height('auto');
                wrap.css(startPos).show();
                wrap.animate(endPos, {
                    duration: current.openSpeed,
                    easing: current.openEasing,
                    step: this.step,
                    complete: F._afterZoomIn
                });
            } else {
                wrap.css($.extend({}, dim, F._getPosition()));
                if (current.openEffect === 'fade') {
                    wrap.fadeIn(current.openSpeed, F._afterZoomIn);
                } else {
                    wrap.show();
                    F._afterZoomIn();
                }
            }
        },
        zoomOut: function () {
            var wrap = F.wrap,
                current = F.current,
                endPos;
            if (current.closeEffect === 'elastic') {
                if (wrap.css('position') === 'fixed') {
                    wrap.css(F._getPosition(true));
                }
                endPos = this.getOrigPosition();
                if (current.closeOpacity) {
                    endPos.opacity = 0;
                }
                wrap.animate(endPos, {
                    duration: current.closeSpeed,
                    easing: current.closeEasing,
                    step: this.step,
                    complete: F._afterZoomOut
                });
            } else {
                wrap.fadeOut(current.closeEffect === 'fade' ? current.closeSpeed : 0, F._afterZoomOut);
            }
        },
        changeIn: function () {
            var wrap = F.wrap,
                current = F.current,
                startPos;
            if (current.nextEffect === 'elastic') {
                startPos = F._getPosition(true);
                startPos.opacity = 0;
                startPos.top = (parseInt(startPos.top, 10) - 200) + 'px';
                wrap.css(startPos).show().animate({
                    opacity: 1,
                    top: '+=200px'
                }, {
                    duration: current.nextSpeed,
                    easing: current.nextEasing,
                    complete: F._afterZoomIn
                });
            } else {
                wrap.css(F._getPosition());
                if (current.nextEffect === 'fade') {
                    wrap.hide().fadeIn(current.nextSpeed, F._afterZoomIn);
                } else {
                    wrap.show();
                    F._afterZoomIn();
                }
            }
        },
        changeOut: function () {
            var wrap = F.wrap,
                current = F.current,
                cleanUp = function () {
                    $(this).trigger('onReset').remove();
                };
            wrap.removeClass('fancybox-opened');
            if (current.prevEffect === 'elastic') {
                wrap.animate({
                    'opacity': 0,
                    top: '+=200px'
                }, {
                    duration: current.prevSpeed,
                    easing: current.prevEasing,
                    complete: cleanUp
                });
            } else {
                wrap.fadeOut(current.prevEffect === 'fade' ? current.prevSpeed : 0, cleanUp);
            }
        }
    };
    F.helpers.overlay = {
        overlay: null,
        update: function () {
            var width, scrollWidth, offsetWidth;
            this.overlay.width(0).height(0);
            if ($.browser.msie) {
                scrollWidth = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth);
                offsetWidth = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth);
                width = scrollWidth < offsetWidth ? W.width() : scrollWidth;
            } else {
                width = D.width();
            }
            this.overlay.width(width).height(D.height());
        },
        beforeShow: function (opts) {
            if (this.overlay) {
                return;
            }
            opts = $.extend(true, {
                speedIn: 'fast',
                closeClick: true,
                opacity: 1,
                css: {
                    background: 'black'
                }
            }, opts);
            this.overlay = $('<div id="fancybox-overlay"></div>').css(opts.css).appendTo('body');
            this.update();
            if (opts.closeClick) {
                this.overlay.bind('click.fb', F.close);
            }
            W.bind("resize.fb", $.proxy(this.update, this));
            this.overlay.fadeTo(opts.speedIn, opts.opacity);
        },
        onUpdate: function () {
            this.update();
        },
        afterClose: function (opts) {
            if (this.overlay) {
                this.overlay.fadeOut(opts.speedOut || 0, function () {
                    $(this).remove();
                });
            }
            this.overlay = null;
        }
    };
    F.helpers.title = {
        beforeShow: function (opts) {
            var title, text = F.current.title;
            if (text) {
                title = $('<div class="fancybox-title fancybox-title-' + opts.type + '-wrap">' + text + '</div>').appendTo('body');
                if (opts.type === 'float') {
                    title.width(title.width());
                    title.wrapInner('<span class="child"></span>');
                    F.current.margin[2] += Math.abs(parseInt(title.css('margin-bottom'), 10));
                }
                title.appendTo(opts.type === 'over' ? F.inner : (opts.type === 'outside' ? F.wrap : F.outer));
            }
        }
    };
    $.fn.fancybox = function (options) {
        var that = $(this),
            selector = this.selector || '',
            index, run = function (e) {
                var what = this,
                    relType = 'rel',
                    relVal = what[relType],
                    idx = index;
                if (!(e.ctrlKey || e.altKey || e.shiftKey || e.metaKey)) {
                    e.preventDefault();
                    if (!relVal) {
                        relType = 'data-fancybox-group';
                        relVal = $(what).attr('data-fancybox-group');
                    }
                    if (relVal && relVal !== '' && relVal !== 'nofollow') {
                        what = selector.length ? $(selector) : that;
                        what = what.filter('[' + relType + '="' + relVal + '"]');
                        idx = what.index(this);
                    }
                    options.index = idx;
                    F.open(what, options);
                }
            };
        options = options || {};
        index = options.index || 0;
        if (selector) {
            D.undelegate(selector, 'click.fb-start').delegate(selector, 'click.fb-start', run);
        } else {
            that.unbind('click.fb-start').bind('click.fb-start', run);
        }
        return this;
    };
}(window, document, jQuery));;
(function ($) {
    $.fn.hoverscroll = function (params) {
        if (!params) {
            params = {};
        }
        params = $.extend({}, $.fn.hoverscroll.params, params);
        this.each(function () {
            var $this = $(this);
            if (params.debug) {
                $.log('[HoverScroll] Trying to create hoverscroll on element ' + this.tagName + '#' + this.id);
            }
            if (params.fixedArrows) {
                $this.wrap('<div class="fixed-listcontainer"></div>')
            } else {
                $this.wrap('<div class="listcontainer"></div>');
            }
            $this.addClass('list');
            var listctnr = $this.parent();
            listctnr.wrap('<div class="ui-widget-content hoverscroll' +
                (params.rtl && !params.vertical ? " rtl" : "") + '"></div>');
            var ctnr = listctnr.parent();
            var leftArrow, rightArrow, topArrow, bottomArrow;
            if (params.arrows) {
                if (!params.vertical) {
                    if (params.fixedArrows) {
                        leftArrow = '<div class="fixed-arrow left"></div>';
                        rightArrow = '<div class="fixed-arrow right"></div>';
                        listctnr.before(leftArrow).after(rightArrow);
                    } else {
                        leftArrow = '<div class="arrow left"></div>';
                        rightArrow = '<div class="arrow right"></div>';
                        listctnr.append(leftArrow).append(rightArrow);
                    }
                } else {
                    if (params.fixedArrows) {
                        topArrow = '<div class="fixed-arrow top"></div>';
                        bottomArrow = '<div class="fixed-arrow bottom"></div>';
                        listctnr.before(topArrow).after(bottomArrow);
                    } else {
                        topArrow = '<div class="arrow top"></div>';
                        bottomArrow = '<div class="arrow bottom"></div>';
                        listctnr.append(topArrow).append(bottomArrow);
                    }
                }
            }
            ctnr.width(params.width).height(params.height);
            if (params.arrows && params.fixedArrows) {
                if (params.vertical) {
                    topArrow = listctnr.prev();
                    bottomArrow = listctnr.next();
                    listctnr.width(params.width).height(params.height - (topArrow.height() + bottomArrow.height()));
                } else {
                    leftArrow = listctnr.prev();
                    rightArrow = listctnr.next();
                    listctnr.height(params.height).width(params.width - (leftArrow.width() + rightArrow.width()));
                }
            } else {
                listctnr.width(params.width).height(params.height);
            }
            var size = 0;
            if (!params.vertical) {
                ctnr.addClass('horizontal');
                $this.children().each(function () {
                    $(this).addClass('item');
                    if ($(this).outerWidth) {
                        size += $(this).outerWidth(true);
                    } else {
                        size += $(this).width() + parseInt($(this).css('padding-left')) + parseInt($(this).css('padding-right')) + parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
                    }
                });
                $this.width(size);
                if (params.debug) {
                    $.log('[HoverScroll] Computed content width : ' + size + 'px');
                }
                if (ctnr.outerWidth) {
                    size = ctnr.outerWidth();
                } else {
                    size = ctnr.width() + parseInt(ctnr.css('padding-left')) + parseInt(ctnr.css('padding-right')) + parseInt(ctnr.css('margin-left')) + parseInt(ctnr.css('margin-right'));
                }
                if (params.debug) {
                    $.log('[HoverScroll] Computed container width : ' + size + 'px');
                }
            } else {
                ctnr.addClass('vertical');
                $this.children().each(function () {
                    $(this).addClass('item')
                    if ($(this).outerHeight) {
                        size += $(this).outerHeight(true);
                    } else {
                        size += $(this).height() + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom')) + parseInt($(this).css('margin-bottom')) + parseInt($(this).css('margin-bottom'));
                    }
                });
                $this.height(size);
                if (params.debug) {
                    $.log('[HoverScroll] Computed content height : ' + size + 'px');
                }
                if (ctnr.outerHeight) {
                    size = ctnr.outerHeight();
                } else {
                    size = ctnr.height() + parseInt(ctnr.css('padding-top')) + parseInt(ctnr.css('padding-bottom')) + parseInt(ctnr.css('margin-top')) + parseInt(ctnr.css('margin-bottom'));
                }
                if (params.debug) {
                    $.log('[HoverScroll] Computed container height : ' + size + 'px');
                }
            }
            var zone = {
                1: {
                    action: 'move',
                    from: 0,
                    to: 0.06 * size,
                    direction: -1,
                    speed: 16
                },
                2: {
                    action: 'move',
                    from: 0.06 * size,
                    to: 0.15 * size,
                    direction: -1,
                    speed: 8
                },
                3: {
                    action: 'move',
                    from: 0.15 * size,
                    to: 0.25 * size,
                    direction: -1,
                    speed: 4
                },
                4: {
                    action: 'move',
                    from: 0.25 * size,
                    to: 0.4 * size,
                    direction: -1,
                    speed: 2
                },
                5: {
                    action: 'stop',
                    from: 0.4 * size,
                    to: 0.6 * size
                },
                6: {
                    action: 'move',
                    from: 0.6 * size,
                    to: 0.75 * size,
                    direction: 1,
                    speed: 2
                },
                7: {
                    action: 'move',
                    from: 0.75 * size,
                    to: 0.85 * size,
                    direction: 1,
                    speed: 4
                },
                8: {
                    action: 'move',
                    from: 0.85 * size,
                    to: 0.94 * size,
                    direction: 1,
                    speed: 8
                },
                9: {
                    action: 'move',
                    from: 0.94 * size,
                    to: size,
                    direction: 1,
                    speed: 16
                }
            }
            var zone = {
                1: {
                    action: 'stop',
                    from: 0.4 * size,
                    to: 0.6 * size
                }
            }
            ctnr[0].isChanging = false;
            ctnr[0].direction = 0;
            ctnr[0].speed = 1;

            function checkMouse(x, y) {
                x = x - ctnr.offset().left;
                y = y - ctnr.offset().top;
                var pos;
                if (!params.vertical) {
                    pos = x;
                } else {
                    pos = y;
                }
                for (i in zone) {
                    if (pos >= zone[i].from && pos < zone[i].to) {
                        if (zone[i].action == 'move') {
                            startMoving(zone[i].direction, zone[i].speed);
                        } else {
                            stopMoving();
                        }
                    }
                }
            }

            function setArrowOpacity() {
                if (!params.arrows || params.fixedArrows) {
                    return;
                }
                var maxScroll;
                var scroll;
                if (!params.vertical) {
                    maxScroll = listctnr[0].scrollWidth - listctnr.width();
                    scroll = listctnr[0].scrollLeft;
                } else {
                    maxScroll = listctnr[0].scrollHeight - listctnr.height();
                    scroll = listctnr[0].scrollTop;
                }
                var limit = params.arrowsOpacity;
                var opacity = (scroll / maxScroll) * limit;
                if (opacity > limit) {
                    opacity = limit;
                }
                if (isNaN(opacity)) {
                    opacity = 0;
                }
                var done = false;
                if (opacity <= 0) {
                    $('div.arrow.left, div.arrow.top', ctnr).hide();
                    if (maxScroll > 0) {
                        $('div.arrow.right, div.arrow.bottom', ctnr).show().css('opacity', limit);
                    }
                    done = true;
                }
                if (opacity >= limit || maxScroll <= 0) {
                    $('div.arrow.right, div.arrow.bottom', ctnr).hide();
                    done = true;
                }
                if (!done) {
                    $('div.arrow.left, div.arrow.top', ctnr).show().css('opacity', opacity);
                    $('div.arrow.right, div.arrow.bottom', ctnr).show().css('opacity', (limit - opacity));
                }
            }

            function startMoving(direction, speed) {
                if (ctnr[0].direction != direction) {
                    if (params.debug) {
                        $.log('[HoverScroll] Starting to move. direction: ' + direction + ', speed: ' + speed);
                    }
                    stopMoving();
                    ctnr[0].direction = direction;
                    ctnr[0].isChanging = true;
                    move();
                }
                if (ctnr[0].speed != speed) {
                    if (params.debug) {
                        $.log('[HoverScroll] Changed speed: ' + speed);
                    }
                    ctnr[0].speed = speed;
                }
            }

            function stopMoving() {
                if (ctnr[0].isChanging) {
                    if (params.debug) {
                        $.log('[HoverScroll] Stoped moving');
                    }
                    ctnr[0].isChanging = false;
                    ctnr[0].direction = 0;
                    ctnr[0].speed = 1;
                    clearTimeout(ctnr[0].timer);
                }
            }

            function move() {
                if (ctnr[0].isChanging == false) {
                    return;
                }
                setArrowOpacity();
                var scrollSide;
                if (!params.vertical) {
                    scrollSide = 'scrollLeft';
                } else {
                    scrollSide = 'scrollTop';
                }
                listctnr[0][scrollSide] += ctnr[0].direction * ctnr[0].speed;
                ctnr[0].timer = setTimeout(function () {
                    move();
                }, 50);
            }
            if (params.rtl && !params.vertical) {
                listctnr[0].scrollLeft = listctnr[0].scrollWidth - listctnr.width();
            }
            ctnr.mousemove(function (e) {
                checkMouse(e.pageX, e.pageY);
            }).bind('mouseleave', function () {
                stopMoving();
            });
            this.startMoving = startMoving;
            this.stopMoving = stopMoving;
            if (params.arrows && !params.fixedArrows) {
                setArrowOpacity();
            } else {
                $('.arrowleft, .arrowright, .arrowtop, .arrowbottom', ctnr).hide();
            }
        });
        return this;
    };
    if (!$.fn.offset) {
        $.fn.offset = function () {
            this.left = this.top = 0;
            if (this[0] && this[0].offsetParent) {
                var obj = this[0];
                do {
                    this.left += obj.offsetLeft;
                    this.top += obj.offsetTop;
                } while (obj = obj.offsetParent);
            }
            return this;
        }
    }
    $.fn.hoverscroll.params = {
        vertical: false,
        width: 400,
        height: 50,
        arrows: true,
        arrowsOpacity: 0.7,
        fixedArrows: false,
        rtl: false,
        debug: false
    };
    $.log = function () {
        try {
            console.log.apply(console, arguments);
        } catch (e) {
            try {
                opera.postError.apply(opera, arguments);
            } catch (e) {}
        }
    };
})(jQuery);;
/*
 * jScrollPane - v2.0.0beta12 - 2012-05-14
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2010 Kelvin Luck
 * Dual licensed under the MIT and GPL licenses.
 */
(function (b, a, c) {
    b.fn.jScrollPane = function (e) {
        function d(D, O) {
            var ay, Q = this,
                Y, aj, v, al, T, Z, y, q, az, aE, au, i, I, h, j, aa, U, ap, X, t, A, aq, af, am, G, l, at, ax, x, av, aH, f, L, ai = true,
                P = true,
                aG = false,
                k = false,
                ao = D.clone(false, false).empty(),
                ac = b.fn.mwheelIntent ? "mwheelIntent.jsp" : "mousewheel.jsp";
            aH = D.css("paddingTop") + " " + D.css("paddingRight") + " " + D.css("paddingBottom") + " " + D.css("paddingLeft");
            f = (parseInt(D.css("paddingLeft"), 10) || 0) + (parseInt(D.css("paddingRight"), 10) || 0);

            function ar(aQ) {
                var aL, aN, aM, aJ, aI, aP, aO = false,
                    aK = false;
                ay = aQ;
                if (Y === c) {
                    aI = D.scrollTop();
                    aP = D.scrollLeft();
                    D.css({
                        overflow: "hidden",
                        padding: 0
                    });
                    aj = D.innerWidth() + f;
                    v = D.innerHeight();
                    D.width(aj);
                    Y = b('<div class="jspPane" />').css("padding", aH).append(D.children());
                    al = b('<div class="jspContainer" />').css({
                        width: aj + "px",
                        height: v + "px"
                    }).append(Y).appendTo(D)
                } else {
                    D.css("width", "");
                    aO = ay.stickToBottom && K();
                    aK = ay.stickToRight && B();
                    aJ = D.innerWidth() + f != aj || D.outerHeight() != v;
                    if (aJ) {
                        aj = D.innerWidth() + f;
                        v = D.innerHeight();
                        al.css({
                            width: aj + "px",
                            height: v + "px"
                        })
                    }
                    if (!aJ && L == T && Y.outerHeight() == Z) {
                        D.width(aj);
                        return
                    }
                    L = T;
                    Y.css("width", "");
                    D.width(aj);
                    al.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()
                }
                Y.css("overflow", "auto");
                if (aQ.contentWidth) {
                    T = aQ.contentWidth
                } else {
                    T = Y[0].scrollWidth
                }
                Z = Y[0].scrollHeight;
                Y.css("overflow", "");
                y = T / aj;
                q = Z / v;
                az = q > 1;
                aE = y > 1;
                if (!(aE || az)) {
                    D.removeClass("jspScrollable");
                    Y.css({
                        top: 0,
                        width: al.width() - f
                    });
                    n();
                    E();
                    R();
                    w()
                } else {
                    D.addClass("jspScrollable");
                    aL = ay.maintainPosition && (I || aa);
                    if (aL) {
                        aN = aC();
                        aM = aA()
                    }
                    aF();
                    z();
                    F();
                    if (aL) {
                        N(aK ? (T - aj) : aN, false);
                        M(aO ? (Z - v) : aM, false)
                    }
                    J();
                    ag();
                    an();
                    if (ay.enableKeyboardNavigation) {
                        S()
                    }
                    if (ay.clickOnTrack) {
                        p()
                    }
                    C();
                    if (ay.hijackInternalLinks) {
                        m()
                    }
                } if (ay.autoReinitialise && !av) {
                    av = setInterval(function () {
                        ar(ay)
                    }, ay.autoReinitialiseDelay)
                } else {
                    if (!ay.autoReinitialise && av) {
                        clearInterval(av)
                    }
                }
                aI && D.scrollTop(0) && M(aI, false);
                aP && D.scrollLeft(0) && N(aP, false);
                D.trigger("jsp-initialised", [aE || az])
            }

            function aF() {
                if (az) {
                    al.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'), b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'), b('<div class="jspDragBottom" />'))), b('<div class="jspCap jspCapBottom" />')));
                    U = al.find(">.jspVerticalBar");
                    ap = U.find(">.jspTrack");
                    au = ap.find(">.jspDrag");
                    if (ay.showArrows) {
                        aq = b('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp", aD(0, -1)).bind("click.jsp", aB);
                        af = b('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp", aD(0, 1)).bind("click.jsp", aB);
                        if (ay.arrowScrollOnHover) {
                            aq.bind("mouseover.jsp", aD(0, -1, aq));
                            af.bind("mouseover.jsp", aD(0, 1, af))
                        }
                        ak(ap, ay.verticalArrowPositions, aq, af)
                    }
                    t = v;
                    al.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function () {
                        t -= b(this).outerHeight()
                    });
                    au.hover(function () {
                        au.addClass("jspHover")
                    }, function () {
                        au.removeClass("jspHover")
                    }).bind("mousedown.jsp", function (aI) {
                        b("html").bind("dragstart.jsp selectstart.jsp", aB);
                        au.addClass("jspActive");
                        var s = aI.pageY - au.position().top;
                        b("html").bind("mousemove.jsp", function (aJ) {
                            V(aJ.pageY - s, false)
                        }).bind("mouseup.jsp mouseleave.jsp", aw);
                        return false
                    });
                    o()
                }
            }

            function o() {
                ap.height(t + "px");
                I = 0;
                X = ay.verticalGutter + ap.outerWidth();
                Y.width(aj - X - f);
                try {
                    if (U.position().left === 0) {
                        Y.css("margin-left", X + "px")
                    }
                } catch (s) {}
            }

            function z() {
                if (aE) {
                    al.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'), b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'), b('<div class="jspDragRight" />'))), b('<div class="jspCap jspCapRight" />')));
                    am = al.find(">.jspHorizontalBar");
                    G = am.find(">.jspTrack");
                    h = G.find(">.jspDrag");
                    if (ay.showArrows) {
                        ax = b('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp", aD(-1, 0)).bind("click.jsp", aB);
                        x = b('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp", aD(1, 0)).bind("click.jsp", aB);
                        if (ay.arrowScrollOnHover) {
                            ax.bind("mouseover.jsp", aD(-1, 0, ax));
                            x.bind("mouseover.jsp", aD(1, 0, x))
                        }
                        ak(G, ay.horizontalArrowPositions, ax, x)
                    }
                    h.hover(function () {
                        h.addClass("jspHover")
                    }, function () {
                        h.removeClass("jspHover")
                    }).bind("mousedown.jsp", function (aI) {
                        b("html").bind("dragstart.jsp selectstart.jsp", aB);
                        h.addClass("jspActive");
                        var s = aI.pageX - h.position().left;
                        b("html").bind("mousemove.jsp", function (aJ) {
                            W(aJ.pageX - s, false)
                        }).bind("mouseup.jsp mouseleave.jsp", aw);
                        return false
                    });
                    l = al.innerWidth();
                    ah()
                }
            }

            function ah() {
                al.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function () {
                    l -= b(this).outerWidth()
                });
                G.width(l + "px");
                aa = 0
            }

            function F() {
                if (aE && az) {
                    var aI = G.outerHeight(),
                        s = ap.outerWidth();
                    t -= aI;
                    b(am).find(">.jspCap:visible,>.jspArrow").each(function () {
                        l += b(this).outerWidth()
                    });
                    l -= s;
                    v -= s;
                    aj -= aI;
                    G.parent().append(b('<div class="jspCorner" />').css("width", aI + "px"));
                    o();
                    ah()
                }
                if (aE) {
                    Y.width((al.outerWidth() - f) + "px")
                }
                Z = Y.outerHeight();
                q = Z / v;
                if (aE) {
                    at = Math.ceil(1 / y * l);
                    if (at > ay.horizontalDragMaxWidth) {
                        at = ay.horizontalDragMaxWidth
                    } else {
                        if (at < ay.horizontalDragMinWidth) {
                            at = ay.horizontalDragMinWidth
                        }
                    }
                    h.width(at + "px");
                    j = l - at;
                    ae(aa)
                }
                if (az) {
                    A = Math.ceil(1 / q * t);
                    if (A > ay.verticalDragMaxHeight) {
                        A = ay.verticalDragMaxHeight
                    } else {
                        if (A < ay.verticalDragMinHeight) {
                            A = ay.verticalDragMinHeight
                        }
                    }
                    au.height(A + "px");
                    i = t - A;
                    ad(I)
                }
            }

            function ak(aJ, aL, aI, s) {
                var aN = "before",
                    aK = "after",
                    aM;
                if (aL == "os") {
                    aL = /Mac/.test(navigator.platform) ? "after" : "split"
                }
                if (aL == aN) {
                    aK = aL
                } else {
                    if (aL == aK) {
                        aN = aL;
                        aM = aI;
                        aI = s;
                        s = aM
                    }
                }
                aJ[aN](aI)[aK](s)
            }

            function aD(aI, s, aJ) {
                return function () {
                    H(aI, s, this, aJ);
                    this.blur();
                    return false
                }
            }

            function H(aL, aK, aO, aN) {
                aO = b(aO).addClass("jspActive");
                var aM, aJ, aI = true,
                    s = function () {
                        if (aL !== 0) {
                            Q.scrollByX(aL * ay.arrowButtonSpeed)
                        }
                        if (aK !== 0) {
                            Q.scrollByY(aK * ay.arrowButtonSpeed)
                        }
                        aJ = setTimeout(s, aI ? ay.initialDelay : ay.arrowRepeatFreq);
                        aI = false
                    };
                s();
                aM = aN ? "mouseout.jsp" : "mouseup.jsp";
                aN = aN || b("html");
                aN.bind(aM, function () {
                    aO.removeClass("jspActive");
                    aJ && clearTimeout(aJ);
                    aJ = null;
                    aN.unbind(aM)
                })
            }

            function p() {
                w();
                if (az) {
                    ap.bind("mousedown.jsp", function (aN) {
                        if (aN.originalTarget === c || aN.originalTarget == aN.currentTarget) {
                            var aL = b(this),
                                aO = aL.offset(),
                                aM = aN.pageY - aO.top - I,
                                aJ, aI = true,
                                s = function () {
                                    var aR = aL.offset(),
                                        aS = aN.pageY - aR.top - A / 2,
                                        aP = v * ay.scrollPagePercent,
                                        aQ = i * aP / (Z - v);
                                    if (aM < 0) {
                                        if (I - aQ > aS) {
                                            Q.scrollByY(-aP)
                                        } else {
                                            V(aS)
                                        }
                                    } else {
                                        if (aM > 0) {
                                            if (I + aQ < aS) {
                                                Q.scrollByY(aP)
                                            } else {
                                                V(aS)
                                            }
                                        } else {
                                            aK();
                                            return
                                        }
                                    }
                                    aJ = setTimeout(s, aI ? ay.initialDelay : ay.trackClickRepeatFreq);
                                    aI = false
                                },
                                aK = function () {
                                    aJ && clearTimeout(aJ);
                                    aJ = null;
                                    b(document).unbind("mouseup.jsp", aK)
                                };
                            s();
                            b(document).bind("mouseup.jsp", aK);
                            return false
                        }
                    })
                }
                if (aE) {
                    G.bind("mousedown.jsp", function (aN) {
                        if (aN.originalTarget === c || aN.originalTarget == aN.currentTarget) {
                            var aL = b(this),
                                aO = aL.offset(),
                                aM = aN.pageX - aO.left - aa,
                                aJ, aI = true,
                                s = function () {
                                    var aR = aL.offset(),
                                        aS = aN.pageX - aR.left - at / 2,
                                        aP = aj * ay.scrollPagePercent,
                                        aQ = j * aP / (T - aj);
                                    if (aM < 0) {
                                        if (aa - aQ > aS) {
                                            Q.scrollByX(-aP)
                                        } else {
                                            W(aS)
                                        }
                                    } else {
                                        if (aM > 0) {
                                            if (aa + aQ < aS) {
                                                Q.scrollByX(aP)
                                            } else {
                                                W(aS)
                                            }
                                        } else {
                                            aK();
                                            return
                                        }
                                    }
                                    aJ = setTimeout(s, aI ? ay.initialDelay : ay.trackClickRepeatFreq);
                                    aI = false
                                },
                                aK = function () {
                                    aJ && clearTimeout(aJ);
                                    aJ = null;
                                    b(document).unbind("mouseup.jsp", aK)
                                };
                            s();
                            b(document).bind("mouseup.jsp", aK);
                            return false
                        }
                    })
                }
            }

            function w() {
                if (G) {
                    G.unbind("mousedown.jsp")
                }
                if (ap) {
                    ap.unbind("mousedown.jsp")
                }
            }

            function aw() {
                b("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp");
                if (au) {
                    au.removeClass("jspActive")
                }
                if (h) {
                    h.removeClass("jspActive")
                }
            }

            function V(s, aI) {
                if (!az) {
                    return
                }
                if (s < 0) {
                    s = 0
                } else {
                    if (s > i) {
                        s = i
                    }
                } if (aI === c) {
                    aI = ay.animateScroll
                }
                if (aI) {
                    Q.animate(au, "top", s, ad)
                } else {
                    au.css("top", s);
                    ad(s)
                }
            }

            function ad(aI) {
                if (aI === c) {
                    aI = au.position().top
                }
                al.scrollTop(0);
                I = aI;
                var aL = I === 0,
                    aJ = I == i,
                    aK = aI / i,
                    s = -aK * (Z - v);
                if (ai != aL || aG != aJ) {
                    ai = aL;
                    aG = aJ;
                    D.trigger("jsp-arrow-change", [ai, aG, P, k])
                }
                u(aL, aJ);
                Y.css("top", s);
                D.trigger("jsp-scroll-y", [-s, aL, aJ]).trigger("scroll")
            }

            function W(aI, s) {
                if (!aE) {
                    return
                }
                if (aI < 0) {
                    aI = 0
                } else {
                    if (aI > j) {
                        aI = j
                    }
                } if (s === c) {
                    s = ay.animateScroll
                }
                if (s) {
                    Q.animate(h, "left", aI, ae)
                } else {
                    h.css("left", aI);
                    ae(aI)
                }
            }

            function ae(aI) {
                if (aI === c) {
                    aI = h.position().left
                }
                al.scrollTop(0);
                aa = aI;
                var aL = aa === 0,
                    aK = aa == j,
                    aJ = aI / j,
                    s = -aJ * (T - aj);
                if (P != aL || k != aK) {
                    P = aL;
                    k = aK;
                    D.trigger("jsp-arrow-change", [ai, aG, P, k])
                }
                r(aL, aK);
                Y.css("left", s);
                D.trigger("jsp-scroll-x", [-s, aL, aK]).trigger("scroll")
            }

            function u(aI, s) {
                if (ay.showArrows) {
                    aq[aI ? "addClass" : "removeClass"]("jspDisabled");
                    af[s ? "addClass" : "removeClass"]("jspDisabled")
                }
            }

            function r(aI, s) {
                if (ay.showArrows) {
                    ax[aI ? "addClass" : "removeClass"]("jspDisabled");
                    x[s ? "addClass" : "removeClass"]("jspDisabled")
                }
            }

            function M(s, aI) {
                var aJ = s / (Z - v);
                V(aJ * i, aI)
            }

            function N(aI, s) {
                var aJ = aI / (T - aj);
                W(aJ * j, s)
            }

            function ab(aV, aQ, aJ) {
                var aN, aK, aL, s = 0,
                    aU = 0,
                    aI, aP, aO, aS, aR, aT;
                try {
                    aN = b(aV)
                } catch (aM) {
                    return
                }
                aK = aN.outerHeight();
                aL = aN.outerWidth();
                al.scrollTop(0);
                al.scrollLeft(0);
                while (!aN.is(".jspPane")) {
                    s += aN.position().top;
                    aU += aN.position().left;
                    aN = aN.offsetParent();
                    if (/^body|html$/i.test(aN[0].nodeName)) {
                        return
                    }
                }
                aI = aA();
                aO = aI + v;
                if (s < aI || aQ) {
                    aR = s - ay.verticalGutter
                } else {
                    if (s + aK > aO) {
                        aR = s - v + aK + ay.verticalGutter
                    }
                } if (aR) {
                    M(aR, aJ)
                }
                aP = aC();
                aS = aP + aj;
                if (aU < aP || aQ) {
                    aT = aU - ay.horizontalGutter
                } else {
                    if (aU + aL > aS) {
                        aT = aU - aj + aL + ay.horizontalGutter
                    }
                } if (aT) {
                    N(aT, aJ)
                }
            }

            function aC() {
                return -Y.position().left
            }

            function aA() {
                return -Y.position().top
            }

            function K() {
                var s = Z - v;
                return (s > 20) && (s - aA() < 10)
            }

            function B() {
                var s = T - aj;
                return (s > 20) && (s - aC() < 10)
            }

            function ag() {
                al.unbind(ac).bind(ac, function (aL, aM, aK, aI) {
                    var aJ = aa,
                        s = I;
                    Q.scrollBy(aK * ay.mouseWheelSpeed, -aI * ay.mouseWheelSpeed, false);
                    return aJ == aa && s == I
                })
            }

            function n() {
                al.unbind(ac)
            }

            function aB() {
                return false
            }

            function J() {
                Y.find(":input,a").unbind("focus.jsp").bind("focus.jsp", function (s) {
                    ab(s.target, false)
                })
            }

            function E() {
                Y.find(":input,a").unbind("focus.jsp")
            }

            function S() {
                var s, aI, aK = [];
                aE && aK.push(am[0]);
                az && aK.push(U[0]);
                Y.focus(function () {
                    D.focus()
                });
                D.attr("tabindex", 0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp", function (aN) {
                    if (aN.target !== this && !(aK.length && b(aN.target).closest(aK).length)) {
                        return
                    }
                    var aM = aa,
                        aL = I;
                    switch (aN.keyCode) {
                    case 40:
                    case 38:
                    case 34:
                    case 32:
                    case 33:
                    case 39:
                    case 37:
                        s = aN.keyCode;
                        aJ();
                        break;
                    case 35:
                        M(Z - v);
                        s = null;
                        break;
                    case 36:
                        M(0);
                        s = null;
                        break
                    }
                    aI = aN.keyCode == s && aM != aa || aL != I;
                    return !aI
                }).bind("keypress.jsp", function (aL) {
                    if (aL.keyCode == s) {
                        aJ()
                    }
                    return !aI
                });
                if (ay.hideFocus) {
                    D.css("outline", "none");
                    if ("hideFocus" in al[0]) {
                        D.attr("hideFocus", true)
                    }
                } else {
                    D.css("outline", "");
                    if ("hideFocus" in al[0]) {
                        D.attr("hideFocus", false)
                    }
                }

                function aJ() {
                    var aM = aa,
                        aL = I;
                    switch (s) {
                    case 40:
                        Q.scrollByY(ay.keyboardSpeed, false);
                        break;
                    case 38:
                        Q.scrollByY(-ay.keyboardSpeed, false);
                        break;
                    case 34:
                    case 32:
                        Q.scrollByY(v * ay.scrollPagePercent, false);
                        break;
                    case 33:
                        Q.scrollByY(-v * ay.scrollPagePercent, false);
                        break;
                    case 39:
                        Q.scrollByX(ay.keyboardSpeed, false);
                        break;
                    case 37:
                        Q.scrollByX(-ay.keyboardSpeed, false);
                        break
                    }
                    aI = aM != aa || aL != I;
                    return aI
                }
            }

            function R() {
                D.attr("tabindex", "-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp")
            }

            function C() {
                if (location.hash && location.hash.length > 1) {
                    var aK, aI, aJ = escape(location.hash.substr(1));
                    try {
                        aK = b("#" + aJ + ', a[name="' + aJ + '"]')
                    } catch (s) {
                        return
                    }
                    if (aK.length && Y.find(aJ)) {
                        if (al.scrollTop() === 0) {
                            aI = setInterval(function () {
                                if (al.scrollTop() > 0) {
                                    ab(aK, true);
                                    b(document).scrollTop(al.position().top);
                                    clearInterval(aI)
                                }
                            }, 50)
                        } else {
                            ab(aK, true);
                            b(document).scrollTop(al.position().top)
                        }
                    }
                }
            }

            function m() {
                if (b(document.body).data("jspHijack")) {
                    return
                }
                b(document.body).data("jspHijack", true);
                b(document.body).delegate("a[href*=#]", "click", function (s) {
                    var aI = this.href.substr(0, this.href.indexOf("#")),
                        aK = location.href,
                        aO, aP, aJ, aM, aL, aN;
                    if (location.href.indexOf("#") !== -1) {
                        aK = location.href.substr(0, location.href.indexOf("#"))
                    }
                    if (aI !== aK) {
                        return
                    }
                    aO = escape(this.href.substr(this.href.indexOf("#") + 1));
                    aP;
                    try {
                        aP = b("#" + aO + ', a[name="' + aO + '"]')
                    } catch (aQ) {
                        return
                    }
                    if (!aP.length) {
                        return
                    }
                    aJ = aP.closest(".jspScrollable");
                    aM = aJ.data("jsp");
                    aM.scrollToElement(aP, true);
                    if (aJ[0].scrollIntoView) {
                        aL = b(a).scrollTop();
                        aN = aP.offset().top;
                        if (aN < aL || aN > aL + b(a).height()) {
                            aJ[0].scrollIntoView()
                        }
                    }
                    s.preventDefault()
                })
            }

            function an() {
                var aJ, aI, aL, aK, aM, s = false;
                al.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp", function (aN) {
                    var aO = aN.originalEvent.touches[0];
                    aJ = aC();
                    aI = aA();
                    aL = aO.pageX;
                    aK = aO.pageY;
                    aM = false;
                    s = true
                }).bind("touchmove.jsp", function (aQ) {
                    if (!s) {
                        return
                    }
                    var aP = aQ.originalEvent.touches[0],
                        aO = aa,
                        aN = I;
                    Q.scrollTo(aJ + aL - aP.pageX, aI + aK - aP.pageY);
                    aM = aM || Math.abs(aL - aP.pageX) > 5 || Math.abs(aK - aP.pageY) > 5;
                    return aO == aa && aN == I
                }).bind("touchend.jsp", function (aN) {
                    s = false
                }).bind("click.jsp-touchclick", function (aN) {
                    if (aM) {
                        aM = false;
                        return false
                    }
                })
            }

            function g() {
                var s = aA(),
                    aI = aC();
                D.removeClass("jspScrollable").unbind(".jsp");
                D.replaceWith(ao.append(Y.children()));
                ao.scrollTop(s);
                ao.scrollLeft(aI);
                if (av) {
                    clearInterval(av)
                }
            }
            b.extend(Q, {
                reinitialise: function (aI) {
                    aI = b.extend({}, ay, aI);
                    ar(aI)
                },
                scrollToElement: function (aJ, aI, s) {
                    ab(aJ, aI, s)
                },
                scrollTo: function (aJ, s, aI) {
                    N(aJ, aI);
                    M(s, aI)
                },
                scrollToX: function (aI, s) {
                    N(aI, s)
                },
                scrollToY: function (s, aI) {
                    M(s, aI)
                },
                scrollToPercentX: function (aI, s) {
                    N(aI * (T - aj), s)
                },
                scrollToPercentY: function (aI, s) {
                    M(aI * (Z - v), s)
                },
                scrollBy: function (aI, s, aJ) {
                    Q.scrollByX(aI, aJ);
                    Q.scrollByY(s, aJ)
                },
                scrollByX: function (s, aJ) {
                    var aI = aC() + Math[s < 0 ? "floor" : "ceil"](s),
                        aK = aI / (T - aj);
                    W(aK * j, aJ)
                },
                scrollByY: function (s, aJ) {
                    var aI = aA() + Math[s < 0 ? "floor" : "ceil"](s),
                        aK = aI / (Z - v);
                    V(aK * i, aJ)
                },
                positionDragX: function (s, aI) {
                    W(s, aI)
                },
                positionDragY: function (aI, s) {
                    V(aI, s)
                },
                animate: function (aI, aL, s, aK) {
                    var aJ = {};
                    aJ[aL] = s;
                    aI.animate(aJ, {
                        duration: ay.animateDuration,
                        easing: ay.animateEase,
                        queue: false,
                        step: aK
                    })
                },
                getContentPositionX: function () {
                    return aC()
                },
                getContentPositionY: function () {
                    return aA()
                },
                getContentWidth: function () {
                    return T
                },
                getContentHeight: function () {
                    return Z
                },
                getPercentScrolledX: function () {
                    return aC() / (T - aj)
                },
                getPercentScrolledY: function () {
                    return aA() / (Z - v)
                },
                getIsScrollableH: function () {
                    return aE
                },
                getIsScrollableV: function () {
                    return az
                },
                getContentPane: function () {
                    return Y
                },
                scrollToBottom: function (s) {
                    V(i, s)
                },
                hijackInternalLinks: b.noop,
                destroy: function () {
                    g()
                }
            });
            ar(O)
        }
        e = b.extend({}, b.fn.jScrollPane.defaults, e);
        b.each(["mouseWheelSpeed", "arrowButtonSpeed", "trackClickSpeed", "keyboardSpeed"], function () {
            e[this] = e[this] || e.speed
        });
        return this.each(function () {
            var f = b(this),
                g = f.data("jsp");
            if (g) {
                g.reinitialise(e)
            } else {
                g = new d(f, e);
                f.data("jsp", g)
            }
        })
    };
    b.fn.jScrollPane.defaults = {
        showArrows: false,
        maintainPosition: true,
        stickToBottom: false,
        stickToRight: false,
        clickOnTrack: true,
        autoReinitialise: false,
        autoReinitialiseDelay: 500,
        verticalDragMinHeight: 0,
        verticalDragMaxHeight: 99999,
        horizontalDragMinWidth: 0,
        horizontalDragMaxWidth: 99999,
        contentWidth: c,
        animateScroll: false,
        animateDuration: 300,
        animateEase: "linear",
        hijackInternalLinks: false,
        verticalGutter: 4,
        horizontalGutter: 4,
        mouseWheelSpeed: 0,
        arrowButtonSpeed: 0,
        arrowRepeatFreq: 50,
        arrowScrollOnHover: false,
        trackClickSpeed: 0,
        trackClickRepeatFreq: 70,
        verticalArrowPositions: "split",
        horizontalArrowPositions: "split",
        enableKeyboardNavigation: true,
        hideFocus: false,
        keyboardSpeed: 0,
        initialDelay: 300,
        speed: 30,
        scrollPagePercent: 0.8
    }
})(jQuery, this);;
(function ($) {
    function int_prop(a) {
        a.elem.style[a.prop] = parseInt(a.now, 10) + a.unit
    }
    var j = function (a) {
        throw ({
            name: "jquery.flip.js plugin error",
            message: a
        })
    };
    var k = function () {
        return ( /*@cc_on!@*/ false && (typeof document.body.style.maxHeight === "undefined"))
    };
    var l = {
        aqua: [0, 255, 255],
        azure: [240, 255, 255],
        beige: [245, 245, 220],
        black: [0, 0, 0],
        blue: [0, 0, 255],
        brown: [165, 42, 42],
        cyan: [0, 255, 255],
        darkblue: [0, 0, 139],
        darkcyan: [0, 139, 139],
        darkgrey: [169, 169, 169],
        darkgreen: [0, 100, 0],
        darkkhaki: [189, 183, 107],
        darkmagenta: [139, 0, 139],
        darkolivegreen: [85, 107, 47],
        darkorange: [255, 140, 0],
        darkorchid: [153, 50, 204],
        darkred: [139, 0, 0],
        darksalmon: [233, 150, 122],
        darkviolet: [148, 0, 211],
        fuchsia: [255, 0, 255],
        gold: [255, 215, 0],
        green: [0, 128, 0],
        indigo: [75, 0, 130],
        khaki: [240, 230, 140],
        lightblue: [173, 216, 230],
        lightcyan: [224, 255, 255],
        lightgreen: [144, 238, 144],
        lightgrey: [211, 211, 211],
        lightpink: [255, 182, 193],
        lightyellow: [255, 255, 224],
        lime: [0, 255, 0],
        magenta: [255, 0, 255],
        maroon: [128, 0, 0],
        navy: [0, 0, 128],
        olive: [128, 128, 0],
        orange: [255, 165, 0],
        pink: [255, 192, 203],
        purple: [128, 0, 128],
        violet: [128, 0, 128],
        red: [255, 0, 0],
        silver: [192, 192, 192],
        white: [255, 255, 255],
        yellow: [255, 255, 0],
        transparent: [255, 255, 255]
    };
    var m = function (a) {
        if (a && a.indexOf("#") == -1 && a.indexOf("(") == -1) {
            return "rgb(" + l[a].toString() + ")"
        } else {
            return a
        }
    };
    $.extend($.fx.step, {
        borderTopWidth: int_prop,
        borderBottomWidth: int_prop,
        borderLeftWidth: int_prop,
        borderRightWidth: int_prop
    });
    $.fn.revertFlip = function () {
        return this.each(function () {
            var a = $(this);
            a.flip(a.data('flipRevertedSettings'))
        })
    };
    $.fn.flip = function (i) {
        return this.each(function () {
            var c = $(this),
                flipObj, $clone, dirOption, dirOptions, newContent, ie6 = k();
            if (c.data('flipLock')) {
                return false
            }
            var e = {
                direction: (function (a) {
                    switch (a) {
                    case "tb":
                        return "bt";
                    case "bt":
                        return "tb";
                    case "lr":
                        return "rl";
                    case "rl":
                        return "lr";
                    default:
                        return "bt"
                    }
                })(i.direction),
                bgColor: m(i.color) || "#999",
                color: m(i.bgColor) || c.css("background-color"),
                content: c.html(),
                speed: i.speed || 500,
                onBefore: i.onBefore || function () {},
                onEnd: i.onEnd || function () {},
                onAnimation: i.onAnimation || function () {}
            };
            c.data('flipRevertedSettings', e).data('flipLock', 1).data('flipSettings', e);
            flipObj = {
                width: c.width(),
                height: c.height(),
                bgColor: m(i.bgColor) || c.css("background-color"),
                fontSize: c.css("font-size") || "12px",
                direction: i.direction || "tb",
                toColor: m(i.color) || "#999",
                speed: i.speed || 500,
                top: c.offset().top,
                left: c.offset().left,
                target: i.content || null,
                transparent: "transparent",
                dontChangeColor: i.dontChangeColor || false,
                onBefore: i.onBefore || function () {},
                onEnd: i.onEnd || function () {},
                onAnimation: i.onAnimation || function () {}
            };
            ie6 && (flipObj.transparent = "#123456");
            $clone = c.css("visibility", "hidden").clone(true).data('flipLock', 1).appendTo("body").html("").css({
                visibility: "visible",
                position: "absolute",
                left: flipObj.left,
                top: flipObj.top,
                margin: 0,
                zIndex: 9999,
                "-webkit-box-shadow": "0px 0px 0px #000",
                "-moz-box-shadow": "0px 0px 0px #000"
            });
            var f = function () {
                return {
                    backgroundColor: flipObj.transparent,
                    fontSize: 0,
                    lineHeight: 0,
                    borderTopWidth: 0,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                    borderBottomWidth: 0,
                    borderTopColor: flipObj.transparent,
                    borderBottomColor: flipObj.transparent,
                    borderLeftColor: flipObj.transparent,
                    borderRightColor: flipObj.transparent,
                    background: "none",
                    borderStyle: 'solid',
                    height: 0,
                    width: 0
                }
            };
            var g = function () {
                var a = (flipObj.height / 100) * 25;
                var b = f();
                b.width = flipObj.width;
                return {
                    "start": b,
                    "first": {
                        borderTopWidth: 0,
                        borderLeftWidth: a,
                        borderRightWidth: a,
                        borderBottomWidth: 0,
                        borderTopColor: '#999',
                        borderBottomColor: '#999',
                        top: (flipObj.top + (flipObj.height / 2)),
                        left: (flipObj.left - a)
                    },
                    "second": {
                        borderBottomWidth: 0,
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderTopColor: flipObj.transparent,
                        borderBottomColor: flipObj.transparent,
                        top: flipObj.top,
                        left: flipObj.left
                    }
                }
            };
            var h = function () {
                var a = (flipObj.height / 100) * 25;
                var b = f();
                b.height = flipObj.height;
                return {
                    "start": b,
                    "first": {
                        borderTopWidth: a,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomWidth: a,
                        borderLeftColor: '#999',
                        borderRightColor: '#999',
                        top: flipObj.top - a,
                        left: flipObj.left + (flipObj.width / 2)
                    },
                    "second": {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomWidth: 0,
                        borderLeftColor: flipObj.transparent,
                        borderRightColor: flipObj.transparent,
                        top: flipObj.top,
                        left: flipObj.left
                    }
                }
            };
            dirOptions = {
                "tb": function () {
                    var d = g();
                    d.start.borderTopWidth = flipObj.height;
                    d.start.borderTopColor = flipObj.bgColor;
                    d.second.borderBottomWidth = flipObj.height;
                    d.second.borderBottomColor = flipObj.toColor;
                    return d
                },
                "bt": function () {
                    var d = g();
                    d.start.borderBottomWidth = flipObj.height;
                    d.start.borderBottomColor = flipObj.bgColor;
                    d.second.borderTopWidth = flipObj.height;
                    d.second.borderTopColor = flipObj.toColor;
                    return d
                },
                "lr": function () {
                    var d = h();
                    d.start.borderLeftWidth = flipObj.width;
                    d.start.borderLeftColor = flipObj.bgColor;
                    d.second.borderRightWidth = flipObj.width;
                    d.second.borderRightColor = flipObj.toColor;
                    return d
                },
                "rl": function () {
                    var d = h();
                    d.start.borderRightWidth = flipObj.width;
                    d.start.borderRightColor = flipObj.bgColor;
                    d.second.borderLeftWidth = flipObj.width;
                    d.second.borderLeftColor = flipObj.toColor;
                    return d
                }
            };
            dirOption = dirOptions[flipObj.direction]();
            ie6 && (dirOption.start.filter = "chroma(color=" + flipObj.transparent + ")");
            newContent = function () {
                var a = flipObj.target;
                return a && a.jquery ? a.html() : a
            };
            $clone.queue(function () {
                flipObj.onBefore($clone, c);
                $clone.html('').css(dirOption.start);
                $clone.dequeue()
            });
            $clone.animate(dirOption.first, flipObj.speed);
            $clone.queue(function () {
                flipObj.onAnimation($clone, c);
                $clone.dequeue()
            });
            $clone.animate(dirOption.second, flipObj.speed);
            $clone.queue(function () {
                if (!flipObj.dontChangeColor) {
                    c.css({
                        backgroundColor: flipObj.toColor
                    })
                }
                c.css({
                    visibility: "visible"
                });
                var a = newContent();
                if (a) {
                    c.html(a)
                }
                $clone.remove();
                flipObj.onEnd($clone, c);
                c.removeData('flipLock');
                $clone.dequeue()
            })
        })
    }
})(jQuery);

;

function initializeTooltips() {
    $('#start').tooltip({
        offset: [86, 0],
        tipClass: 'tooltip_start',
        layout: '<div><span class="tooltip-up-arrow" style="left:70px;"></span></div>'
    });
    $('#social-icons-home a.icon.search, #social-icons-home a.icon.network').tooltip({
        offset: [72, 0],
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-up-arrow"></span></div>'
    });
    $('#social-icons-home a.icon.sarakakis').tooltip({
        offset: [86, -32],
        tipClass: 'tooltip_sarakakis',
        layout: '<div><span class="tooltip-up-arrow-sarakakis"></span></div>'
    });
    $('#social-icons-home a.icon.search, #social-icons-home a.icon.network').mouseover(function () {
        $('#top-right-menu').animate({
            top: "-148px"
        }, 400, function () {
            $(this).hide();
        });
    });
    $('#social-icons-home a.icon.top-right-menu').mouseover(function () {
        $('#top-right-menu').fadeIn(160, function () {
            $(this).animate({
                top: "48px"
            }, 500);
        });
    });
    $('#social-icons-home a.icon.top-right-menu').click(function () {
        if ($('#top-right-menu').is(':visible')) {
            $('#top-right-menu').animate({
                top: "-148px"
            }, 400, function () {
                $(this).hide();
            });
        } else {
            $('#top-right-menu').fadeIn(160, function () {
                $(this).animate({
                    top: "48px"
                }, 500);
            });
        }
    });
}

function LoadModelPage() {}

function applyBlurOverlay() {
    $('#honda-overlay').delay(500).fadeIn(300);
}

function resetBlurOverlay() {
    $('#honda-overlay').delay(400).fadeOut(490);
}
$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        if ($('#menu-boxes-container').hasClass("opened")) {
            closeBoxes();
        }
    }
});

function contactFormEvents() {
    $('a.send_to_radio_icon').click(function () {
        if ($(this).hasClass("checked")) {} else {
            $('a.send_to_radio_icon.checked').prev().removeAttr("checked");
            $('a.send_to_radio_icon.checked').removeClass("checked");
            $(this).addClass("checked");
            $(this).prev().attr("checked", "checked");
        }
    });
    $('a.newsletter_icon').click(function () {
        if ($(this).hasClass("checked")) {
            $(this).prev().removeAttr("checked");
            $(this).removeClass("checked")
        } else {
            $(this).addClass("checked");
            $(this).prev().attr("checked", "checked");
        }
    });
    $('#close_contact a').click(function () {
        closeContactForm();
    });
}

function closeBoxes() {
    $(".flip-box").removeClass("flipped").removeClass("reflipped");
    $('#menu-boxes').delay(10).fadeIn(100, function () {
        resetBoxes();
    });
    $('#menu-boxes-container').fadeOut(300, function () {
        resetBlurOverlay();
        startGallery();
    }).removeClass("opened");
    $('a#close_boxes').fadeOut(250);
    $('a#boxes_back').fadeOut(250);
    $('#start').removeClass("opened");
}

function closeBoxesNoAnimation() {
    $(".flip-box").removeClass("flipped").removeClass("reflipped");
    $('#menu-boxes').delay(10).fadeIn(100, function () {
        $(".flip-box").each(function () {
            var obj = $(this);
            obj.removeClass("reflipped", 10);
            obj.removeClass("flipped", 10);
            obj.removeClass("inactive", 10);
            $('#menu-boxes').animate({
                top: "-2px"
            }, 500);
        });
    });
    $('#menu-boxes-container').fadeOut(370, function () {}).removeClass("opened");
    $('a#close_boxes').fadeOut(295);
    $('a#boxes_back').fadeOut(295);
    $('#start').removeClass("opened");
}

function checkBoxVisible(index) {
    if (index == 1 || index == 2 || index == 3) {
        $('#menu-boxes').animate({
            top: "-2px"
        }, 500);
    } else if (index == 4 || index == 5 || index == 6) {
        if (HEIGHT <= 664) {
            $('#menu-boxes').animate({
                top: -(60 + (664 - HEIGHT))
            }, 500);
        }
    } else if (index == 7 || index == 8 || index == 9) {
        if (HEIGHT <= 933) {
            $('#menu-boxes').animate({
                top: -(30 + (933 - HEIGHT))
            }, 500);
        }
    }
}

function BoxesBack() {
    $('a#back_category_boxes,a#close_category_boxes').hide();
    $(".flip-box").removeClass("flipped").removeClass("reflipped");
    $('#menu-boxes').delay(10).fadeIn(100, function () {
        resetBoxes();
        $('#close_category_boxes').delay(660).fadeIn(650);
        $('#back_category_boxes').delay(660).fadeIn(650);
    });
    $('#menu-boxes-container').fadeOut(300, function () {
        $('#main-categories').delay(390).addClass("opened").fadeIn(420);
        $('a#close_boxes').fadeOut(350);
        $('a#boxes_back').fadeOut(350);
    }).removeClass("opened");
}

function closeGoogleMap() {
    if ($('#dealers_google_map_container').is(":visible")) {
        $('#dealers_google_map_container').fadeOut(800, function () {
            $(this).css("visibility", "hidden");
            $(this).css("background-color", "transparent");
            $(this).css("display", "none");
            $('#social-icons-home a.network').removeClass("opened");
        });
        $('#secondary-nav').fadeIn(700);
    }
}

function closeHistory() {
    if ($('#history').is(":visible")) {
        $('#history').fadeOut(800);
    }
}

function closeDealerSearchBox() {
    $('#map_instrunctions').animate({
        left: "-75%"
    }, 880, function () {
        $('#map_instrunctions').hide();
    });
}
var selected_menu = '';

function closeContactForm() {
    var contact_easing_close = 'easeInOutCubic';
    $('#contact_container').delay(100).animate({
        left: "120%"
    }, 950, contact_easing_close, function () {
        $('#contact').fadeOut(300, contact_easing_close);
    });
}

function closeCategoryBoxes() {
    $('#main-categories').removeClass("opened").fadeOut(550);
    $('#honda-overlay').delay(400).fadeOut(350);
    $('#close_category_boxes').fadeOut(220);
    $('#start').removeClass("opened");
}

function openSearchModelBar() {
    var searchbox_easing = 'easeInOutCubic';
    if ($.trim($('#input_searchbar').val()) == "") {
        $('#input_searchbar').val("Search");
    }
    $('#social-icons-home a.icon.search').addClass("opened");
    $('#searchbar form input').animate({
        left: "0px"
    }, 770, searchbox_easing);
}

function closeSearchModelBar() {
    var searchbox_easing = 'easeInOutCubic';
    if ($('#social-icons-home a.icon.search').hasClass("opened")) {
        $('#social-icons-home a.icon.search').removeClass("opened");
        $('#searchbar form input').animate({
            left: "400px"
        }, 450, searchbox_easing);
    }
}

function mainMenu() {
    $('li#contactbox').live("click", function () {
        ContactboxFlipFast();
    });
    $('a#contact_form_trigger').live("click", function () {
        $('#top-right-menu ul li a.contact').trigger("click");
        return true;
    })
    $('#close_history').click(function () {
        closeHistory();
        if ($('#main-categories').hasClass("opened")) {
            closeCategoryBoxes();
        }
        $('#honda-overlay, #top_menu_with_boxes').fadeOut(400);
        $('#countdown-footer').fadeIn(400);
        $('#footer-modelsboxes').removeClass("active");
    });
    var contact_easing = 'easeInOutCubic';
    $('#top-right-menu ul li a.contact').click(function () {
        closeGoogleMap();
        closeSearchModelBar();
        closeHistory();
        if ($('#menu-boxes-container').hasClass("opened")) {
            closeBoxes();
        }
        if ($('#main-categories').hasClass("opened")) {
            closeCategoryBoxes();
        }
        $('#contact').fadeIn(300, contact_easing, function () {
            $('#contact_container').delay(100).animate({
                left: "0%"
            }, 1200, contact_easing)
        });
        return false;
    });
    var history_easing = 'easeInOutCubic';
    $('a.history_entry_point').live("click", function () {
        closeGoogleMap();
        closeSearchModelBar();
        if ($('#menu-boxes-container').hasClass("opened")) {
            closeBoxesNoAnimation();
        }
        if ($('#main-categories').hasClass("opened")) {
            closeCategoryBoxes();
        }
        if (!$('#history').hasClass("loaded")) {
            $('#history .history_middler').html('<iframe class="page" src="history.php" width="100%" height="911"></iframe>');
        }
        screenSetup();
        $('#history').fadeIn(1000, history_easing, function () {}).addClass("loaded");
        return false;
    });
    $('#dealersearch').val("");
    $('#input_searchbar').focus(function () {
        if ($(this).val() == "Search") {
            $(this).val("");
        }
    });
    $('#close_dealersearch').click(function () {
        closeDealerSearchBox();
    });
    $('#input_searchbar').blur(function () {
        if ($.trim($(this).val()) == "") {
            $(this).val("Search");
        }
    });
    $('#social-icons-home a.icon.search').click(function () {
        var searchbox_easing = 'easeInOutCubic';
        if ($(this).hasClass("opened")) {
            closeSearchModelBar();
        } else {
            openSearchModelBar();
        }
    });
    var network_loaded_first_time = true;
    $('#social-icons-home a.icon.network').click(function () {
        if ($(this).hasClass("opened")) {
            closeDealerSearchBox();
            closeGoogleMap();
            closeSearchModelBar();
            closeHistory();
            startGallery();
        } else {
            closeContactForm();
            closeSearchModelBar();
            if ($('#menu-boxes-container').is(":visible")) {
                closeBoxes();
            }
            if ($('#main-categories').is(":visible")) {
                $('#close_category_boxes').trigger("click");
            }
            $(this).addClass("opened")
            if ($('#dealers_google_map_container').children().length > 0) {
                $('#dealers_google_map_container').css("display", "none");
                $('#dealers_google_map_container').css("visibility", "visible");
                $('#dealers_google_map_container').fadeIn(400);
            } else {
                MAP_loadScript();
                $('#dealers_google_map_container').css("visibility", "visible");
            }
            stopGallery();
            if (!network_loaded_first_time) {
                $('#map_instrunctions').fadeIn(300, function () {
                    $(this).animate({
                        left: "0%"
                    }, 1000);
                });
            }
            network_loaded_first_time = false;
            $('#secondary-nav').fadeOut(700);
        }
    });
    $('#close_category_boxes').click(function () {
        closeCategoryBoxes()
        startGallery();
    });
    $('#start').click(function () {
        $(this).addClass("opened");
        closeDealerSearchBox();
        closeGoogleMap();
        closeContactForm();
        closeSearchModelBar();
        closeHistory();
        $('#close_category_boxes').fadeIn(250);
        $('#back_category_boxes').fadeIn(250);
        if (!$('#main-categories').hasClass("opened") && !$('#menu-boxes-container').is(":visible")) {
            $('#honda-overlay').delay(100).fadeIn(450);
            $('#main-categories').addClass("opened").fadeIn(600);
            stopGallery();
        }
    });
    if ($.browser.msie && parseInt($.browser.version) <= 9) {} else {}
    if ($.browser.msie && parseInt($.browser.version) <= 9) {} else {}
    $('#main-categories ul li a#cat1').click(function () {
        $('#main-menu ul li a#lineup_trigger').trigger("click");
        $('#main-categories').removeClass("opened").fadeOut(10);
    });
    $('#main-categories ul li a#cat2').click(function () {
        $('#main-menu ul li a#offers_trigger').trigger("click");
        $('#main-categories').removeClass("opened").fadeOut(10);
    });
    $('#main-categories ul li a#cat6').click(function () {
        $('#main-menu ul li a#archive_trigger').trigger("click");
        $('#main-categories').removeClass("opened").fadeOut(10);
    });
    $('#main-categories ul li a#cat3').click(function () {
        $('#main-menu ul li a#pod_trigger').trigger("click");
        $('#main-categories').removeClass("opened").fadeOut(10);
    });
    $('a#boxes_back').click(function () {
        BoxesBack();
    });
    $('a#close_boxes').click(function () {
        closeBoxes();
    });
    $('.box-model-list ul li .box-models-txt a').live("mouseover", function () {
        $(this).parent().parent().parent().find("a.selected").removeClass("selected");
        $(this).parent().parent().parent().parent().parent().parent().find('.box-model-image .img_.current').removeClass("current");
        var target = $(this).attr("class");
        $(this).parent().parent().parent().parent().parent().parent().find('.box-model-image .' + target).addClass("current");
        var rel = parseInt($(this).parent().parent().parent().parent().parent().parent().find('.box-model-image-container .' + target).attr("rel"));
        $(this).parent().parent().parent().parent().parent().parent().find('.box-model-image-container').stop(true, true).animate({
            left: -rel * 317
        }, 600);
        $(this).addClass("selected");
    });
    $('#main-menu ul li a#lineup_trigger, #main-menu ul li a#offers_trigger, #main-menu ul li a#archive_trigger').click(function () {
        if ($('#menu-boxes-container').hasClass("opened")) {
            closeBoxes();
        } else {
            $('#close_boxes, #boxes_back').hide();
            $('#menu-boxes-container.opened').fadeOut(300);
            $('#menu-boxes-container').fadeIn(300, function () {
                $('#close_boxes').delay(670).fadeIn(670);
                $('#boxes_back').delay(670).fadeIn(670);
            }).addClass("opened");
            flipBoxes($(this));
            applyBlurOverlay();
        }
    });
    $('#main-menu ul li a#pod_trigger').click(function () {
        if ($('#menu-boxes-container').hasClass("opened")) {
            closeBoxes();
        } else {
            $('#close_boxes, #boxes_back').hide();
            $('#menu-boxes-container.opened').fadeOut(300);
            $('#menu-boxes-container').fadeIn(300, function () {
                $('#close_boxes').delay(670).fadeIn(670);
                $('#boxes_back').delay(670).fadeIn(670);
            }).addClass("opened");
            flipBoxes($(this));
            applyBlurOverlay();
        }
    });
    $('#primary-nav a.clickable').click(function () {
        if ($('#menu-boxes-container').hasClass("opened")) {
            closeBoxes();
        } else {
            stopGallery();
            $('#menu-boxes-container.opened').fadeOut(300);
            $('#menu-boxes-container').fadeIn(300).addClass("opened");
            flipBoxes($(this));
            applyBlurOverlay();
            $('#close_boxes').delay(1000).fadeIn(250);
        }
    });
    $('#menu-boxes .flip-box.flipped').live("click", function () {
        checkBoxVisible($(this).attr("rel"));
        if (!$(this).hasClass("reflipped")) {
            var obj = $(this);
            if ($(this).find('a.category .hide_on_flip').hasClass("no_reflip")) {} else {
                if ($(this).find('a.category .hide_on_flip').children().length > 0) {
                    $(this).find('a.category .hide_on_flip').stop(true, true).fadeOut(25, function () {
                        boxFlipFast2(obj);
                    });
                } else {
                    boxFlipFast2(obj);
                }
            }
        }
    });
    $('#menu-boxes .flip-box.reflipped .box-models-txt a').live("click", function () {
        $(this).parent().parent().parent().parent().parent().parent().removeClass("act");
        return true;
    });
    $('#menu-boxes .flip-box.reflipped.act').live("click", function () {
        if ($(this).hasClass("act")) {
            checkBoxVisible($(this).attr("rel"));
            $(this).removeClass("reflipped");
            var obj = $(this);
            if ($(this).find('.hide_on_flip').children().length > 0) {
                $(this).find('.hide_on_flip').stop(true, true).hide(25, function () {
                    boxFlipFast(obj, selected_menu);
                });
            } else {
                boxFlipFast(obj, selected_menu);
            }
        }
    });
    var param = document.URL.split('#')[1];
    if (param == "dealers") {
        $('.network ').trigger("click");
    }
}

function boxFlipFast2(obj) {
    obj.flip({
        color: "#CFCCC7",
        direction: obj.attr("rev"),
        content: selected_menu.parent().find('ul.main-submenu li.' + obj.attr("rel") + ' .second-level').html(),
        speed: 170,
        onBefore: function () {
            obj.removeClass("flipped", 10);
        },
        onAnimation: function () {
            obj.addClass("reflipped", 10);
        },
        onEnd: function () {}
    });
}

function resetBoxes() {
    $(".flip-box").each(function () {
        var obj = $(this);
        resetboxFlip(obj);
        $('#menu-boxes').animate({
            top: "-2px"
        }, 500);
    });
}

function resetboxFlip(obj) {
    obj.flip({
        color: "#CFCCC7",
        direction: obj.attr("rev"),
        content: "",
        speed: 460,
        onBefore: function () {
            obj.find('.category .category_title').hide();
        },
        onAnimation: function () {
            obj.removeClass("reflipped", 10);
            obj.removeClass("flipped", 10);
            obj.removeClass("inactive", 10);
        },
        onEnd: function () {}
    });
}

function flipBoxes(target) {
    selected_menu = target;
    $(".flip-box").not(".flipped").each(function () {
        var obj = $(this);
        boxFlip(obj, target);
    });
}

function boxFlip(obj, target) {
    obj.flip({
        color: "#232323",
        direction: obj.attr("rev"),
        speed: 520,
        content: target.parent().find('ul.main-submenu li.' + obj.attr("rel") + ' .first-level').html(),
        onBefore: function () {
            obj.removeClass("reflipped", 10);
            obj.removeClass("inactive", 10);
        },
        onAnimation: function () {
            if (target.parent().find('ul.main-submenu li.' + obj.attr("rel") + ' .first-level').html()) {
                obj.addClass("flipped", 10);
            } else {
                obj.addClass("inactive", 10);
            }
        },
        onEnd: function () {
            obj.find('.category .category_title').show();
        }
    });
}

function boxFlipFast(obj, target) {
    obj.flip({
        color: "#232323",
        direction: obj.attr("rev"),
        speed: 200,
        content: target.parent().find('ul.main-submenu li.' + obj.attr("rel") + ' .first-level').html(),
        onBefore: function () {
            obj.removeClass("reflipped", 10);
        },
        onAnimation: function () {
            obj.addClass("flipped", 10);
        },
        onEnd: function () {
            obj.find('.category .category_title').show();
        }
    });
}

function ContactboxFlipFast() {
    if ($('#contactbox').hasClass("flipped")) {
        $('#contactbox').flip({
            color: "#232323",
            direction: "rl",
            speed: 200,
            content: $('#category_box_hidden_data .first-level').html(),
            onBefore: function () {
                $('#contactbox .contact_bg2 a').hide();
                $('#contactbox').removeClass("reflipped", 10);
            },
            onAnimation: function () {
                $('#contactbox').removeClass("flipped", 10);
            },
            onEnd: function () {
                $('#contactbox').find('.category .category_title').show();
            }
        });
    } else {
        $('#contactbox').flip({
            color: "#CFCCC7",
            direction: "lr",
            speed: 200,
            content: $('#category_box_hidden_data .second-level').html(),
            onBefore: function () {
                $('#contactbox .content .title').hide();
                $('#contactbox').removeClass("reflipped", 10);
            },
            onAnimation: function () {
                $('#contactbox').addClass("flipped", 10);
            },
            onEnd: function () {
                $('#contactbox').find('.category .category_title').show();
            }
        });
    }
}
var GALLERY_SPEED = 8420;
var START_GALLERY = 0;
var SWAP = true;

function galleryChange() {
    $('#home-page a.next').trigger("click");
}

function startGallery() {
    START_GALLERY = setInterval("galleryChange()", GALLERY_SPEED);
}

function stopGallery() {
    clearInterval(START_GALLERY);
}

function homeGallery() {
    $('#test').click(function () {
        galleryChange();
    });
    var gallery_easing = 'easeInOutCubic';
    $('#home-page a.next').click(function () {
        if ($('#home-page .frame.current').next().length > 0) {
            $('#home-page .frame.current').next().stop(true, true).fadeIn(1300, gallery_easing, function () {
                $('#home-page .frame.current').removeClass("current").hide();
                $(this).addClass("current");
            });
        } else {
            $('#home-page .frame.fullscreen-active').stop(true, true).fadeIn(20, function () {
                $('#home-page .frame.current').removeClass("current").fadeOut(1400, gallery_easing);
                $(this).addClass("current");
            });
        }
    });
}

function topMenu() {
    var menu_easing = 'easeOutExpo';
    $('#primary-nav ul li a.clickable').live("mouseover", function () {
        var img_height = $(this).height() - 60;
        $(this).stop(true, false).animate({
            bottom: '0%'
        }, 840, menu_easing);
    });
    $('#primary-nav ul li a.clickable').live("mouseout", function () {
        var img_height = ((WIDTH / 4) * 224) / 640 + 2;
        $(this).stop(true, false).animate({
            bottom: img_height
        }, 530, menu_easing);
    });
}

function bottomMenu() {
    $('#footer-menu-trigger').toggle(function () {
        $('#secondary-nav').animate({
            bottom: "0px"
        }, 800, function () {
            $('#secondary-nav ul li a').addClass("clickable");
        });
    }, function () {
        $('#secondary-nav ul li a').removeClass("clickable");
        $('#secondary-nav').animate({
            bottom: "-48px"
        }, 800);
    });
    var menu_easing = 'easeOutExpo';
    $('#secondary-nav ul li a.clickable').live("mouseover", function () {
        var img_height = $(this).height() - 60;
        $(this).stop(true, false).animate({
            top: -img_height
        }, 840, menu_easing);
    });
    $('#secondary-nav ul li a.clickable').live("mouseout", function () {
        $(this).stop(true, false).animate({
            top: 0
        }, 530, menu_easing);
    });
    $('#secondary-nav ul li a, #menu-boxes .box-model-list ul a').live("click", function () {
        $('#honda_preloader').stop(true, true).fadeIn(800);
    });
}
var WIDTH = 1024;
var HEIGHT = 768;
var RATIO = 4 / 3;
var first_time_screen = true;

function screenSetup() {
    WIDTH = $(window).width();
    HEIGHT = $(window).height();
    if (first_time_screen) {
        $('.page').width(WIDTH + 25);
    } else {
        $('.page').width(WIDTH);
    }
    $('.page').height(HEIGHT);
    var ratio_ = WIDTH / HEIGHT;
    if (RATIO <= ratio_) {
        $('.page .frame img.slider_img').removeClass("height_respect");
        var percent_top = (RATIO - ratio_) * 40;
        $('.page .frame img.slider_img').addClass("width_respect").css({
            "left": "0%",
            "bottom": percent_top + "%"
        });
    } else {
        $('.page .frame img.slider_img').removeClass("width_respect");
        var percent_left = (RATIO - ratio_) * 55;
        var percent_left = 0;
        $('.page .frame img.slider_img').addClass("height_respect").css({
            "left": -percent_left + "%",
            "bottom": "0%"
        });
    }
    var img_height = ((WIDTH / 4) * 224) / 640 + 2;
    $('#primary-nav ul li a.clickable').stop(true, true).animate({
        bottom: img_height
    }, 2);
    if (WIDTH < 983) {
        $('html').css("overflow-x", "scroll");
        $('html').css("overflow-y", "hidden");
    } else {
        $('html').css("overflow", "hidden");
    }
    resizeQuotes();
}

function resizeQuotes() {
    $('#detail3').height(HEIGHT);
    $('#detail8').height(HEIGHT);
    $('#detail10').height(HEIGHT);
}

function screenResize() {
    $(window).resize(function () {
        screenSetup();
    });
}

function enableJScrollPane() {
    $('.generation_container .make_scroll').height($(window).height());
    $('body').height($(window).height());
    $('#left_area').height($(window).height());
    $('#right_area').height($(window).height());
    $(window).resize(function () {
        $('.generation_container .make_scroll').height($(window).height());
        $('body').height($(window).height());
        $('#left_area').height($(window).height());
        $('#right_area').height($(window).height());
    });
    $('.generation_container .make_scroll').jScrollPane({
        scrollbarWidth: 8,
        scrollbarMargin: 0,
        showArrows: false,
        wheelSpeed: 18,
        animateInterval: 300
    });
}

function lazyLoadInit() {
    if ($.browser.mozilla && parseInt($.browser.version) < 4) {
        $("img.lazy2").show().lazyload();
    } else {
        $(function () {
            $("img.lazy2").lazyload({
                event: "sporty",
                effect: "fadeIn",
                failure_limit: 10,
                skip_invisible: false,
                threshold: 400
            });
        });
        $(window).bind("load", function () {
            var timeout = setTimeout(function () {
                $("img.lazy2").trigger("sporty")
            }, 600);
        });
    }
}

function menuHover() {}

function socialMediaButtonsInit() {
    $('.social-icons .facebook').live("click", function () {
        var url = $(this).attr("url");
        var title = $(this).attr("title");
        facebookShare(url, title);
    });
    $('.social-icons .twitter').live("click", function () {
        var url = $(this).attr("url");
        var title = $(this).attr("title");
        twitterShare(url, title);
    });
}

function facebookShare(url, title) {
    u = url;
    t = title;
    window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
}

function twitterShare(url, title) {
    t = title;
    u = url;
    window.open('http://twitter.com/home?status=' + encodeURIComponent(t) + ' : ' + encodeURIComponent(u), 'sharer', 'toolbar=0,status=0,width=626,height=436');
    return false;
}

function landing() {
    $('a#landing-go').click(function () {
        $('#landing-inside').fadeIn(200, function () {
            $(this).css({
                'visibility': 'visible'
            });
            $('#landing').fadeOut(1040);
        });
    });
}

function applyTextShadow() {
    if ($.browser.msie) {
        $('.main_description .cont h3').textShadow({
            x: 1,
            y: 1,
            radius: 0,
            color: "#ffffff"
        });
    }
}
var prev_rel = 'home';
var prev_rel2 = 0;
var ajax_in_progress = false;

function doAjax(rel, rel2) {
    rel2 = parseInt(rel2);
    var desc_easing = 'easeInQuad';
    if (rel2 != 0) {
        $('#text-selector').fadeOut(100);
    } else {
        $('#text-selector').fadeIn(100);
    }
    $('ul#menu a.selected').removeClass("selected");
    $(this).find('a').addClass("selected");
    $.post('ajax.desc.php', {
        rel: rel,
        prev_rel: prev_rel,
        rel2: rel2
    }, function (data) {
        $('#main-container-ajax').slideUp(550, desc_easing, function () {
            $(this).html(data).slideDown(700, desc_easing, function () {
                setupHoverscroll();
                applyTextShadow();
                carInfoHoverscroll();
                prev_rel = rel;
                prev_rel2 = rel2;
                ajax_in_progress = false;
                var new_width = (rel2 + 1) * 95 + 2;
                $('#red-line-menu-top,#red-line-menu-bottom').stop(true, true).animate({
                    width: new_width
                }, 300);
            });
        });
    });
    galleryAjax(rel, rel2);
}

function menuAjax() {
    $('ul#menu li').click(function () {
        ajax_in_progress = true;
        var rel = ($(this).find('a').attr("rel"));
        var rel2 = parseInt($(this).find('a').attr("rel2"));
        doAjax(rel, rel2);
    });
}

function galleryAjax(rel, rel_number) {
    var gallery_easing = 'easeInQuad';
    var rel2 = rel_number;
    $.post('ajax.gallery.php', {
        rel: rel,
        rel2: rel2
    }, function (data) {
        $('#coin-slider-wrapper').fadeOut(500, gallery_easing, function () {
            $(this).html(data).fadeOut(10, gallery_easing, function () {
                mainGallery();
                $(this).fadeIn(760);
            });
        });
    });
}

function mainGallery() {
    $('.coin-slider-cont').coinslider({
        width: 509,
        height: 267,
        delay: 3640,
        sDelay: 40,
        opacity: 0.7,
        links: false,
        titleSpeed: 500,
        effect: 'rain',
        hoverPause: true,
        stopAtLastSlide: true,
        stopAtFirstSlide: true
    });
    $('.coin-slider-wrapper .prev_arrow_css').click(function () {
        $(this).parent().parent().find('.coin-slider a.cs-prev').trigger("click");
    });
    $('.coin-slider-wrapper .next_arrow_css').click(function () {
        $('#cs-next-coin-slider').trigger("click");
        $(this).parent().parent().find('.coin-slider a.cs-next').trigger("click");
    });
}
var slider_cont = '';
var slider_init = '';

function activateGallery(slider_container) {
    slider_cont = slider_container;
    var delay = 4140;
    slider_init = setInterval("nextSlideCrossSlider()", delay);
    if (slider_cont && slider_cont != "") {
        $('ul#menu li.selected').removeClass("selected");
        $('ul#menu li a[rel=' + slider_cont.substr(1) + ']').parent().addClass("selected");
    }
}

function nextSlideCrossSlider() {
    $(slider_cont).find('.coin-slider-wrapper .next_arrow_css').trigger("click");
}

function deactivateGallery() {
    clearInterval(slider_init);
}

function textSelector() {
    var easing_animation_text = 'easeOutQuint';
    $('a#text-selector').hover(function () {
        if (parseInt($('#cont_2').children().length) > 0) {
            $('#main-container .cont.selected').fadeTo(400, 0.5);
            $('#main-container .cont.not_selected').fadeTo(400, 1);
        }
    }, function () {
        if (parseInt($('#cont_2').children().length) > 0) {
            $('#main-container .cont.not_selected').fadeTo(400, 0.5);
            $('#main-container .cont.selected').fadeTo(400, 1);
        }
    });
    $('a#text-selector').click(function () {
        if (parseInt($('#cont_2').children().length) > 0) {
            $('#main-container .cont.selected').removeClass("selected").addClass("not_selected1").animate({
                left: "534px"
            }, 1000, easing_animation_text);
            $('#main-container .cont.not_selected').removeClass("not_selected").animate({
                left: "10px"
            }, 1000, easing_animation_text, function () {
                $(this).addClass("selected").fadeTo(400, 1);
                $('#main-container .cont.not_selected1').removeClass("not_selected1").addClass("not_selected").fadeTo(400, 0.5);
            });
        }
    });
}

function mainMenuHoverscroll() {
    $('#menu-container').hoverscroll({
        vertical: false,
        fixedArrows: false,
        rtl: false,
        arrows: false,
        height: 48,
        width: 723
    });
    $('#next_menu').hover(function () {
        var direction = 1,
            speed = 8;
        $('#menu-container')[0].startMoving(direction, speed);
    }, function () {
        var direction = 0,
            speed = 1;
        $('#menu-container')[0].startMoving(direction, speed);
    });
    $('#prev_menu').hover(function () {
        var direction = -1,
            speed = 8;
        $('#menu-container')[0].startMoving(direction, speed);
    }, function () {
        var direction = 0,
            speed = 1;
        $('#menu-container')[0].startMoving(direction, speed);
    });
}

function carInfoHoverscroll() {
    $('.car-desc-hovereffect').hoverscroll({
        vertical: true,
        fixedArrows: false,
        rtl: false,
        arrows: false,
        height: 207,
        width: 528
    });
}

function setupHoverscroll() {
    $('.hoverscroll3').hoverscroll({
        vertical: true,
        fixedArrows: false,
        rtl: false,
        arrows: false,
        height: 310,
        width: 400
    });
    $('#model_bottom_arrows .nav-arrows a.up').hover(function () {
        var direction = 1,
            speed = 4;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    }, function () {
        var direction = 0,
            speed = 1;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    });
    $('#model_bottom_arrows .nav-arrows a.down').hover(function () {
        var direction = -1,
            speed = 4;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    }, function () {
        var direction = 0,
            speed = 1;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    });
}

function loadLoaderImagesFirst() {
    if ($.browser.msie && parseInt($.browser.version) <= 8) {
        var loader = new Loader("body", {
            userCallback: loaderComplete,
            showProgress: true,
            showProgressText: true,
            overlayColor: "#000000",
            textSize: 15
        });
        loader.Start();
    } else {
        var _url1 = "images/loader/bg.jpg";
        var _url2 = "images/loader/loader_logo.png";
        _im = $("<img id=\"img_loader_1\">");
        _im.hide();
        _im.bind("load", function () {
            $(this).fadeIn();
        });
        _im2 = $("<img id=\"img_loader_2\">");
        _im2.hide();
        _im2.bind("load", function () {
            $(this).fadeIn();
        });
        $('body div#honda_loader_container').append(_im);
        $('body div#honda_loader_container').append(_im2);
        _im.attr('src', _url1).load(function () {
            $('#img_loader_1').remove();
        });
        _im2.attr('src', _url2).load(function () {
            $('#img_loader_2').remove();
            var loader = new Loader("body", {
                userCallback: loaderComplete,
                showProgress: true,
                showProgressText: true,
                overlayColor: "#000000",
                textSize: 15
            });
            loader.Start();
        });
    }
}
var display_message;

function sendForm($prename_id) {
    $prename_id = "#contact_form"
    var passed = true;
    display_message = "";
    var first_name = $($prename_id + ' input[name="first_name"]').val();
    var last_name = $($prename_id + ' input[name="last_name"]').val();
    var email = $($prename_id + ' input[name="email"]').val();
    var phone = $($prename_id + ' input[name="phone"]').val();
    var current_moto = $($prename_id + ' input[name="current_moto"]').val();
    var message = $($prename_id + ' #comments').val();
    if (!CheckInputNotNull(first_name, "first_name", "όνομα")) {
        passed = false;
    }
    if (!CheckInputNotNull(last_name, "last_name", "επώνυμο")) {
        passed = false;
    }
    if (!CheckInputNotNull(phone, "phone", "τηλέφωνο")) {
        passed = false;
    } else {
        CheckInputIsNumber(phone, "phone", "τηλέφωνο");
    }
    if (!isNumber(phone))
        passed = false;
    if (!CheckValidMail2(email))
        passed = false;
    var send_to = $('#contact_form .send_to_radio:checked').val();
    var newsletter = false;
    if ($('#contact_form .newsletter_icon').hasClass("checked")) {
        newsletter = true;
    }
    var captcha = $('#captcha-form').val();
    if (captcha != "") {
        $.post('ajax.checkCaptcha.php', {
            captcha: captcha
        }, function (data) {
            if (data == "OK") {
                if (passed) {
                    var form = "contact_form";
                    display_message = "<div class=\"advertise_message\">Αποστολή...</div>";
                    $('#contact_form .message').html(display_message).fadeIn(700);
                    $.post('ajax.send-form.php', {
                        form: form,
                        first_name: first_name,
                        last_name: last_name,
                        current_moto: current_moto,
                        email: email,
                        send_to: send_to,
                        newsletter: newsletter,
                        phone: phone,
                        message: message
                    }, function (data) {
                        if (data == "OK") {
                            $('#contact_form .message').html("Η αποστολή ολοκληρώθηκε...").fadeIn(700);
                            ClearContactForm();
                        }
                        $('#contact_form .message').delay(990000).fadeOut(850);
                    });
                } else {
                    $('#contact_form .message').html(display_message).fadeIn(700);
                }
            } else {
                passed = false;
                display_message += "<div>Εισάγετε σωστά τη λέξη που βλέπετε στην εικόνα.</div>";
                $('#contact_form .message').html(display_message).fadeIn(700);
            }
        });
    } else {
        passed = false;
        display_message += "<div>Εισάγετε τη λέξη που βλέπετε στην εικόνα.</div>";
        $('#contact_form .message').html(display_message).fadeIn(700);
    }
}

function ClearContactForm() {
    $('#contact_form input[name="first_name"]').val("");
    $('#contact_form input[name="last_name"]').val("");
    $('#contact_form input[name="email"]').val("");
    $('#contact_form input[name="current_moto"]').val("");
    $('#contact_form input[name="phone"]').val("");
    $('#contact_form textarea[name="comments"]').val("");
}

function CheckValidMail2(email) {
    if (email != "") {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
        if (emailPattern.test(email)) {
            return true;
        } else {
            display_message += "<div>Το email δεν είναι εγκυρο.</div>";
            return false;
        }
    } else {
        display_message += "<div>Το email δεν είναι εγκυρο.</div>";
        return false;
    }
}

function CheckInputNotNull(element, element_name, display_name) {
    if (element != "" && element != "undefined") {
        return true;
    } else {
        display_message += "<div>To " + display_name + " είναι υποχρεωτικό πεδίο.</div>";
        return false;
    }
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function CheckInputIsNumber(element, element_name, display_name) {
    if (isNumber(element)) {
        return true;
    } else {
        display_message += "<div>To " + display_name + " δεν είναι έγκυρο.</div>";
        return false;
    }
};
var HondaModelPageInit = (function () {
    init_();
    setupHoverscroll();
    initStandingsTablesOnCircuits();
    initPowerOfDreams();
    socialMediaButtonsInit();
    galleryInit2();
    modelScreenFix();
    if ($.browser.msie && parseInt($.browser.version) <= 8) {
        GalleryTooltips_OLD_IE();
    } else {
        GalleryTooltips();
    }
    modelColorPicker();
    var settings = {},
        defaults = {
            startAt: 0,
            sectionCheckInterval: 1000,
            clampWidth: 1600,
            tracking: false
        },
        scrollAnimate, currentSection = -1,
        checkSectionLock = 0,
        updateCount = 0,
        loadProgress;
    var wHeight, wWidth, wCenter, outroComp, ratio;
    var $scrollBar, $scrollThumb, isScrolling, scrollBarHeight, scrollThumbHeight, thumbDelta, scrollThumbPosition, scrollPercent;
    var tooltipJustOpened = true;
    var $overlay;
    var player;
    var lastActivity = (new Date()).getTime();
    var pulseReminders = 3;
    var pulseScrollTeaserRunning = false;
    var animationFunctions = {
        absPosition: function (opts) {
            var defaults = {
                    startLeft: 0,
                    startTop: 0,
                    endLeft: 0,
                    endTop: 0
                },
                settings = $.extend(defaults, opts);
            this.startProperties['left'] = settings.startLeft;
            this.startProperties['top'] = settings.startTop;
            this.endProperties['left'] = settings.endLeft;
            this.endProperties['top'] = settings.endTop;
            this.startProperties['display'] = 'block';
            this.endProperties['display'] = 'none';
        },
        bottomLeftOutside: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            var portrait = false,
                elemHalfWidth = anim._elem.width() / 2,
                elemHalfHeight = anim._elem.height() / 2,
                adj = portrait ? wWidth / 2 + elemHalfWidth : adj = wHeight / 2 + elemHalfHeight,
                tan = Math.sqrt(Math.pow(adj, 2) + Math.pow(adj, 2));
            this.properties['top'] = wCenter.top + adj - elemHalfHeight + (portrait ? settings.offset : 0);
            this.properties['left'] = wCenter.left - adj - elemHalfWidth + (portrait ? 0 : settings.offset);
        },
        topRightOutside: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            var portrait = false,
                elemHalfWidth = anim._elem.width() / 2,
                elemHalfHeight = anim._elem.height() / 2,
                adj = portrait ? wWidth / 2 + elemHalfWidth : adj = wHeight / 2 + elemHalfHeight,
                tan = Math.sqrt(Math.pow(adj, 2) + Math.pow(adj, 2));
            this.properties['top'] = wCenter.top - adj - elemHalfHeight + (portrait ? settings.offset : 0);
            this.properties['left'] = wCenter.left + adj - elemHalfWidth + (portrait ? 0 : settings.offset);
        },
        leftOutside: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            this.properties['left'] = -anim._elem.width() + settings.offset;
        },
        leftOutsideClampWidth: function (anim, opts) {
            this.properties['left'] = -anim._elem.width() - (settings.clampWidth - wWidth) / 2;
        },
        rightOutside: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            this.properties['left'] = wWidth + settings.offset;
        },
        rightOutsideClampWidth: function (anim, opts) {
            this.properties['left'] = wWidth + (settings.clampWidth - wWidth) / 2;
        },
        centerV: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            var elemHalfHeight = anim._elem.height() / 2;
            this.properties['top'] = wCenter.top - elemHalfHeight + settings.offset;
        },
        centerH: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            var elemHalfWidth = anim._elem.width() / 2;
            this.properties['left'] = wCenter.left - elemHalfWidth + settings.offset;
        },
        bottomOutside: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            this.properties['top'] = wHeight + settings.offset;
        },
        topOutside: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            this.properties['top'] = -anim._elem.height() + settings.offset;
        },
        rightInside: function (anim, opts) {
            var defaults = {
                    offset: 0
                },
                settings = $.extend(defaults, opts);
            this.properties['left'] = -anim._elem.width() + (wWidth) + settings.offset;
        },
        backgroundA: function (anim, opts) {
            var defaults = {
                    offset: 0,
                    bgsize: 0
                },
                settings = $.extend(defaults, opts);
            this.properties['background-position'] = anim._elem.width() + settings.offset - (1600 - wWidth) / 2;
        },
        backgroundB: function (anim, opts) {
            var defaults = {
                    offset: 0,
                    bgsize: 0
                },
                settings = $.extend(defaults, opts);
            this.properties['background-position'] = -opts.bgsize + settings.offset + (1600 - wWidth) / 2;
        }
    }
    var t = 0;
    var totalHeightPx = 2400;
    var outroHeightPx = 0;
    var detailStart = 860;
    var outroStart = 6680;
    var outroLength = 100;
    var eventsDelta = 20;
    var maxScroll = 0;
    maxScroll = totalHeightPx + 1800;
    var sectionIndex = [{
        id: "#intro1",
        name: "intro1",
        tag: "#intro1",
        position: 0,
        correct: false
    }, {
        id: "#intro3",
        name: "intro3",
        tag: "#intro3",
        position: 700,
        correct: false
    }, {
        id: "#horizontalSection",
        name: "horizontal",
        tag: "#horizontal",
        position: 4196,
        correct: false
    }, {
        id: "#model_gallery",
        name: "model_gallery",
        tag: "#model_gallery",
        position: 4196,
        correct: false
    }, {
        id: "#model_compare",
        name: "model_compare",
        tag: "#model_compare",
        position: 4196,
        correct: false
    }];
    if ($('#current_model_name').text() == "power-of-dreams-asimo") {
        sectionIndex = [{
            id: "#intro1",
            name: "intro1",
            tag: "#intro1",
            position: 0,
            correct: false
        }, {
            id: "#intro3",
            name: "intro3",
            tag: "#intro3",
            position: 700,
            correct: false
        }, {
            id: "#horizontalSection",
            name: "horizontal",
            tag: "#horizontal",
            position: 4196,
            correct: false
        }, {
            id: "#model_gallery",
            name: "model_gallery",
            tag: "#model_gallery",
            position: 4196,
            correct: false
        }];
    } else if ($('#current_model_name').text() == "power-of-dreams-technology") {
        sectionIndex = [{
            id: "#intro1",
            name: "intro1",
            tag: "#intro1",
            position: 0,
            correct: false
        }, {
            id: "#intro3",
            name: "intro3",
            tag: "#intro3",
            position: 700,
            correct: false
        }, {
            id: "#horizontalSection",
            name: "horizontal",
            tag: "#horizontal",
            position: 4196,
            correct: false
        }];
    } else if ($('#current_model_name').text() == "power-of-dreams-racing-motogp" || $('#current_model_name').text() == "power-of-dreams-racing-wsbk" || $('#current_model_name').text() == "power-of-dreams-racing-mxgp") {
        sectionIndex = [{
            id: "#intro1",
            name: "intro1",
            tag: "#intro1",
            position: 0,
            correct: false
        }, {
            id: "#intro3",
            name: "intro3",
            tag: "#intro3",
            position: 700,
            correct: false
        }, {
            id: "#horizontalSection",
            name: "horizontal",
            tag: "#horizontal",
            position: 4196,
            correct: false
        }, {
            id: "#model_gallery",
            name: "model_gallery",
            tag: "#model_gallery",
            position: 4196,
            correct: false
        }];
    } else if ($('#current_model_name').text() == "dreamware") {
        sectionIndex = [{
            id: "#intro1",
            name: "intro1",
            tag: "#intro1",
            position: 0,
            correct: false
        }, {
            id: "#intro3",
            name: "intro3",
            tag: "#intro3",
            position: 700,
            correct: false
        }, {
            id: "#horizontalSection",
            name: "horizontal",
            tag: "#horizontal",
            position: 4180,
            correct: false
        }, {
            id: "#dreamwear_all",
            name: "dreamwear_all",
            tag: "#dreamwear_all",
            position: 4185,
            correct: false
        }, {
            id: "#dreamwear_men",
            name: "dreamwear_men",
            tag: "#dreamwear_men",
            position: 4185,
            correct: false
        }, {
            id: "#dreamwear_women",
            name: "dreamwear_women",
            tag: "#dreamwear_women",
            position: 4185,
            correct: false
        }, ];
    }
    if (window.eventSectionLive) {} else {}
    var i2delta = 50;
    var animation = [{
        selector: '#verticalScrollArea',
        startAt: 0,
        endAt: 700,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                top: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                this.properties['top'] = -2400;
            },
            properties: {}
        }]
    }, {
        selector: '#verticalScrollArea',
        startAt: 0,
        endAt: 3350,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            onInit: function (anim) {},
            properties: {}
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {}
        }]
    }, {
        selector: '#intro1 > #intro1content',
        startAt: 0,
        endAt: 200,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            onInit: function (anim) {
                var yPos = Math.min(2000, -(500 - wHeight));
                this.properties['background-position'] = {
                    x: "50%",
                    y: "36%"
                };
            },
            properties: {
                "margin-top": 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            properties: {
                "margin-top": 680
            }
        }]
    }, {
        selector: '#first_frame',
        startAt: 0,
        endAt: 200,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            onInit: function (anim) {
                var yPos = Math.min(2000, -(500 - wHeight));
                this.properties['background-position'] = {
                    x: "50%",
                    y: "36%"
                };
            },
            properties: {
                "margin-top": 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            properties: {
                "margin-top": 610
            }
        }]
    }, {
        selector: '#first_frame',
        startAt: 260,
        endAt: 345,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            onInit: function (anim) {},
            properties: {
                "opacity": 1
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            properties: {
                "opacity": 0
            }
        }]
    }, {
        selector: '#intro1 > #intro1content',
        startAt: 180,
        endAt: 285,
        onInit: function (anim) {},
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            onInit: function (anim) {},
            properties: {
                "opacity": 1
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            properties: {
                "opacity": 0
            }
        }]
    }, {
        selector: '#intro3 #fireblade_moto',
        startAt: 130,
        endAt: 790,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                "top": 0,
                "left": 1600
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Sinusoidal.EaseOut,
            properties: {
                "top": 890,
                "left": -300
            }
        }]
    }, {
        selector: '#intro3 #fireblade_colons',
        startAt: 100,
        endAt: 710,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                "top": 0,
                "left": -1600
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Sinusoidal.EaseOut,
            properties: {
                "top": 0,
                "left": 1300
            }
        }]
    }, {
        selector: '#horizontalSection > .content > p',
        startAt: detailStart - 300,
        endAt: detailStart,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                "margin-top": -400,
                "margin-bottom": 200
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Quadratic.EaseOut,
            onInit: function (anim) {
                anim._elem.height() / 2
                var yPos = Math.max(40, 240 - (anim._elem.height() / 2) - Math.max(0, 1000 - wHeight) / 2);
                this.properties['margin-top'] = yPos;
            },
            properties: {
                "margin-bottom": 0
            }
        }]
    }, {
        selector: '#detail1',
        startAt: detailStart,
        endAt: detailStart + 500,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutside.call(this, anim, {});
                this.properties['background-position'] = -1600 + (1600 - wWidth) / 2;
            },
            properties: {}
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutside.call(this, anim, {});
                this.properties['background-position'] = 320 - (1600 - wWidth) / 2;
            },
            properties: {}
        }]
    }, {
        selector: '#detail2',
        startAt: detailStart + 500,
        endAt: detailStart + 1050,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": -1100
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 400
            }
        }]
    }, {
        selector: '#detail3',
        startAt: detailStart + 600,
        endAt: detailStart + 1200,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 0
            }
        }]
    }, {
        selector: '#detail4',
        startAt: detailStart + 760,
        endAt: detailStart + 1470,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": -1500
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 935
            }
        }]
    }, {
        selector: '#detail5',
        startAt: detailStart + 1030,
        endAt: detailStart + 1575,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {}
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {}
        }]
    }, {
        selector: '#detail5',
        startAt: detailStart + 1030,
        endAt: detailStart + 1400,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "background-position": -800
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Sinusoidal.EaseInOut,
            onInit: function (anim) {},
            properties: {
                "background-position": 0
            }
        }]
    }, {
        selector: '#detail6',
        startAt: detailStart + 1160,
        endAt: detailStart + 1960,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
                this.properties['background-position'] = {
                    x: -1400 + (settings.clampWidth - wWidth) / 2,
                    y: "top"
                };
            },
            properties: {}
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
                this.properties['background-position'] = {
                    x: 1400 - (settings.clampWidth - wWidth) / 2,
                    y: "top"
                };
            },
            properties: {}
        }]
    }, {
        selector: '#detail6 > .content',
        startAt: detailStart + 1160,
        endAt: detailStart + 1960,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "left": -1600
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "left": 1600
            }
        }]
    }, {
        selector: 'html.opacity #detail6 > .content > p',
        startAt: detailStart + 1400,
        endAt: detailStart + 1500,
        onEndAnimate: function (direction) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "opacity": 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "opacity": 1
            }
        }]
    }, {
        selector: 'html.opacity #detail6 > .content > p',
        startAt: detailStart + 1600,
        endAt: detailStart + 1700,
        onEndAnimate: function (direction) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "opacity": 1
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "opacity": 0
            }
        }]
    }, {
        selector: '#detail7',
        startAt: detailStart + 1555,
        endAt: detailStart + 2100,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": -1000
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 400
            }
        }]
    }, {
        selector: '#detail8',
        startAt: detailStart + 1700,
        endAt: detailStart + 2325,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": -900
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 775
            }
        }]
    }, {
        selector: '#detail9',
        startAt: detailStart + 1937,
        endAt: detailStart + 2467,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 0
            }
        }]
    }, {
        selector: '#detail10',
        startAt: detailStart + 2075,
        endAt: detailStart + 2700,
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": -1100
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.leftOutsideClampWidth.call(this, anim, {});
            },
            properties: {
                "background-position": 825
            }
        }]
    }, {
        selector: '#detail11',
        startAt: detailStart + 2300,
        endAt: detailStart + 3062,
        onEndAnimate: function (direction) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                animationFunctions.rightOutsideClampWidth.call(this, anim, {});
            },
            properties: {}
        }, {
            position: 0.5,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                this.properties['left'] = -(1600 - wWidth) / 2;
            },
            properties: {}
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {}
        }]
    }, {
        selector: '#model-header-panel',
        startAt: detailStart + 2480,
        endAt: detailStart + 2610,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '#scroll-down-footer-message',
        startAt: detailStart + 2610,
        endAt: detailStart + 2680,
        onInit: function () {},
        onEndAnimate: function (anim) {
            $('#scroll-down-footer-message').toggleClass("blink");
        },
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                $('#scroll-down-footer-message').addClass("blink");
            },
            properties: {
                opacity: 1
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }]
    }, {
        selector: '#model-info-panel',
        startAt: detailStart + 2550,
        endAt: detailStart + 2660,
        onInit: function () {},
        onEndAnimate: function (anim) {
            $('#scrollTeaser').css("visibility", "visible");
        },
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '#model-specs-panel',
        startAt: detailStart + 2650,
        endAt: detailStart + 2800,
        onInit: function () {},
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '#model-accessories-panel',
        startAt: detailStart + 2630,
        endAt: detailStart + 2780,
        onInit: function () {},
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '#model-menu-panel',
        startAt: detailStart + 2680,
        endAt: detailStart + 2900,
        onInit: function () {},
        onEndAnimate: function (anim) {
            bugFixScroller();
            enableJScrollPaneOnModelInfoPage();
        },
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '#model-specs-menu-panel',
        startAt: detailStart + 2690,
        endAt: detailStart + 2840,
        onInit: function () {},
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '#dreamwear_menu',
        startAt: detailStart + 3000,
        endAt: detailStart + 3210,
        onInit: function () {},
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '.dreamware_items_area.cat_dreamwear,#dreamwear_footer_logo',
        startAt: detailStart + 3100,
        endAt: detailStart + 3240,
        onInit: function () {},
        onEndAnimate: function (anim) {
            if (!$('#dreamwear_menu_container ul.d_relative li a.smooth_hover.first').hasClass("active")) {
                $('#dreamwear_menu_container ul.d_relative li a.smooth_hover.first').trigger("click");
            }
        },
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                opacity: 1
            }
        }]
    }, {
        selector: '.simple-nav-link',
        startAt: detailStart + 3100,
        endAt: detailStart + 3310,
        onInit: function () {},
        onEndAnimate: function (anim) {
            if ($('#current_model_name').text() == "dreamware") {
                if (!pageScrollerisFlag) {
                    pageScrollerisFlag = true;
                    pageScrollerisDisabled = true;
                    scrollAnimate.pause();
                    $('#scrollBar').fadeOut(200);
                    $('.nav-link.active').removeClass("active");
                    $('.nav-link3').addClass("active");
                    TooltipEnabled('nav-link3');
                }
            }
        },
        keyframes: [{
            position: 0,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "top": 100
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {},
            properties: {
                "top": 0
            }
        }]
    }, {
        selector: '#verticalScrollArea',
        onInit: function () {
            this.startAt = outroStart;
            this.endAt = maxScroll;
        },
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            onInit: function (anim) {
                this.properties['top'] = -4030 + Math.max(((wHeight - 1000) / 2), 0);
            },
            properties: {}
        }, {
            position: 1,
            ease: TWEEN.Easing.Linear.EaseNone,
            onInit: function (anim) {
                this.properties.top = -totalHeightPx + Math.max(0, wHeight - 1000);
            },
            properties: {}
        }]
    }, {
        selector: '#outro1',
        onInit: function () {
            this.startAt = outroStart + 000 - ratio;
            this.endAt = outroStart + 1300 - ratio;
        },
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                "background-position": {
                    x: "50%",
                    y: -500
                }
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Sinusoidal.EaseOut,
            properties: {
                "background-position": {
                    x: "50%",
                    y: 200
                }
            }
        }]
    }, {
        selector: '#outro1 > .content',
        onInit: function () {
            this.startAt = outroStart + 420 - ratio;
            this.endAt = outroStart + 1100 - ratio;
        },
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                "margin-top": 0
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Sinusoidal.EaseOut,
            properties: {
                "margin-top": 600
            }
        }]
    }, {
        selector: '#outro2 > .content1',
        startAt: outroStart + 500,
        endAt: outroStart + 1000,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                "background-position": -200,
                "margin-top": 180
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Sinusoidal.EaseOut,
            properties: {
                "background-position": 0,
                "margin-top": 180
            }
        }]
    }, {
        selector: '#outro2 > .content2',
        startAt: outroStart + 500,
        endAt: outroStart + 1000,
        onEndAnimate: function (anim) {},
        keyframes: [{
            position: 0,
            properties: {
                "margin-top": 650
            }
        }, {
            position: 1,
            ease: TWEEN.Easing.Sinusoidal.EaseOut,
            properties: {
                "margin-top": 650
            }
        }]
    }];

    function checkSection() {
        if (checkSectionLock > 0) {
            checkSectionLock--;
            return;
        }
        if ((pulseReminders > 0) && ((new Date()).getTime() - lastActivity > 1000)) {
            if (startPulseScrollTeaser()) {
                pulseReminders--;
            }
        }
        var scrollTop = scrollAnimate.getScrollTop();
        var extraFudgeFactor = Math.max(0, wHeight - 1000);
        for (var i = 0; i < sectionIndex.length; i++) {
            var section = sectionIndex[i];
            var actualScrollTop = section.correct ? scrollTop + ratio + extraFudgeFactor : scrollTop;
            if (actualScrollTop > section.position) {
                var sectionEnd = (i == sectionIndex.length - 1) ? scrollAnimate.getMaxScroll() + ratio + extraFudgeFactor : sectionIndex[i + 1].position;
                if (actualScrollTop < sectionEnd) {
                    if (i > 3)
                        pulseReminders = 0;
                    enterSection(i);
                    return;
                }
            }
        };
    }

    function gotoSectionTag(sectionTag) {
        for (var i = 0; i < sectionIndex.length; i++) {
            if (sectionIndex[i].tag === sectionTag) {
                var newpos = sectionIndex[i].position + 1;
                if (sectionIndex[i].correct == true)
                    newpos -= ratio;
                scrollAnimate.scrollTo(newpos);
                enterSection(i);
                return;
            }
        }
    }

    function enterSection(index) {
        if ($('#current_model_name').text() == "dreamware" && $('.nav-link3').hasClass("active") && index == 2) {
            return false;
        }
        if ($('#current_model_name').text() != "dreamware" && index == 1) {
            return false;
        }
        if (index == 3 || index == 4) {
            index = 2;
        }
        if (currentSection == index) {
            currentSection = index;
            return false;
        }
        currentSection = index;
        $('#navigation > a').each(function (i, elm) {
            if (i == index) {
                $(elm).addClass('active');
                if (index > 0 && index < 4) {
                    TooltipEnabled('nav-link' + (index + 1));
                }
            } else {
                $(elm).removeClass('active');
            }
        });
    }

    function TooltipEnabled(nav_link_class) {
        var $this = $('#navigation a.' + nav_link_class);
        if ($this.hasClass('nav-link')) {
            var $navtip = $('#navtip');
            var pos = $this.offset().left + 10;
            $('#navtipArrow').stop().animate({
                left: pos
            }, tooltipJustOpened ? 0 : 0);
            $navtip.html($this.data('name'));
            var width = $navtip.width();
            var left = Math.max(0, pos - (width / 2));
            $navtip.stop().animate({
                left: left
            }, tooltipJustOpened ? 0 : 0);
            $('#navtip').show();
            $('#navtipArrow').show();
            tooltipJustOpened = true;
        }
    }

    function initalizeNavigation() {
        var navContent = "";
        for (var i = 0; i < sectionIndex.length; i++) {
            sectionIndex[i].name = $(sectionIndex[i].id).data("navigationTag");
            if (sectionIndex[i].tag == "#model_gallery") {
                navContent += "<a class='nav-link nav-link" + (i + 1) + "' rel='nav-link" + (i + 1) + "' id='model_gallery_nav' href='" + sectionIndex[i].tag + "' data-name='" + sectionIndex[i].name + "'><div></div></a>";
            } else if (sectionIndex[i].tag == "#model_compare") {
                navContent += "<a class='nav-link nav-link" + (i + 1) + "' rel='nav-link" + (i + 1) + "' id='model_compare_nav' href='" + sectionIndex[i].tag + "' data-name='" + sectionIndex[i].name + "'><div></div></a>";
            } else if (sectionIndex[i].tag == "#dreamwear_men") {
                navContent += "<a name=\"men\" class='simple-nav-link nav-link" + (i + 1) + "' id='dreamwear_men_nav' href='javascript:;' data-name='Dreamwear for men'><span class=\"footer_dreamwear_text\">MEN</span></a>";
            } else if (sectionIndex[i].tag == "#dreamwear_women") {
                navContent += "<a name=\"women\" class='simple-nav-link nav-link" + (i + 1) + "' id='dreamwear_women_nav' href='javascript:;' data-name='Dreamwear for women'><span class=\"footer_dreamwear_text\">WOMEN</span></a>";
            } else if (sectionIndex[i].tag == "#dreamwear_all") {
                navContent += "<a name=\"all\" class='start_active simple-nav-link nav-link" + (i + 1) + "' id='dreamwear_all_nav' href='javascript:;' data-name='All Dreamwares'><span class=\"footer_dreamwear_text\">ALL</span></a>";
            } else {
                navContent += "<a class='nav-link nav-link" + (i + 1) + "' rel='nav-link" + (i + 1) + "' href='" + sectionIndex[i].tag + "' data-name='" + sectionIndex[i].name + "'><div></div></a>";
            }
        }
        $('#navigation').html(navContent);
        $('#navigation').parent().parent().append("<div id='navtip'></div><div id='navtipArrow'></div>");
        TooltipEnabled('nav-link1');
        var fix_navigation_after_scroller_pause_flag = 1;
        $('.nav-link').click(function (e) {
            var this_navlink = $(this);
            if (fix_navigation_after_scroller_pause_flag == 2) {
                fix_navigation_after_scroller_pause_flag = 1;
                $('#footer-modelsboxes').fadeOut(50, function () {
                    $(this).fadeIn(50, function () {
                        $('#scrollBar').show();
                        this_navlink.trigger("click");
                    });
                });
                pageScrollerisFlag = false;
            }
            if (!$(this).hasClass("nav-link3") && pageScrollerisDisabled && fix_navigation_after_scroller_pause_flag == 1) {
                fix_navigation_after_scroller_pause_flag = 2;
                scrollAnimate.resume();
                pageScrollerisDisabled = false;
                $('#scrollBar').fadeIn(60, function () {
                    $('#footer-modelsboxes').fadeOut(50, function () {
                        $(this).fadeIn(50, function () {
                            $('#scrollBar').show();
                            this_navlink.trigger("click");
                        });
                    });
                });
            } else {
                $('#navigation a.nav-link.active').removeClass("active");
                TooltipEnabled($(this).attr("rel"));
                $(this).addClass("active");
                e.preventDefault();
                var hash = this.href.substring(this.href.indexOf('#'));
                stopPulseScrollTeaser();
                gotoSectionTag(hash);
                var easing_gallery = 'easeInOutExpo';
                if ($(this).attr("id") == "model_gallery_nav") {
                    var _rel = $(this).attr("rel");
                    $('#navigation a.active').removeClass("active");
                    $('#model_compare_nav').removeClass("active");
                    $('#model_gallery_nav').addClass("active");
                    $('.simple-nav-link').removeClass("active");
                    TooltipEnabled(_rel);
                    $('#scrollTeaser').css("visibility", "hidden");
                    if (!bgStrecherEnabled) {
                        enableFullscreenGallery();
                    }
                    $('#compare_list_container').fadeOut(250, function () {
                        $('#model_compare').animate({
                            left: "1600px"
                        }, 1000, easing_gallery, function () {
                            resetModelCompare();
                        });
                    });
                    $('#model_gallery_nav').delay(600).animate({
                        opacity: "1"
                    }, 200);
                    $('#scrollBar').stop(true, true).fadeOut(320);
                    $('#model_gallery').delay(500).animate({
                        left: "0px"
                    }, 1200, easing_gallery, function () {
                        $('#gallery_bar_container').fadeIn(450, easing_gallery);
                        TooltipEnabled(_rel);
                    });
                } else if ($(this).attr("id") == "model_compare_nav") {
                    var _rel = $(this).attr("rel");
                    $('#navigation a.active').removeClass("active");
                    $('#model_gallery_nav').removeClass("active");
                    $('#model_compare_nav').addClass("active");
                    $('.simple-nav-link').removeClass("active");
                    TooltipEnabled(_rel);
                    checkSectionLock = 3;
                    pulseReminders = 0;
                    stopPulseScrollTeaser();
                    $('#scrollBar').stop(true, true).fadeIn(320);
                    $('#model_gallery_nav').animate({
                        opacity: "0.5"
                    }, 200);
                    $('#model_gallery').animate({
                        left: "1600px"
                    }, 1000, easing_gallery);
                    $('#gallery_bar_container').stop(true, true).fadeOut(250, easing_gallery);
                    $('#model_compare_nav').delay(600).animate({
                        opacity: "1"
                    }, 200);
                    $('#scrollBar').stop(true, true).fadeOut(320);
                    $('#model_compare').delay(600).animate({
                        left: "0px"
                    }, 1200, easing_gallery, function () {
                        TooltipEnabled(_rel);
                        $('#compare_list_container').fadeIn(250);
                        $('#model_list_form ul li input#' + $('#current_model_name').text()).parent().parent().prev().addClass("selected").next().show();
                        $('#model_list_form ul li input#' + $('#current_model_name').text()).trigger("click");
                    });
                } else {
                    $('#navigation a.nav-link.active').removeClass("active");
                    $('#model_gallery_nav, #model_compare_nav').removeClass("active");
                    TooltipEnabled($(this).attr("rel"));
                    $(this).addClass("active");
                    checkSectionLock = 3;
                    pulseReminders = 3;
                    if ($(this).hasClass("nav-link3") || $(this).attr("href") == "#horizontal") {
                        pulseReminders = 0;
                        stopPulseScrollTeaser();
                    } else {
                        $('#scrollTeaser').css("visibility", "visible");
                        startPulseScrollTeaser();
                    }
                    $('#scrollBar').stop(true, true).fadeIn(320);
                    $('#model_gallery_nav').animate({
                        opacity: "0.5"
                    }, 200);
                    $('#model_compare_nav').animate({
                        opacity: "0.5"
                    }, 200);
                    $('#model_gallery').animate({
                        left: "1600px"
                    }, 1000, easing_gallery);
                    $('#compare_list_container').fadeOut(250, function () {
                        $('#model_compare').animate({
                            left: "1600px"
                        }, 1000, easing_gallery, function () {
                            resetModelCompare();
                        });
                    });
                    $('#gallery_bar_container').stop(true, true).fadeOut(250, easing_gallery);
                }
                return false;
            }
            return false;
        });
        if (!isTouch()) {
            $('#navigation').hover(function () {
                $('#navtip').show();
                $('#navtipArrow').show();
                tooltipJustOpened = true;
            }, function () {
                tooltipJustOpened = true;
                TooltipEnabled($('#navigation a.active').attr("rel"));
            });
            $('.nav-link').bind('mouseover', function (e) {
                var $this = $(this);
                var $navtip = $('#navtip');
                var pos = $this.offset().left + 10;
                $('#navtipArrow').stop().animate({
                    left: pos
                }, tooltipJustOpened ? 0 : 150);
                $navtip.html($this.data('name'));
                var width = $navtip.width();
                var left = Math.max(0, pos - (width / 2));
                $navtip.stop().animate({
                    left: left
                }, tooltipJustOpened ? 0 : 150);
                tooltipJustOpened = false;
            });
        }
        enterSection(0);
    }

    function bugFixScroller() {
        if ($('#current_model_name').text() != "dreamware") {
            if (!pageScrollerisFlag) {
                pageScrollerisFlag = true;
                pageScrollerisDisabled = true;
                scrollAnimate.pause();
                $('#scrollBar').fadeOut(200);
                $('.nav-link.active').removeClass("active");
                $('.nav-link3').addClass("active");
                TooltipEnabled('nav-link3');
            }
        }
        $('#scrollTeaser').css("visibility", "hidden");
    }

    function activateScrollBar(thumbHeight) {
        scrollThumbHeight = thumbHeight;
        scrollThumbPosition = 0;
        scrollPercent = 0;
        isScrolling = false;
        $scrollBar = $('#scrollBar');
        $scrollBar.show();
        $scrollThumb = $('#scrollBar .thumb');
        $scrollThumb.css('height', scrollThumbHeight + "px");
        $scrollThumb.bind('mousedown', startScroll);
    }

    function resizeScrollBar() {
        scrollBarHeight = wHeight - 40;
        $scrollBar.css('height', scrollBarHeight + "px");
        setScrollBarPosition(scrollPercent);
    }

    function startScroll(event) {
        isScrolling = true;
        thumbDelta = scrollThumbPosition - event.pageY;
        $(document).bind('mousemove', scrollUpdate);
        $(document).bind('mouseup', endScroll);
        return false;
    }

    function scrollUpdate(event) {
        scrollThumbPosition = event.pageY + thumbDelta;
        scrollThumbPosition = Math.max(0, Math.min(scrollBarHeight - scrollThumbHeight, scrollThumbPosition));
        scrollPercent = scrollThumbPosition / (scrollBarHeight - scrollThumbHeight);
        scrollPercent = Math.max(0, Math.min(1, scrollPercent));
        scrollAnimate.scrollTo(maxScroll * scrollPercent);
        return false;
    }

    function setScrollBarPosition(percent) {
        scrollThumbPosition = (scrollBarHeight - scrollThumbHeight) * percent;
        $scrollThumb.css('top', scrollThumbPosition);
    }

    function endScroll(event) {
        isScrolling = false;
        $(document).unbind('mousemove', scrollUpdate);
        $(document).unbind('mouseup', endScroll);
        return false;
    }

    function showOverlay(opacity, inSpeed, outSpeed, contentData) {
        scrollAnimate.pause();
        $overlay.data('outSpeed', outSpeed);
        var contentClass;
        if (contentData !== undefined) {
            $overlay.data('contentData', contentData);
            if (contentData.type == "bgimage") {
                contentClass = ".imageOverlay";
                $overlay.after('<div class="overlayContent imageOverlay offscreen ' + contentData.bgclass + '"><div id="overlayClose"><a href="#">Close</a></div></div>');
                $(contentClass).css({
                    'width': contentData.width,
                    'height': contentData.height
                });
            } else if (contentData.type == "video") {
                contentClass = ".videoOverlay";
                if (!player) {
                    $overlay.after('<div class="overlayContent videoOverlay offscreen"><div id="overlayClose"><a href="#">Close</a></div><video id="player" class="video-js vjs-default-skin" controls poster="' + contentData.poster + '" width="' + contentData.width + '" height="' + contentData.height + '" preload="auto"><source type="video/mp4" src="' + contentData.url + '" /></video></div>');
                    _V_("player", {}, function () {
                        player = this;
                        player.load();
                        setTimeout(function () {
                            player.play();
                        }, 100);
                    });
                } else {
                    player.src(contentData.url);
                    player.size(contentData.width, contentData.height);
                    player.load();
                    setTimeout(function () {
                        player.play();
                    }, 100);
                }
            } else {
                $overlay.after('<div class="overlayContent"></div>');
            }
            $('.overlayContent').css({
                'margin-left': -contentData.width / 2,
                'margin-top': -contentData.height / 2
            });
        }
        $('#overlayClose').fadeTo(inSpeed, 1).bind('click', removeOverlay);
        $overlay.fadeTo(inSpeed, opacity, function () {
            $overlay.bind('click', removeOverlay);
            $(contentClass).removeClass('offscreen');
            $('#overlayClose').show();
        });
    }

    function removeOverlay(e) {
        if (e)
            e.preventDefault();
        $overlay.unbind('click', removeOverlay);
        $overlay.fadeOut($overlay.data('outSpeed') || 100);
        $('#overlayClose').fadeOut($overlay.data('outSpeed') || 100).unbind('click', removeOverlay);
        var contentData = $overlay.data('contentData');
        if (player) {
            player.pause();
            if ($.browser.mozilla) {
                player.src("");
            }
        }
        if (contentData) {
            if (contentData.closeAction == "destroy") {
                $('.overlayContent:not(.offscreen)').remove();
            }
        }
        $('.overlayContent').addClass('offscreen');
        $overlay.data('contentData', null);
        scrollAnimate.resume();
    }

    function runPulseScrollTeaser() {
        if (!pulseScrollTeaserRunning) {
            $('#scrollTeaser').hide();
            return;
        }
        var pulseSpeed = 470;
        $('#scrollTeaser-down1').fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed);
        $('#scrollTeaser-down2').delay(200).fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed);
        $('#scrollTeaser-down3').delay(370).fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed, runPulseScrollTeaser);
    }

    function startPulseScrollTeaser() {
        if (pulseScrollTeaserRunning == true)
            return false;
        pulseScrollTeaserRunning = true;
        $('#scrollTeaser').show();
        runPulseScrollTeaser();
        return true;
    }

    function stopPulseScrollTeaser() {
        pulseScrollTeaserRunning = false;
    }

    function buildGallery() {}

    function isTouch() {
        return 'ontouchstart' in window;
    }

    function track(a, b, c) {
        if (settings.tracking == true) {}
    }

    function runIntro() {}

    function initalizePage() {
        $overlay = $('#overlay');
        $('.autoPlay').click(function (e) {
            showOverlay(0.8, 1000, 100, {
                closeAction: "destroy",
                width: 800,
                height: 400
            });
            return false;
        });
        runIntro();
        buildGallery();
    }
    var init = function (opts) {
        settings = $.extend(defaults, opts);
        initalizeNavigation();
        if (!isTouch())
            activateScrollBar(37);
        if (window.location.hash) {};
        scrollAnimate = ScrollAnimator();
        scrollAnimate.init({
            animation: animation,
            maxScroll: maxScroll,
            useRAF: false,
            tickSpeed: 50,
            scrollSpeed: 15,
            debug: false,
            tweenSpeed: .2,
            startAt: settings.startAt,
            container: $('#main'),
            onStart: function () {},
            onResize: function (page) {
                wHeight = page.wHeight;
                wWidth = (settings.clampWidth > 0 && page.wWidth > settings.clampWidth) ? settings.clampWidth : page.wWidth;
                wCenter = page.wCenter;
                outroComp = Math.max(0, 1000 - wHeight) / 2;
                var pcent = (outroComp * 2) / outroHeightPx;
                ratio = (outroLength * pcent);
                $('.scale').css({
                    'width': wWidth + 'px',
                    'height': wHeight + 'px'
                });
                var centerPcent = 100;
                if (wHeight < 750) {
                    centerPcent = ((wHeight / 750) * 100);
                }
                $('#horizontalSection').css('background-position', 'center ' + centerPcent + '%');
                if ($scrollBar)
                    resizeScrollBar();
                scrollAnimate.scrollTo(scrollAnimate.getScrollTop() + 3);
                if (player) {
                    var contentData = $overlay.data('contentData');
                    if (wWidth < 1024) {
                        player.size(700, 300);
                        $('.videoOverlay').css({
                            'margin-left': -350,
                            'margin-top': -150
                        });
                    } else {
                        if (contentData) {
                            player.size(contentData.width, contentData.height);
                            $('.videoOverlay').css({
                                'margin-left': -contentData.width / 2,
                                'margin-top': -contentData.height / 2
                            });
                        }
                    }
                }
            },
            onUpdate: function (scrollTop) {
                updateCount++;
                if (updateCount == 5) {
                    stopPulseScrollTeaser();
                }
                if ($scrollBar)
                    setScrollBarPosition(scrollTop / maxScroll);
                if (pulseReminders > 0)
                    lastActivity = (new Date()).getTime();
                pulseScrollTeaserRunning = false;
            }
        });
        initalizePage();
        if (scrollAnimate.isDebug()) {
            $('#status').show();
        };
        $(window).keydown(function (e) {
            if (e.keyCode == 40 || e.keyCode == 39) {
                var currentScroll = scrollAnimate.getScrollTop();
                var targetScroll = currentScroll + 25;
                if (targetScroll > scrollAnimate.getMaxScroll() + ratio) {
                    targetScroll = currentScroll;
                }
                scrollAnimate.scrollTo(targetScroll);
            }
            if (e.keyCode == 38 || e.keyCode == 37) {
                var currentScroll = scrollAnimate.getScrollTop();
                var targetScroll = currentScroll - 25;
                if (targetScroll < 0) {
                    targetScroll = 0;
                }
                scrollAnimate.scrollTo(targetScroll);
            }
            if (e.keyCode == 34) {
                var currentScroll = scrollAnimate.getScrollTop();
                var targetScroll = currentScroll + 75;
                if (targetScroll > scrollAnimate.getMaxScroll() + ratio) {
                    targetScroll = currentScroll;
                }
                scrollAnimate.scrollTo(targetScroll);
            }
            if (e.keyCode == 33) {
                var currentScroll = scrollAnimate.getScrollTop();
                var targetScroll = currentScroll - 75;
                if (targetScroll < 0) {
                    targetScroll = 0;
                }
                scrollAnimate.scrollTo(targetScroll);
            }
        });
        setInterval(checkSection, settings.sectionCheckInterval);
        return scrollAnimate;
    }
    return {
        init: init,
        scrollAnimate: scrollAnimate
    }
})();;
$(document).ready(function () {
	
	/*
    if ($('body').hasClass("model_simple")) {
        window.siteAnimator = HondaModelPageInitModelSimple.init();
    } else {
        window.siteAnimator = HondaModelPageInit.init();
    }
    sendEmailToFriend();
	*/
});

function CheckValidMailFriend(email) {
    if (email != "") {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
        if (emailPattern.test(email)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function sendEmailToFriend() {
    $('#dreamwear_print_area .friend_email .button').live("click", function () {
        var url = $('#send_to_friend_frame1').attr("rel");
        url = 'http://www.honda-motorcycles.gr/' + url;
        var friend_mail = $('#dreamwear_print_area .friend_email input.friend_email_text').val();
        var validEmail = CheckValidMailFriend(friend_mail);
        if (!validEmail) {
            alert("Το email δεν είναι εγκυρο!");
        } else {
            $.post('ajax.send-to-friend.php', {
                friend_mail: friend_mail,
                url: url
            }, function (data) {
                if (data == "OK") {
                    alert("Το email έχει σταλεί!");
                    $('#dreamwear_print_area .friend_email input.friend_email_text').val("")
                }
            });
        }
    });
}

function homePageInit() {
    navigateToSection();
    screenSetup();
    screenResize();
    topMenu();
    homeGallery();
    LoadModelPage();
    initializeTooltips();
    mainMenu();
    contactFormEvents();
    $('#main-categories').addClass("opened");
    $('#start').addClass("opened").addClass("inactive");
    $('#main-categories').show();
    $('#social-icons-home').hide();
    $('#close_category_boxes,#close_boxes').live("click", function () {
        $('#footer-modelsboxes').trigger("click");
    });
}

function navigateToSection() {
    var section_ = $('#current_model_nav').text();
    if ($('#current_model_trigger_link').length) {
        var trigger_link = $('#current_model_trigger_link').text();
        if ($('#model-menu-panel a.' + trigger_link + '').length) {
            $('#current_model_trigger_link').delay(1600).animate({
                left: "1px"
            }, 100, function () {
                $('#model-menu-panel a.' + trigger_link).trigger("click");
            });
        }
    }
    if (parseInt(section_) == 3) {
        $('a.nav-link3').trigger("click");
    } else {
        $('a.nav-link1').trigger("click");
    }
}

function enableJScrollPaneOnModelInfoPage() {
    if (true) {
        $('#model-info-container .middler').jScrollPane({
            scrollbarWidth: 8,
            scrollbarMargin: 0,
            showArrows: false,
            wheelSpeed: 18,
            animateInterval: 300,
            verticalDragMinHeight: 60,
            verticalDragMaxHeight: 60,
            autoReinitialise: true,
            autoReinitialiseDelay: 900,
            maintainPosition: true
        });
        var obj = $('#model-header-panel').parent();
        if (obj.hasClass("jspPane")) {
            obj.height(928);
            obj.next().css({
                top: "26px",
                height: "94px"
            });
        }
    }
    $('#model-specs-panel .panelscroll').jScrollPane({
        scrollbarWidth: 8,
        scrollbarMargin: 0,
        showArrows: false,
        wheelSpeed: 18,
        animateInterval: 300,
        verticalDragMinHeight: 60,
        verticalDragMaxHeight: 60,
        autoReinitialise: true,
        autoReinitialiseDelay: 900,
        maintainPosition: true
    });
}

function resetModelCompare() {
    $('#model_list_form .category ul li input').each(function () {
        $(this).attr("checked", false);
        $(this).removeClass("selected");
        $(this).prev().removeClass("selected");
    });
    $('#model_compare .compare-model-specs').remove();
}

function init_() {
    if ($.browser.mozilla) {
        enableFancyboxVideoPopup("swf");
    } else {
        enableFancyboxVideoPopup("iframe");
    }
    var easing_model_menu_panel = 'easeInOutExpo';
    $('#model-accessories-panel a.more_arrow').click(function () {
        var index = $(this).parent().index();
        var obj = $(this).parent().find('.more');
        if (obj.hasClass("opened")) {
            $(this).removeClass("opened");
            obj.slideUp(200).removeClass("opened");
            if (index == 2) {
                obj.parent().parent().animate({
                    bottom: 0
                }, 250);
            }
        } else {
            obj.slideDown(300, function () {
                if (index == 2) {
                    obj.parent().parent().animate({
                        bottom: obj.height()
                    }, 250);
                }
                $('#model-accessories-panel a.more_arrow.opened').trigger("click");
                $(this).parent().find('.more_arrow').addClass("opened");
            }).addClass("opened");
        }
    });
    $('a#accessories_panel_close').click(function () {
        $('#model-menu-panel a.info').addClass("opened");
        $('#model-accessories-panel').animate({
            left: "1600px"
        }, 600, easing_model_menu_panel, function () {
            $(this).hide();
            $('#model-specs-panel').fadeIn(10).animate({
                left: "547px"
            }, 600, easing_model_menu_panel);
            $('#model-menu-panel a.accessories').removeClass("opened");
        });
    });
    $('#model-menu-panel ul li a.info').click(function () {
        if (!$(this).hasClass("opened")) {
            $('a#accessories_panel_close').trigger("click");
        }
        $('#model-info-panel .model_info_panel_element.active').fadeOut(360, function () {
            $(this).removeClass("active");
        });
        $('#model-info-panel .model_info_panel_element.info').fadeIn(420, function () {
            $(this).addClass("active");
        });
    });
    $('#model-menu-panel ul li a.accessories').click(function () {
        if ($(this).hasClass("opened")) {
            $('a#accessories_panel_close').trigger("click");
        } else {
            $('#model-menu-panel a.opened').removeClass("opened");
            $('#model-menu-panel a.accessories').addClass("opened");
            $('#model-info-panel .model_info_panel_element.active').fadeOut(360, function () {
                $(this).removeClass("active");
            });
            $('#model-info-panel .model_info_panel_element.accessories').fadeIn(420, function () {
                $(this).addClass("active");
            });
            $('#model-specs-panel').animate({
                left: "1600px"
            }, 600, easing_model_menu_panel, function () {
                $(this).hide();
                $('#model-accessories-panel').fadeIn(10).animate({
                    left: "547px"
                }, 600, easing_model_menu_panel);
            });
        }
    });
    $('a.close-compare-model').live("click", function () {
        $(this).parent().animate({
            width: "0px",
            opacity: 0
        }, 370, function () {
            var model_to_uncheck = $(this).attr("rel");
            $(this).remove();
            $('#model_list_form .category ul li input#' + model_to_uncheck).removeClass("selected").attr("checked", false).prev().removeClass("selected");
        });
    });
    $('#model_list_form .category .cat_').click(function () {
        if ($(this).parent().find('ul').is(":visible")) {
            $(this).parent().find('ul').slideUp(300).prev().removeClass("selected")
        } else {
            $('#model_list_form .category ul').slideUp(300).prev().removeClass("selected");
            $(this).parent().find('ul').slideDown(600).prev().addClass("selected");
        }
    });
    $('#model_list_form .category ul li input').each(function () {
        $(this).attr("checked", false);
    });
    $('#model_list_form .category ul li input').click(function () {
        var model_code = $(this).attr("name");
        if ($(this).hasClass("selected")) {
            $('#model_compare .compare-model-specs.' + model_code).fadeOut(200, function () {
                $(this).remove();
            });
            $(this).removeClass("selected");
            $(this).prev().removeClass("selected");
        } else {
            $(this).addClass("selected");
            $(this).prev().addClass("selected");
            if ($('#model_compare .compare-model-specs').length < 3) {
                $('#loading_compare').fadeIn(40);
                $('#model_compare').append('<div class="compare-model-specs ' + model_code + '" rel="' + model_code + '"></div>');
                $.post('ajax.compare-model.php', {
                    model: model_code
                }, function (data) {
                    $('#model_compare .compare-model-specs.' + model_code).html(data).animate({
                        left: "0px"
                    }, 500);
                    modelScreenSetup();
                    enableJscrollPaneOnCompareModels($('#model_compare .compare-model-specs.' + model_code).find('.model-compare-specs-panel'));
                    $('#loading_compare').fadeOut(350);
                });
            } else {
                $('#loading_compare').fadeIn(40);
                $('#model_compare .compare-model-specs:first').animate({
                    width: "0px",
                    opacity: 0
                }, 370, function () {
                    var model_to_uncheck = $(this).attr("rel");
                    $(this).remove();
                    $('#model_list_form .category ul li input#' + model_to_uncheck).removeClass("selected").attr("checked", false).prev().removeClass("selected");
                    $('#model_compare').append('<div class="compare-model-specs ' + model_code + '" rel="' + model_code + '"></div>');
                    $.post('ajax.compare-model.php', {
                        model: model_code
                    }, function (data) {
                        $('#model_compare .compare-model-specs.' + model_code).html(data).animate({
                            left: "0px"
                        }, 500);
                        modelScreenSetup();
                        enableJscrollPaneOnCompareModels($('#model_compare .compare-model-specs.' + model_code).find('.model-compare-specs-panel'));
                        $('#loading_compare').fadeOut(350);
                    });
                });
            }
        }
    });
    $('a#close_compare').click(function () {
        $('.nav-link3').trigger("click");
    });
}

function initStandingsTablesOnCircuits() {
    if ($('#current_model_name').text() == "power-of-dreams-racing-motogp" || $('#current_model_name').text() == "power-of-dreams-racing-wsbk" || $('#current_model_name').text() == "power-of-dreams-racing-mxgp") {
        enableJscrollOnce($('#standings_area .standings_table.active'));
    }
}

function enableJscrollOnce(obj) {
    obj.jScrollPane({
        scrollbarWidth: 8,
        scrollbarMargin: 0,
        showArrows: false,
        verticalDragMinHeight: 45,
        verticalDragMaxHeight: 45,
        autoReinitialise: false,
        autoReinitialiseDelay: 470,
        maintainPosition: true
    });
}

function enableJscrollRepeat(obj) {
    obj.jScrollPane({
        scrollbarWidth: 8,
        scrollbarMargin: 0,
        showArrows: false,
        verticalDragMinHeight: 45,
        verticalDragMaxHeight: 45,
        autoReinitialise: true,
        autoReinitialiseDelay: 170
    });
}

function enableJscrollPaneOnCompareModels(obj) {
    obj.jScrollPane({
        scrollbarWidth: 8,
        scrollbarMargin: 0,
        showArrows: false,
        verticalDragMinHeight: 45,
        verticalDragMaxHeight: 45,
        autoReinitialise: true,
        autoReinitialiseDelay: 170
    });
}
var imagePanningEnabled = false;

function imagePanning() {
    imagePanningEnabled = true;
    $('#gallery_cont').live("mousemove", function (e) {
        var img_height = ($(window).width() * 1200) / 1600;
        var hidden_height_area = img_height - $(window).height();
        if (hidden_height_area > 0) {
            $('#bgstretcher ul').css("margin-top", -(e.pageY / $(window).height()) * (hidden_height_area));
        }
    });
}
var bgStrecherEnabled = false;

function modelColorPicker() {
    $('#model-info-panel .color-picker ul li a').click(function () {
        if (!$(this).hasClass("active")) {
            $('#model-info-panel .color-picker ul li a.active').removeClass("active");
            $(this).addClass("active");
            $('#model-colors-container .moto-color.current').removeClass("current").fadeOut(500);
            var rel = $(this).attr("rel");
            $('#model-colors-container .moto-color.' + rel).addClass("current").fadeIn(500);
        }
    });
    var selected_color = 0;
    var max_colors = $('#model-info-panel .color-picker ul').children().length;
    if (max_colors <= 3) {
        $('#color-picker-next,#color-picker-prev').hide();
        $('.color-picker-holder').addClass('small_holder');
    }
    $('#color-picker-prev').click(function () {
        var easing_gallery = 'easeInOutExpo';
        if (selected_color <= 0) {
            selected_color = 0;
        } else {
            selected_color--;
            $('#model-info-panel .color-picker').animate({
                left: -selected_color * (123)
            }, 400, easing_gallery);
        }
    });
    $('#color-picker-next').click(function () {
        var easing_gallery = 'easeInOutExpo';
        if (selected_color >= (max_colors - 3)) {} else {
            selected_color++;
            $('#model-info-panel .color-picker').animate({
                left: -selected_color * (123)
            }, 400, easing_gallery);
        }
    });
}

function modelScreenFix() {
    if ($.browser.msie) {
        $('#gallery_images .gallery_trigger').removeClass("no-ie");
        if (parseInt($.browser.version) >= 9) {
            $('#gallery_images .gallery_trigger').addClass('enableIETransition');
        }
    }
    modelScreenSetup();
    $(window).resize(function () {
        modelScreenSetup();
    });
}

function galleryInit2() {
    if ($('#gallery_images').hasClass("no_videos")) {
        $('#video_gallery_link').hide();
    }
    $('#gallery_images a.enableIETransition').live("mouseover", function () {
        $(this).find("img").animate({
            width: "105%"
        }, 250);
    });
    $('#gallery_images a.enableIETransition').live("mouseout", function () {
        $(this).find("img").animate({
            width: "100%"
        }, 250);
    });
    $('#gallery_info a').click(function () {
        var rel = $(this).attr("rel");
        if (rel == "filter_video" && $('#gallery_images').hasClass("no_videos")) {
            return false;
        }
        $('#gallery_images a').each(function () {
            if ($(this).hasClass(rel)) {
                $(this).fadeIn(230);
            } else {
                $(this).fadeOut(230);
            }
        });
    });
    var bottom_flag = true;
    var up_flag = true;
    $('#gallery-top-arrow').click(function () {
        if (bottom_flag) {
            bottom_flag = false;
            up_flag = true;
            var easing_gallery = 'easeInOutExpo';
            var h = $('#model_gallery a.gallery_trigger').height();
            $('#gallery_images').animate({
                top: 0
            }, 600, easing_gallery);
        }
    });
    $('#gallery-bottom-arrow').click(function () {
        if (up_flag && $('#gallery_images').children().length > 8) {
            up_flag = false;
            bottom_flag = true;
            var easing_gallery = 'easeInOutExpo';
            var h = $('#model_gallery a.gallery_trigger').height();
            $('#gallery_images').delay(180).animate({
                top: -h
            }, 600, easing_gallery);
        }
    });
    $('#gallery_bar').hover(function () {
        var easing_gallery = 'easeInOutExpo';
        if (!$(this).hasClass("opened")) {
            $(this).stop(true, false).animate({
                width: "180px"
            }, 400, easing_gallery, function () {
                $('#gallery_info').fadeIn(120);
                $(this).addClass("opened");
            });
        }
    }, function () {
        var easing_gallery = 'easeInOutExpo';
        if ($(this).hasClass("opened")) {
            $('#gallery_info').stop(true, false).fadeOut(120, function () {
                $('#gallery_bar').animate({
                    width: "51px"
                }, 400, easing_gallery, function () {
                    $(this).removeClass("opened");
                });
            });
        }
    });
    $('#gallery_close').click(function () {
        $('#gallery_cont').fadeOut(400);
        $('#gallery_cont').css("visibility", "hidden");
        $('#bgstretcher').css("width", WIDTH);
        $('#bgstretcher').css("height", HEIGHT);
        $('#gallery_bar').fadeIn(90);
        $('#countdown-footer').fadeIn(250);
    });
    $('#model_gallery a.gallery_trigger.img_').click(function () {
        $('#gallery_bar').stop(true, true).fadeOut(30);
        $('#bgstretcher').css("width", WIDTH);
        $('#bgstretcher').css("height", HEIGHT);
        var rel = $(this).find('img').attr("rel");
        $('#gallery_cont').css("display", "block");
        $('#gallery_helper #nav ul li a').each(function () {
            if ($(this).text() == rel) {
                $(this).trigger("click");
                $('#bgstretcher').css("width", WIDTH);
                $('#bgstretcher').css("height", HEIGHT);
                $('#gallery_cont').css("visibility", "visible");
                $('#gallery_cont').css("display", "none");
            }
        });
        $('#gallery_cont').fadeIn(500, function () {
            if (!imagePanningEnabled) {
                imagePanning();
            }
            $('#countdown-footer').fadeOut(350);
            $('#scrollBar').fadeOut(350);
        });
    });
    $('#footer-home').click(function () {
        $(this).addClass("active");
        $('#honda_preloader').stop(true, true).fadeIn(1600);
        return true;
    });
    $('#footer-modelsboxes').click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $('#top_menu_with_boxes').fadeOut(450, function () {
                $('.flash_video_object, .youtube_video_object').show();
            });
            $('#countdown-footer').fadeIn(370);
        } else {
            var cat1 = $('#current_model_cat1').text();
            var cat2 = $('#current_model_cat2').text();
            $('.flash_video_object, .youtube_video_object').hide();
            $(this).addClass("active");
            $('#main-categories').addClass("opened");
            $('#start').addClass("opened").addClass("inactive");
            $('#main-categories').show();
            $('#social-icons-home,#menu-boxes-container').hide();
            $('#close_category_boxes,#back_category_boxes').show();
            $('#top_menu_with_boxes').fadeIn(450);
            $('#countdown-footer').fadeOut(370);
        }
        return false;
    });
    $('#model-menu-panel a.compare').live("click", function () {
        $('#model_compare_nav').trigger("click");
    });
}

function OpenBoxesMenus() {}

function modelScreenSetup() {
    WIDTH = $(window).width();
    HEIGHT = $(window).height();
    $('#bgstretcher').css("width", WIDTH);
    $('#bgstretcher').css("height", HEIGHT);
    if (WIDTH < 1600) {
        $('#model_gallery a.gallery_trigger').css("width", (WIDTH / 4));
        $('#model_gallery a.gallery_trigger').css("height", (3 * WIDTH / 16));
        $('.compare-model-specs').css("width", (WIDTH - 250) / 3);
        $('.model-compare-specs-panel').removeClass("big");
    } else {
        $('#model_gallery a.gallery_trigger').css("width", "400px");
        $('#model_gallery a.gallery_trigger').css("height", "300px");
        $('.compare-model-specs').css("width", "450px");
        $('.model-compare-specs-panel').addClass("big");
    }
    $('.model-compare-specs-panel').height(HEIGHT / 2.55);
    $('#dreamwear_product_page .product_scroller').height(HEIGHT / 1.1);
}

function GalleryTooltips() {
    $('a.#print_frame1').tooltip({
        offset: [78, 0],
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-up-arrow-loan2"></span></div>'
    });
    $('a.#send_to_friend_frame1').tooltip({
        offset: [76, 3],
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-up-arrow-loan"></span></div>'
    });
    $('a.nav-link-alternative').tooltip({
        offset: [-2, 0],
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-down-arrow"></span></div>'
    });
    $('#footer-home').tooltip({
        offset: [-3, 2],
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-down-arrow"></span></div>'
    });
    $('#footer-modelsboxes').tooltip({
        offset: [-6, 40],
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-down-arrow"></span></div>'
    });
    $('#model-menu-panel ul li a').tooltip({
        offset: [40, 110],
        tipClass: 'tooltip2 unselectable',
        layout: '<div><span class="tooltip-left-arrow"></span></div>'
    });
    $('.color-picker ul li a.color_pick').tooltip({
        offset: [110, 0],
        tipClass: 'tooltip_colors unselectable',
        layout: '<div><span class="tooltip-up-arrow"></span></div>'
    });
}

function GalleryTooltips_OLD_IE() {
    $('a.nav-link-alternative').tooltip({
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-down-arrow"></span></div>'
    });
    $('#footer-modelsboxes').tooltip({
        tipClass: 'tooltip',
        layout: '<div><span class="tooltip-down-arrow"></span></div>'
    });
    $('#model-menu-panel ul li a').tooltip({
        tipClass: 'tooltip2 unselectable',
        layout: '<div><span class="tooltip-right-arrow"></span></div>'
    });
    $('.color-picker ul li a.color_pick').tooltip({
        tipClass: 'tooltip_colors unselectable',
        layout: '<div><span class="tooltip-up-arrow"></span></div>'
    });
}

function initPowerOfDreams() {
    circuitClick();
    enableFancyboxAccessories();
    asimoNavigation();
    technologyNavigation();
}
pageScrollerisDisabled = false;
pageScrollerisFlag = false;

function loanButton() {
    $('#model-menu-panel a.loan').click(function () {
        if (!$('#model-loan-calculator').hasClass("active")) {
            $('#model-loan-calculator').fadeIn(300, function () {
                $(this).addClass("active");
                $('#model-loan-calculator-container').animate({
                    left: "0%"
                }, 550);
            });
        } else {
            $('#model-loan-calculator-container').animate({
                left: "-120%"
            }, 590, function () {
                $('#model-loan-calculator').fadeOut(300, function () {
                    $(this).removeClass("active");
                });
            });
        }
    });
    $('a#close_loan_calculator').click(function () {
        $('#model-loan-calculator-container').animate({
            left: "-120%"
        }, 590, function () {
            $('#model-loan-calculator').fadeOut(300, function () {
                $(this).removeClass("active");
            });
        });
    });
}

function socialMediaButtonsInit() {
    loanButton();
    $('#model-menu-panel ul li a.facebook_NOT_READY_YET').live("click", function () {
        var url = $(this).attr("url");
        var title = $(this).attr("title");
        facebookShare(url, title);
    });
    $('#model-menu-panel ul li a.twitter').live("click", function () {
        var url = $(this).attr("rev");
        var title = $(this).attr("rel");
        twitterShare(url, title);
    });
}

function facebookShare(url, title) {
    u = url;
    t = title;
    window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
}

function twitterShare(url, title) {
    t = title;
    u = url;
    window.open('http://twitter.com/home?status=' + encodeURIComponent(t) + ' : ' + encodeURIComponent(u), 'sharer', 'toolbar=0,status=0,width=626,height=436');
    return false;
}

function technologyNavigation() {
    $('#model-menu-panel ul li a.abs, #model-menu-panel ul li a.dct, #model-menu-panel ul li a.airbag,#model-menu-panel ul li a.stop,#model-menu-panel ul li a.info').click(function () {
        var target = $(this).attr("rel");
        $('#model-colors-container.power_of_dreams img.header_power_of_dreams').fadeOut(380);
        $('#model-menu-panel ul li a.opened').removeClass("opened");
        $(this).addClass("opened");
        $('#model-info-panel .page_left.opened,#model-specs-panel .page_right.opened').fadeOut(300, function () {
            $(this).removeClass("opened");
        });
        if ($(this).hasClass("has_swf")) {
            var url = $(this).attr("rev");
            $.post('ajax.load-header-cover.php', {
                url: url,
                type: "swf"
            }, function (data) {
                $('#tech_header').fadeOut(350, function () {
                    $(this).css("background-color", "#FFFFFF");
                    $(this).html(data).fadeIn(400);
                });
            });
        } else {
            $('#tech_header').fadeOut(350, function () {
                $(this).empty();
            });
        }
        $('#model-info-panel .page_left.' + target + ',#model-specs-panel .page_right.' + target).delay(150).fadeIn(300, function () {
            $(this).addClass("opened");
        });
    });
    $('a.play_flash').click(function () {
        var url = $(this).attr("rev");
        if (url != "") {
            $.post('ajax.load-header-cover.php', {
                url: url,
                type: "swf"
            }, function (data) {
                $('#tech_header').fadeOut(350, function () {
                    $(this).css("background-color", "#FFFFFF");
                    $(this).html(data).fadeIn(400);
                });
            });
        }
    });
    $('a.play_video').click(function () {
        var url = $(this).attr("rev");
        if (url != "") {
            $.post('ajax.load-header-cover.php', {
                url: url,
                type: "video"
            }, function (data) {
                $('#tech_header').fadeOut(350, function () {
                    $(this).css("background-color", "#000000");
                    $(this).html(data).fadeIn(400);
                });
            });
        }
    });
}

function asimoNavigation() {
    $('#model-menu-panel ul li a.power_of_dreams').click(function () {
        $('#model-colors-container.power_of_dreams img.header_power_of_dreams').fadeIn(390);
        $('#model-menu-panel ul li a.opened').removeClass("opened");
        $(this).addClass("opened");
        $('#model-info-panel .page_left.opened,#model-specs-panel .page_right.opened').fadeOut(300, function () {
            $(this).removeClass("opened");
        });
        $('#model-info-panel .page_left.power_of_dreams,#model-specs-panel .page_right.power_of_dreams').delay(150).fadeIn(300, function () {
            $(this).addClass("opened");
        });
    });
    $('#model-menu-panel ul li a.asimo').click(function () {
        $('#model-colors-container.power_of_dreams img.header_power_of_dreams').fadeOut(380);
        $('#model-menu-panel ul li a.opened').removeClass("opened");
        $(this).addClass("opened");
        $('#model-info-panel .page_left.opened,#model-specs-panel .page_right.opened').fadeOut(300, function () {
            $(this).removeClass("opened");
        });
        $('#model-info-panel .page_left.asimo,#model-specs-panel .page_right.asimo').delay(150).fadeIn(300, function () {
            $(this).addClass("opened");
        });
    });
}
var default_zoom_level = 15;

function circuitClick() {
    $('#circuits-list .circuit_line a').click(function () {
        $('#circuits-list .circuit_line a.selected').removeClass("selected").next().fadeOut(200).next().fadeOut(200);
        $(this).addClass("selected");
        if ($(this).attr("pos") != "1") {
            $('#map_canvas').fadeIn(400);
            GMAP_initialize($(this).attr("pos"), $(this).attr("mapzoom"));
            $('#circuits-list .show_info').removeClass("show_info");
            $(this).next().addClass("show_info");
            var circuit_info = $(this).next().html();
            $('#circuit_details_right_ajax').fadeOut(300, function () {
                $(this).html(circuit_info);
            }).fadeIn(370);
        } else {
            $('#map_canvas').fadeOut(400);
        }
    });
    $('#model-menu-panel ul li a.drivers').click(function () {
        $('#model-menu-panel ul li a.opened').removeClass("opened");
        $(this).addClass("opened");
        $('#circuit_details_right').fadeOut(300);
        $('#circuits-list .category.opened').fadeOut(300, function () {
            $(this).removeClass("opened");
            $('#model-colors-container.power_of_dreams').fadeIn(300);
            $('#map_canvas').fadeOut(300);
            $('#model-colors-container img.header').fadeIn(300);
            $('#circuits-list .category.riders').fadeIn(400).addClass("opened");
        });
    });
    var machines_container_jscrollpane = false;
    $('#model-menu-panel ul li a.machines').click(function () {
        $('#model-menu-panel ul li a.opened').removeClass("opened");
        $(this).addClass("opened");
        $('#circuit_details_right').fadeOut(300);
        $('#circuits-list .category.opened').fadeOut(300, function () {
            $(this).removeClass("opened");
            $('#model-colors-container.power_of_dreams').fadeIn(300);
            $('#map_canvas').fadeOut(300);
            $('#model-colors-container img.header').fadeIn(300);
            $('#circuits-list .category.specs').fadeIn(400).addClass("opened");
        });
    });
    var calendar_container_jscrollpane = false;
    $('#model-menu-panel ul li a.circuits').click(function () {
        $('#circuits-list .circuit_line a.first').trigger("click");
        $('#model-colors-container img.header').fadeOut(390);
        $('#model-menu-panel ul li a.opened').removeClass("opened");
        $('#circuit_details_right').fadeIn(300);
        $(this).addClass("opened");
        $('#circuits-list .category.opened').fadeOut(300, function () {
            $(this).removeClass("opened");
            $('#circuits-list .category.calendar').fadeIn(400, function () {
                if (!$('#standings_area .standings_table.active').hasClass("jpane_enabled")) {
                    enableJscrollOnce($('#standings_area .standings_table.active'));
                    $('#standings_area .standings_table.active').addClass("jpane_enabled");
                }
                if (!calendar_container_jscrollpane) {
                    enableJscrollOnce($('#circuits-list .calendar_container'));
                    calendar_container_jscrollpane = true;
                }
            }).addClass("opened");
        });
    });
    enableJscrollOnce($('#circuits-list .calendar_container'));
    $('#standings_area a.accordeon').click(function () {
        $('#standings_area a.accordeon.opened').removeClass("opened");
        $(this).addClass("opened");
        var obj = $(this);
        var rel_ = $(this).attr("rel");
        $('#standings_area .standings_table.active').fadeOut(300, function () {
            $(this).removeClass("active");
            $('#standings_area .standings_table' + rel_).fadeIn(300, function () {
                $(this).addClass("active");
                if (!$('#standings_area .standings_table.active').hasClass("jpane_enabled")) {
                    enableJscrollOnce($('#standings_area .standings_table.active'));
                    $('#standings_area .standings_table').addClass("jpane_enabled");
                }
            });
        });
    });
}
var GMAP_LOADED = false;

function GMAP_initialize(point, default_zoom) {
    if (!GMAP_LOADED) {
        $('#map_canvas').show();
        GMAP_LOADED = true;
    }
    var p1 = "";
    var p2 = "";
    default_zoom_level = default_zoom;
    if (point == 0) {
        p1 = "20";
        p2 = "-10";
    } else {
        p1 = point.split(',', 2)[0];
        p2 = point.split(',', 2)[1];
    }
    var myLatLng = new google.maps.LatLng(p1, p2);
    var myOptions = {
        scrollwheel: false,
        center: myLatLng,
        zoom: parseInt(default_zoom),
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var map = new google.maps.Map(document.getElementById("googlemap"), myOptions);
    if (point != 0) {
        marker = new google.maps.Marker({
            position: myLatLng,
            map: map
        });
        marker.setMap(map);
    }
}

function setupHoverscroll() {
    enableJscrollRepeat($('.hoverscroll3'));
    enableJscrollRepeat($('.driver_hoverscroll'));
    enableJscrollRepeat($('#model-info-container3'));
    $('#model_bottom_arrows .nav-arrows a.up').hover(function () {
        var direction = 1,
            speed = 4;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    }, function () {
        var direction = 0,
            speed = 1;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    });
    $('#model_bottom_arrows .nav-arrows a.down').hover(function () {
        var direction = -1,
            speed = 4;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    }, function () {
        var direction = 0,
            speed = 1;
        $('#model-info-container .hoverscroll3')[0].startMoving(direction, speed);
    });
}

function enableFancyboxVideoPopup(type) {
    $("a.gallery_video_trigger").live("click", function () {
        $.fancybox({
            openEffect: 'elastic',
            helpers: {
                overlay: {
                    opacity: 0.85,
                    css: {
                        'background-color': '#111111'
                    }
                }
            },
            wrapCSS: "video",
            openSpeed: "normal",
            padding: 0,
            closeEffect: 'fade',
            'closeSpeed': "normal",
            'autoScale': false,
            'title': this.title,
            'width': 640,
            'height': 385,
            'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type': type,
            'swf': {
                'wmode': 'transparent',
                'allowfullscreen': 'true'
            }
        });
        return false;
    });
    $("a.gallery_video_trigger_vimeo").click(function () {
        $.fancybox({
            'padding': 0,
            'autoScale': false,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'title': this.title,
            'width': 681,
            'height': 383,
            'href': this.href.replace(new RegExp("([0-9])", "i"), 'moogaloop.swf?clip_id=$1'),
            'type': 'swf'
        });
        return false;
    });
}

function enableFancyboxAccessories() {
    var easing_accessories_panel = 'easeInOutExpo';
    var lists = $('#model-accessories-panel .cont').children().length;
    var screens = parseInt(lists / 3);
    if (lists <= 3) {
        $('#accessories_navigation').hide();
    }
    var curr_screen = 1;
    $('#accessories_navigation a.next').click(function () {
        if (curr_screen <= screens && lists > 3) {
            curr_screen++;
            $('#model-accessories-panel .cont').animate({
                left: "-=456px"
            }, 600, easing_accessories_panel);
        }
    });
    $('#accessories_navigation a.prev').click(function () {
        if (curr_screen > 1) {
            $('#model-accessories-panel .cont').animate({
                left: "+=456px"
            }, 600, easing_accessories_panel);
            curr_screen--;
        }
    });
    $('body.model .accessories .fancybox-inner .thumbs_container a').live("click", function () {
        var rel_ = $(this).attr("rel");
        $('body.model .accessories .fancybox-inner .img_container .img_c.active').fadeOut(200, function () {
            $(this).removeClass("active");
        });
        $('body.model .accessories .fancybox-inner .img_container .img_c.img_' + rel_).fadeIn(200, function () {
            $(this).addClass("active");
        });
    });
    $("a.acc_image_link").fancybox({
        openEffect: 'fade',
        helpers: {
            showEarly: true,
            overlay: {
                opacity: 0.85,
                css: {
                    'background-color': '#111111'
                }
            },
            title: {
                type: 'outside'
            }
        },
        wrapCSS: "accessories",
        openSpeed: "normal",
        nextMethod: "zoomIn",
        padding: 0,
        closeEffect: 'elastic',
        'type': 'ajax',
        'closeSpeed': "normal",
        'autoScale': true,
        'autoSize': true,
        'autoWidth': true,
        'autoHeight': true,
        'autoResize': true,
        'title': this.title,
        beforeLoad: function () {
            var attributes = this.content.split("[]", 2);
            var url = 'images/models/' + attributes[1] + '/accessories/' + attributes[0];
            ForceLoadImage(url);
        },
        afterLoad: function () {
            var tmp = 1;
            for (var i = 0; i < 50000; i++) {
                tmp++;
            }
            $.fancybox.update();
        },
        afterShow: function () {
            $('body.model .accessories .fancybox-inner .thumbs_container').animate({
                opacity: 1
            }, 300);
        }
    });
}

function ForceLoadImage(_url1) {
    var _im = $("<img id=\"img_loader_forced\" style=\"visibility:hidden;\">");
    _im.hide();
    _im.bind("load", function () {
        $(this).fadeIn();
    });
    $('body.model .accessories .fancybox-inner .img_container').append(_im);
    _im.attr('src', _url1).load(function () {
        $('#img_loader_forced').remove();
    });
}