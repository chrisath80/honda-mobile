function DisplayModels(Id) {
	$(".h-menu-model-js").css({
		display : "none"
	});
	$("#" + Id).css({
		display : "block"
	});
	return;
}

function GoMenuBack(){
	if($(".h-menu-first-level")[0].style.left != "0px"){
		$(".h-menu-first-level").css({
			"left": ($(".h-menu-first-level")[0].style.left.split("px")[0].split("%")[0] + 100) + "%"
		});
		$(".h-menu-second-level").css({
			"left": ($(".h-menu-second-level")[0].style.left.split("px")[0].split("%")[0] + 100) + "%"
		});
		$(".h-menu-third-level").css({
			"left": ($(".h-menu-third-level")[0].style.left.split("px")[0].split("%")[0] + 100) + "%"
		});
	}
	return;
}
