var AnimArrows = 1;
function AnimateArrows() {
	if (AnimArrows == 1) {
		$("#Arrows1").animate({
			opacity : 1,
		}, 500);
		$("#Arrows2").animate({
			opacity : 0,
		}, 500);
		$("#Arrows3").animate({
			opacity : 0,
		}, 500);
		AnimArrows = 2;
	}
	if (AnimArrows == 2) {
		$("#Arrows1").animate({
			opacity : 0,
		}, 400);
		$("#Arrows2").animate({
			opacity : 1,
		}, 400);
		$("#Arrows3").animate({
			opacity : 0,
		}, 400);
		AnimArrows = 3;
	}
	if (AnimArrows == 3) {
		$("#Arrows1").animate({
			opacity : 0,
		}, 350);
		$("#Arrows2").animate({
			opacity : 0,
		}, 350);
		$("#Arrows3").animate({
			opacity : 1,
		}, 350);
		AnimArrows = 1;
	}
}

setInterval("AnimateArrows()", 550);

var CBRPage = 1;
var TotalCBRpages = 4;

var InitFire = function() {
	$("#Model").on("swiperight", function(e) {
		var a = MoveFireblade("right");
		return;
	});

	$("#Model").on("swipeleft", function(e) {
		var a = MoveFireblade("left");
		return;
	});
	MoveFireblade("booooooo");
	return;
};

var MoveFireblade = function(Direction) {
	var PageChanged = false;
	if (Direction == "left" && CBRPage < TotalCBRpages) {
		PageChanged = true;
		CBRPage++;
	}
	if (Direction == "right" && CBRPage > 1) {
		PageChanged = true;
		CBRPage--;
	}
	if (PageChanged) {
		$("#ModelContainer").animate({
			left : "-" + ((CBRPage - 1) * 100) + "%",
		}, 500, "easeInOutQuart", function() {
			FinishAnimationRun = false;
		});
	}

	var Indexations = [0, 1, 2, 3, 4];

	var Opaciters = $("#ModelSubmenuContent")[0].getElementsByClassName("opaciter");
	var OpacitersCounter = 0;
	while (OpacitersCounter < Opaciters.length) {
		Opaciters[OpacitersCounter].style.backgroundColor = '#000';
		OpacitersCounter++;
	}

	if ($("#Opaciter" + (CBRPage - 1))) {
		$("#Opaciter"+(CBRPage-1))[0].style.backgroundColor = '#EC1B2E';
	}

	return;
};

InitFire();
