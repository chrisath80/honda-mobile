$(document).bind('mobileinit', function() {
	$.mobile.keepNative = "select,input";
	$.mobile.ajaxEnabled = false;
});

$(window).load(function() {
	var a = Preloader(false);
	$('select').change(function() {
		if ($(this).val() != "") {
			$(this)[0].style.border = "1px solid #16A085";
		} else {
			$(this)[0].style.border = "1px solid #ffffff";
		}
	});
	var a = InputMarker();
});

var InputMarker = function() {
	var inpsToMonitor = document.querySelectorAll("input,textarea");

	function adjustStyling(zEvent) {
		var inpVal = zEvent.target.value;
		if (inpVal && inpVal.replace(/^\s+|\s+$/g, ""))
			zEvent.target.style.border = "1px solid #16A085";
		else
			zEvent.target.style.border = "1px solid #fff";
	}

	for (var J = inpsToMonitor.length - 1; J >= 0; --J) {
		inpsToMonitor[J].addEventListener("change", adjustStyling, false);
		inpsToMonitor[J].addEventListener("keyup", adjustStyling, false);
		inpsToMonitor[J].addEventListener("focus", adjustStyling, false);
		inpsToMonitor[J].addEventListener("blur", adjustStyling, false);
		inpsToMonitor[J].addEventListener("mousedown", adjustStyling, false);

		//-- Initial update. note that IE support is NOT needed.
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		inpsToMonitor[J].dispatchEvent(evt);
	}
};

var Preloader = function(Mode){
	if(Mode){
		$("#Preloader").css("display","block");
	}else{
		$("#Preloader").css("display","none");
	}
	return;
};

var ShowVideo = function(VideoURL) {
	var Id = VideoURL.split("/")[VideoURL.split("/").length - 1];
	$("#VideoContainer")[0].innerHTML = '<iframe src="//player.vimeo.com/video/' + Id + '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	$("#ShowVideo").css("display","block");
	return;
};

var CloseVideo = function(){
	$("#VideoContainer")[0].innerHTML = '';
	$("#ShowVideo").css("display","none");
	return;
};
